
local function PrintNotice( stMsg, chatTypeIn)DawnMod:PrintNotice(stMsg,chatTypeIn);end
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end


local	channelMsgData = {
	arrJoin			= {};
	arrLeft			= {};
	stJoinZone		= "";
	stLeftZone		= "";
	stPrevMsg		= "";
	stSavedJoinPre		= "";
	stSavedJoinPost		= "";
	bWasOnTaxi		= false;
	bFirstLeftPosted	= false;
	timeLastReceived	= 0;
	};

local	pollingData = {
	start	= 0;
	elapsed	= 0;
	bInit	= false;
	}



local function PollingStart()
	if (not pollingData.bInit) then
		DawnMod.frame:Show();
		DawnMod.frame:SetScript("OnUpdate",DawnModTimer_OnUpdate);
		pollingData.start = GetTime();
		end
	pollingData.bInit = true;
	end


local function PollingStop()
	DawnMod.frame:Hide();
	DawnMod.frame:SetScript("OnUpdate",nil);
	pollingData.bInit = false;
	end


local function ShortChannelName( stChannel, bJoining)
	local	str, stUpr, stZone;

	assertstr(stChannel);
	assertbool(bJoining);
	
	stUpr	= string.upper(stChannel);	
	str	= stChannel;
	stZone	= "";
	
	if (string.find( stUpr,"GENERAL - ")==1) then
		str = string.sub(stChannel,1,string.len("GENERAL"));
		stZone = string.sub(stChannel, string.len("GENERAL - ")+1);
		end
	if (string.find( stUpr,"LOCALDEFENSE - ")==1) then
		str = string.sub(stChannel,1,string.len("LOCALDEFENSE"));
		stZone = string.sub(stChannel, string.len("LOCALDEFENSE - ")+1);
		end
	if (string.find( stUpr,"TRADE - ")==1) then
		str = string.sub(stChannel,1,string.len("TRADE"));
		end
	if (string.find( stUpr,"GUILDRECRUITMENT - ")==1) then
		str = string.sub(stChannel,1,string.len("GUILDRECRUITMENT"));
		end
	if (stZone~="") then
		if (bJoining) then channelMsgData.stJoinZone = stZone;
		else	channelMsgData.stLeftZone = stZone; end
		end
		
	return str;
	end
	

local function PrintChannelString( bInFlight, stPre, stPost)
	assertbool(bInFlight);
	assertstr(stPre);
	assertstr(stPost);
	if (bInFlight) then
		PrintNotice( stPre..stPost, ChatTypeInfo.SYSTEM);
		--RaidNotice_AddMessage(RaidBossEmoteFrame, stPre..stPost, ChatTypeInfo.SYSTEM);
	else	DawnMod:PrintInfoItem( stPre, stPost);
		end
	if (DawnMod:IsDebug() and (string.find(stPre,"%(")~=nil)) then
		local	tmp,tmp2;
		tmp = string.sub(stPre, string.find(stPre,"%(")+1, string.find(stPre,"%)")-1);
		tmp,tmp2 = GetInstanceInfo();
		end
	end



local function DoChannelString( bJoining, tab, stPrefixIn, stChannelZone)
	local	str, index, value, stPrefix, bPostInFlight;
	
	assertbool(bJoining);
	asserttab(tab);
	assertstr(stPrefixIn);
	assertstr(stChannelZone);
	
	table.sort( tab, function(a,b) return string.upper(a)<string.upper(b);end);
	str = "";
	
	for index,value in ipairs(tab) do
		if (string.len(str)>0) then str=str..", "; end
		-- Filter out carbonite channel
		if (string.find(value,"Crb")==1) then value="";end
		str=str..value;
		end
	if (string.len(str)>0) then
		stPrefix = stPrefixIn;
		if (stChannelZone~="") then
			stPrefix=stPrefix.." ("..stChannelZone..")";
			end
		stPrefix = stPrefix..": ";
		bPostInFlight = channelMsgData.bIsOnTaxi;
		if (not bJoining) then
			if (channelMsgData.bFirstLeftPosted) then
				bPostInFlight = false;
				channelMsgData.bFirstLeftPosted = true;
				end
			end
		PrintChannelString( bPostInFlight, stPrefix, str);
		if (bJoining) then
			channelMsgData.stSavedJoinPre = stPrefix;
			channelMsgData.stSavedJoinPost = str;
			end
		end
	table.wipe(tab);
	end


function TableInsertUnique( tab, str)
	local	i;
	
	i=1;
	while (tab[i]~=nil) do
		if (tab[i]==str) then return end
		i = i + 1;
		end
	table.insert(tab,str);
	end


function DawnMod_ChannelHandler( this,event,farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,...)
	local	retval, str, StChan,stMsg;
	
	retval = false;
	
	if (DawnMod:CfgGetValue(DawnModConfigItem_Enabled) and
	    DawnMod:CfgGetValue(DawnModConfigItem_FixChannelChanged) and
	    ((farg1=="YOU_CHANGED")or(farg1=="YOU_JOINED")or(farg1=="SUSPENDED")or(farg1=="YOU_LEFT")) ) then
	    	if (farg9~="") then stChan=farg9;
	    	else	stChan=farg4;end
		str=farg1..stChan;
		if (str~=channelMsgData.stPrevMsg) then 
			if ((farg1=="YOU_CHANGED")or(farg1=="YOU_JOINED")) then
				TableInsertUnique(channelMsgData.arrJoin, ShortChannelName(stChan, true));
				end
			if ((farg1=="SUSPENDED")or(farg1=="YOU_LEFT")) then
				TableInsertUnique(channelMsgData.arrLeft, ShortChannelName(stChan, false));
				end
			PollingStart();
			channelMsgData.timeLastReceived	= GetTime();
			end
		channelMsgData.stPrevMsg = str;
		retval=true;
		end
	
	
	return retval,farg1,farg2, farg3, farg4, farg5, farg6, farg7, farg8, farg9, ...
	end


function AllowTaxiPolling()
	if (UnitOnTaxi("PLAYER")==nil) then return false; end
	if ( (DawnMod:PlayerIsInRaid() or DawnMod:PlayerIsInParty()) and
	     (DawnMod:PlayerIsInRaidZone() or DawnMod:PlayerIsInPartyZone()) ) then
		return false; end
	return true;
	end

function DawnModTimer_OnUpdate(  this,elapsed)
	local	curTime;
	
	curTime = GetTime();
	
	if ((channelMsgData.timeLastReceived~=0) and(curTime-channelMsgData.timeLastReceived > DawnMod_ChangeChannelWaitTime)) then
		channelMsgData.bIsOnTaxi = (UnitOnTaxi("PLAYER") ~= nil);
		if (not channelMsgData.bIsOnTaxi) then PollingStop();end
		DoChannelString( true, channelMsgData.arrJoin, "Joined",channelMsgData.stJoinZone);
		DoChannelString( false, channelMsgData.arrLeft, "Left",channelMsgData.stLeftZone);
		channelMsgData.stJoinZone="";
		channelMsgData.stLeftZone="";
		channelMsgData.timeLastReceived = 0;
		end
	
	if (channelMsgData.bIsOnTaxi and (not UnitOnTaxi("PLAYER"))) then
		PollingStop();
		channelMsgData.bIsOnTaxi = false;
		channelMsgData.bFirstLeftPosted = false;
		PrintDbg("Not on taxi anymore");
		PrintChannelString( false, channelMsgData.stSavedJoinPre, channelMsgData.stSavedJoinPost);
		end
	end
	

