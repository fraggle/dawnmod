
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end
local function StringMatch(st1,st2) assertstr(st1);assertstr(st2);return (st1==st2); end


--------------------------

DawnModKnown		= {};
local CacheWidth	= 5;


function DawnModKnown:New(obj)
	obj = obj or {};

	setmetatable(obj,self);
    self.__index   = self;
    	
    obj.cache		= {}; -- [1] name#1, [2] isFriend#1, [3] isGuildie#1, [4] isParty#1, [5] isRaid#1,
    				      ---[6] name#1, [7] isFriend#2, etc
	obj.arrMisc		= {};
    obj.numInCache		= 0;
    obj.nCacheLength	= 0;
    	
	return obj
	end


function DawnModKnown:Begin( nCacheLength)
	self:SetCacheLength( nCacheLength);
	end


function DawnModKnown:IsFriend( stNameIn)
	local numFriends, cnt, stFriendName, stName;
	assertstr( stNameIn);

-- Check if they're on your friends list.
	stName = string.upper(DawnMod:GetLongName( stNameIn));
	cnt=1;
	repeat
		stFriendName = GetFriendInfo(cnt);
		if (stFriendName~=nil) then
			stFriendName = string.upper(DawnMod:GetLongName( stFriendName));
			if (StringMatch( stName, stFriendName)) then return true; end
			end
		cnt=cnt+1;
	until ((cnt>=51) or (stFriendName==nil));

-- Check if it's a real-id friend sending you a normal whisper
	numFriends = BNGetNumFriends();
	stName=DawnMod:ShortNameIfSameRealm(stName);
	cnt= 1;
	stFriendName="";
	while (cnt<=numFriends) do
		_,_,_,stFriendName = BNGetFriendInfo( cnt);
		if ((stFriendName~=nil) and StringMatch(string.upper(stFriendName), stName)) then return true; end
		cnt=cnt+1;
		end
	return false;
	end


function DawnModKnown:IsGuildie( stNameIn)
	assertstr(stNameIn);

	return (UnitIsInMyGuild( DawnMod:ShortNameIfSameRealm( stNameIn)) ~= nil);
	end



function DawnModKnown:RemoveCacheEntry( nIndexIn)
	local	cnt, nIndex;
	
	if (self.nCacheLength==0) then return end
	if (self.numInCache==0) then return end

	if (nIndexIn==nil) then nIndex = self.numInCache-1; else nIndex = nIndexIn; end
	for cnt=1,CacheWidth do
		table.remove( self.cache, nIndex*CacheWidth+1);
		end
	self.numInCache = self.numInCache-1;
	end


function DawnModKnown:AddToCache( stName,bFriend,bGuild,bParty,bRaid)

	if (self.nCacheLength==0) then return end
	
	if (self.numInCache == self.nCacheLength) then
		self:RemoveCacheEntry( self.numInCache);
		end

	if (self:FindInCache(stName)==nil) then
		table.insert( self.cache, 1, bRaid);
		table.insert( self.cache, 1, bParty);
		table.insert( self.cache, 1, bGuild);
		table.insert( self.cache, 1, bFriend);
		table.insert( self.cache, 1, stName);
		self.numInCache = self.numInCache+1;
		end
	end


function DawnModKnown:SetCacheLength( nNewLength)
	assertnum(nNewLength);
	if (nNewLength<0) then return end
	
	while (self.numInCache > nNewLength) do
		self:RemoveCacheEntry();
		end
	self.nCacheLength = nNewLength;
	end

function DawnModKnown:PrintCache()
	local	pos;
	
	PrintDbg("Wid = "..tostring(CacheWidth).."  num = "..tostring(self.numInCache));
	pos=0;
	while (pos<self.numInCache) do
		PrintDbg("#"..tostring(pos+1)..": "..self.cache[pos*CacheWidth+1] );
		pos=pos+1;
		end
	end


function DawnModKnown:FindInCache( stName)
	local	pos, bFriend, bGuild, bParty, bRaid;
	
	assertstr(stName);
	if (self.nCacheLength==0) then return nil; end

	pos = 0;
	while ((pos<self.numInCache) and (not StringMatch( stName, self.cache[pos*CacheWidth+1]))) do
		pos = pos+1;
		end
	
	if (pos<self.numInCache) then return pos; end
	return nil;
	end


function DawnModKnown:IsMisc( stNameIn)
	local	stName, pos;

	assertstr(stNameIn)
	stName = string.upper(DawnMod:GetLongName( stNameIn));
	pos=1;
	while (self.arrMisc[pos]) do
		if (StringMatch(self.arrMisc[pos],stName)) then
			return true;
			end
		pos=pos+1;
		end
	return false;
	end


function DawnModKnown:AddMisc( stNameIn)
	local	stName, pos;

	assertstr(stNameIn)
	stName = string.upper(DawnMod:GetLongName( stNameIn));
	if (not self:IsMisc( stName)) then
		table.insert( self.arrMisc, stNameIn);
		end
	end


function DawnModKnown:IsKnown( stNameIn)
	local	stName, pos, bAnswer, bFriend, bGuild, bParty, bRaid;
	
	assertstr( stNameIn);
	stName = string.upper(DawnMod:GetLongName( stNameIn));

	if (StringMatch(DawnMod:ShortNameIfSameRealm(stName),string.upper(UnitName("PLAYER")))) then
		return true,true,true,true,true;
		end
	
	pos = self:FindInCache( stName);
	
	bAnswer = self:IsMisc( stName);
	if (pos==nil) then
		bFriend = self:IsFriend( stName);
		bGuild = self:IsGuildie( stName);
		bParty = DawnMod:IsInParty( stName);
		bRaid = DawnMod:IsInRaid( stName);
	else
		bFriend = self.cache[CacheWidth*pos+2];
		bGuild = self.cache[CacheWidth*pos+3];
		bParty = self.cache[CacheWidth*pos+4];
		bRaid = self.cache[CacheWidth*pos+5];
		if (pos>0) then
			self:RemoveCacheEntry(pos);
			end
		end

	self:AddToCache( stName, bFriend, bGuild, bParty, bRaid);
	return bAnswer, bFriend, bGuild, bParty, bRaid;
	end


