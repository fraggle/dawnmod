
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end
local function StringMatch(st1,st2) assertstr(st1);assertstr(st2);return (st1==st2); end


DawnMod_Reply_Normal	= 1;
DawnMod_Reply_Battlenet	= 2;


local function assertcategory(x) assertnum(x);assert((x>=DawnMod_Reply_Normal)and(x<=DawnMod_Reply_Battlenet));end


DawnModReply	= {};


function DawnModReply:New(obj)
	obj = obj or {};

	setmetatable(obj,self);
    self.__index   = self;
    	
    obj.arrReplies		= {}; -- [1] category#1, [2] recipient#1, [3] time#1,
    				      ---[4] category#2, [5] recipient#2, [6] time#2, etc
    obj.arrCatNumRepliesPerWindow	= {};
    obj.overallNumRepliesPerWindow	= 0;

	-- singleWindow should be lower than overallWindow ('PurgeOld' assumes this to be the case).
	obj.overallWindow	= 10000; -- numRepliesPerCategory & numRepliesOverall are measured against this window.
	obj.singleWindow	= 3000;  -- no single recipient gets a reply more that 'this' often

    obj.nDefaultMaxCategoryRepliesPerWindow = 7;
    obj.nDefaultMaxOverallRepliesPerWindow = 10;
    	
    obj.bDisablePurge = false;
    	
	return obj
	end


function DawnModReply:Begin()
	self.overallNumRepliesPerWindow = self.nDefaultMaxOverallRepliesPerWindow;
	self.arrCatNumRepliesPerWindow[ DawnMod_Reply_Normal] = self.nDefaultMaxCategoryRepliesPerWindow;
	self.arrCatNumRepliesPerWindow[ DawnMod_Reply_Battlenet] = self.nDefaultMaxCategoryRepliesPerWindow;
	end


function DawnModReply:PurgeOld()
	local curTime, pos;
	
	if (self.bDisablePurge) then return end;
	
	curTime = GetTime()*1000;
	pos = 1;

	while (self.arrReplies[pos]) do
		if (curTime-self.arrReplies[ pos+2] > self.overallWindow) then
			table.remove( self.arrReplies, pos);
			table.remove( self.arrReplies, pos);
			table.remove( self.arrReplies, pos);
		else	pos = pos+3;
			end
		end
	end
	

function DawnModReply:GetNumRepliesInWindow( nCategory)
	local	cat, pos, nReplies;
	
	assert( (type(nCategory)=="number")or(nCategory==nil));
	if (nCategory == nil) then cat = 0; else cat = nCategory; end

	self:PurgeOld();
	pos = 1;
	nReplies = 0;
	while (self.arrReplies[pos]) do
		if ((self.arrReplies[pos]==cat) or (cat==0)) then
			nReplies = nReplies+1;
			end
		pos=pos+3;
		end
	return nReplies;
	end


function DawnModReply:SetNumRepliesPerWindowForCategory( nCategory, nMaxReplies)
	assertcategory(nCategory);
	assertnum(nMaxReplies);
	assert( nMaxReplies>=0);
	self.arrCatNumRepliesPerWindow[ nCategory] = nMaxReplies;
	end


function DawnModReply:GetNumRepliesPerWindowForCategory( nCategory)
	assertcategory(nCategory);
	return self.arrCatNumRepliesPerWindow[nCategory];
	end


function DawnModReply:FindRecipient( nCategory, stRecipient)
	local	pos;
	assertcategory(nCategory);
	assertstr(stRecipient);

	pos = 0;
	while (self.arrReplies[pos*3+1]) do
		if (StringMatch(self.arrReplies[pos*3+2], stRecipient)) then
			return pos;
			end
		pos = pos + 1;
		end
	return nil;
	end


function DawnModReply:ShowTable()
	local pos;
	
	pos = 0;
	while (self.arrReplies[pos*3+1]) do
		PrintDbg("#"..tostring(pos*3+1)..": "..tostring(self.arrReplies[(pos*3)+1]).."  "..
			 "#"..tostring(pos*3+2)..": "..tostring(self.arrReplies[(pos*3)+2]).."  "..
			 "#"..tostring(pos*3+3)..": "..tostring(self.arrReplies[(pos*3)+3]).."  ");
		pos = pos+1;
		end
	return
	end


function DawnModReply:DidReply( nCategory, stRecipient)
	local	tmp;
	
	tmp = self:FindRecipient( nCategory, stRecipient);
	if (tmp==nil) then
		table.insert( self.arrReplies, nCategory);
		table.insert( self.arrReplies, stRecipient);
		table.insert( self.arrReplies, GetTime() *1000);
	else	self.arrReplies[tmp*3+3] = GetTime() *1000;
		end
	end


function DawnModReply:CountReplies( nCategory)
	local	pos, numReplies;
	
	pos = 1;
	numReplies = 0;
	while (self.arrReplies[pos]) do
		if ((nCategory==nil) or (self.arrReplies[pos]==nCategory)) then
			numReplies = numReplies + 1;
			end
		pos = pos+3;
		end
	return numReplies;
	end
	

function DawnModReply:SendReply( nCategory, stRecipientIn, stMsg)
	local	bnId, st, bNoReply, tmp, stRecip;
	assertcategory( nCategory);
	assertstr( stRecipientIn);
	assertstr( stMsg);
	
	if (nCategory == DawnMod_Reply_Battlenet) then
		stRecip = string.sub( stRecipientIn, 4);
		stRecip = string.sub( stRecip, 1, string.find(stRecip, "%|k")-1);
		end
	if (nCategory == DawnMod_Reply_Normal) then
		stRecip = stRecipientIn;
		end
		
	
	bNoReply = false;
	
	self:PurgeOld();
	self.bDisablePurge = true;
	
	if (self:GetNumRepliesInWindow(nil) >= self.overallNumRepliesPerWindow) then
		PrintDbg("SendReply> No reply:  Too many in overall window.");
		bNoReply = true;
		end
	
	if (self:GetNumRepliesInWindow( nCategory) >= self:GetNumRepliesPerWindowForCategory( nCategory)) then
		PrintDbg("SendReply> No reply:  Too many in category window.");
		bNoReply = true;
		end

	tmp = self:FindRecipient( nCategory, stRecip);
	if (tmp) then
		PrintDbg("recip time = "..tostring((GetTime()*1000) - self.arrReplies[tmp*3+3]).."   single = "..tostring(self.singleWindow));
		end
	if (tmp and ((GetTime()*1000) - self.arrReplies[tmp*3+3]<=self.singleWindow)) then
		PrintDbg("SendReply> No reply:  Recipient window.");
		bNoReply = true;
		end
	self.bDisablePurge = false;
	if (bNoReply) then return; end
	
	if (nCategory==DawnMod_Reply_Normal) then
		PrintDbg("Sending reply to: "..stRecip);
		SendChatMessage( stMsg, "WHISPER", nil, stRecip);
-- This one's for testing.
--		SendChatMessage( stMsg, "SAY", nil, nil);
		end
	if (nCategory==DawnMod_Reply_Battlenet) then
		bnId = tonumber( stRecip);
		PrintDbg("DawnModReply:SendReply >>  stRecipient = >"..stRecip.."< pId = "..tostring(bnId));
		if (bnId==nil) then PrintDbg("Couldn't extract bnId from >"..DawnMod:ShowReal(stRecipient).."<"); return; end
		if (bnId) then BNSendWhisper( bnId, stMsg); end
		end
	self:DidReply( nCategory, stRecip);
	end


