
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end
local function StringMatch(st1,st2) assertstr(st1);assertstr(st2);return (st1==st2); end



DawnModFilters = {};

local function FilterOptions()
	return DawnMod:CurConfig()[DawnModConfigItem_FilterOptions];
	end


function DawnModFilters:New(obj)
	obj = obj or {};

	setmetatable(obj,self);
    	self.__index   = self;
    	
	return obj
	end


function DawnModFilters:CompareOptionsLists( lista, listb)
	local	pos=1;
	local	stGroup=" ";
	local	vala, valb;
	
	asserttab(lista);
	asserttab(listb);
	while (stGroup~="") do
		stGroup = self:GetGroupByIndex( pos);
		if (stGroup~="") then
			vala = self:GetOption( stGroup, lista);
			valb = self:GetOption( stGroup, listb);
			if (vala ~= valb) then return false; end
			end
		pos=pos+1;
		end
	return true;
	end


function DawnModFilters:AssertOptions( list)
	local	pos=1;
	local	bOdds=true;
	
	if (type(list)~="table") then return false; end
	
	while(list[pos]) do
		if (bOdds) then
--			if ((type(list[pos])~="string") or (self:FindGroup(list[pos])==0) )then
-- For assertion purposes, we should be checking if the group exists, but I
-- don't want the user's configuration being reset if they mess with
-- 'DawnMod_Words.lua' which will happen if the assert routine finds a 'group'
-- which isn't in the word-list.
			if (type(list[pos])~="string") then
				return false;
				end
			end
		if (not bOdds) then
			if (type(list[pos])~="boolean") then
				return false;
				end
			end
		if (bOdds) then bOdds=false;
		else bOdds = true; end
		pos=pos+1;
		end
	if (not bOdds) then	-- odd number of items in the list
		return false;
		end
	return true;
	end


function DawnModFilters:CopyList( listIn)
	local	list = listIn;
	local	pos=1;
	local	newList = {}
	
	if (list==nil) then list = FilterOptions();end
	while (list[pos]) do
		newList[pos] = list[pos];
		pos=pos+1;
		end
	return newList;
	end


function DawnModFilters:FixCategoryName( stCat)
	assertstr(stCat)
	if (string.find(stCat,"@#")~=1) then return "@#"..stCat; end
	return stCat;
	end


function DawnModFilters:FixGroupName( stGroup)
	local	st = stGroup;
	assertstr(stGroup);
	while ((string.find(st,"@")==1) or (string.find(st,"#")==1) or (string.find(st,"!")==1)) do
		st=string.sub(st,2);
		end
	return "@@"..st;
	end


function DawnModFilters:GetNumCategories()
	local	pos=1;
	local	cnt=0;
	
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@#")==1) then
			cnt=cnt+1;
			end
		pos=pos+1;
		end
	return cnt;
	end


function DawnModFilters:FindGroup( stGroupIn)
	assertstr(stGroupIn);
	local	st = string.upper(self:FixGroupName( stGroupIn));
	local	pos=1;
	
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.upper(DawnMod_Words.arrMessageFilters[pos])==st) then
			return pos;
			end
		pos=pos+1;
		end
	return 0;
	end


function DawnModFilters:GetCategoryComment( stCatIn)
	assertstr( stCatIn);
	local	st=string.upper(self:FixCategoryName( stCatIn));
	local	pos;
	
	pos=self:FindCategory( st);
	if (pos==0) then return ""; end
	pos=pos+1;
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if ( string.find(DawnMod_Words.arrMessageFilters[pos],"@#") or
		     string.find(DawnMod_Words.arrMessageFilters[pos],"@@") ) then
			return "";
			end

		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@{")) then
			return string.sub(DawnMod_Words.arrMessageFilters[pos],3);
			end
		end
	return "";
	end


function DawnModFilters:GetGroupComment( stGroupIn)
	assertstr( stGroupIn);
	local	st=string.upper(self:FixGroupName( stGroupIn));
	local	pos;
	
	pos=self:FindGroup( st);
	if (pos==0) then return ""; end
	pos=pos+1;
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if ( string.find(DawnMod_Words.arrMessageFilters[pos],"@#") or
		     string.find(DawnMod_Words.arrMessageFilters[pos],"@@") ) then
			return "";
			end

		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@!")) then
			return string.sub(DawnMod_Words.arrMessageFilters[pos],3);
			end
		pos=pos+1;
		end
	return "";
	end


function DawnModFilters:GetCategoryByIndex( nIndex)
	local	pos = 1;
	local	cnt=0;
	
	assertnum(nIndex);
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@#")==1) then
			cnt=cnt+1;
			if (cnt==nIndex) then return string.sub(DawnMod_Words.arrMessageFilters[pos],3);end
			end
		pos=pos+1;
		end
	return "";
	end


function DawnModFilters:GetCategoryIndex( stCatIn)
	assertstr(stCatIn);
	local	pos = 1;
	local	catNo = 0;
	local	stCat = string.upper(self:FixCategoryName(stCatIn));
	
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos], "@#")==1) then
			catNo=catNo+1;
			if (string.upper(DawnMod_Words.arrMessageFilters[pos])==stCat) then
				return catNo;
				end
			end
		pos=pos+1;
		end
	return 0;
	end
			

function DawnModFilters:FindCategory( stCatIn)
	assertstr(stCatIn);
	local	pos = 1;
	local	stCat = string.upper(self:FixCategoryName(stCatIn));
	
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.upper(DawnMod_Words.arrMessageFilters[pos])==stCat) then
			return pos;
			end
		pos=pos+1;
		end
	return 0;
	end


function DawnModFilters:FindGroupInCategory( stCatIn, stGroupIn)
	assertstr(stCatIn);
	assertstr(stGroupIn);
	local	pos;
	local	stGroup = string.upper(self:FixGroupName(stGroupIn));
	
	pos = self:FindCategory( stCatIn);
	if (pos==0) then return 0; end
	pos=pos+1;
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@#")==1) then
			return 0;
			end
		if (string.upper(DawnMod_Words.arrMessageFilters[pos])==stGroup) then
			return pos;
			end
		pos=pos+1;
		end
	return 0;
	end


function DawnModFilters:GetGroupInCategoryByIndex( stCatIn, nIndex)
	local	pos, cnt;
	
	assertstr(stCatIn);
	assertnum(nIndex);
	pos = self:FindCategory(stCatIn);
	if (pos==0) then return "";end
	pos=pos+1;
	cnt=0;
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@#")==1) then
			return "";
			end
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@@")==1) then
			cnt=cnt+1;
			if (cnt==nIndex) then return string.sub(DawnMod_Words.arrMessageFilters[pos],3);end
			end
		pos=pos+1;
		end
	return "";
	end


function DawnModFilters:FindFilter( stCategory, stGroup, nIndex)
	local	pos, ofs;
	
	assertstr(stCategory);
	assertstr(stGroup);
	assertnum(nIndex);
	
	pos = self:FindGroupInCategory( stCategory, stGroup);
	if (pos==0) then return ""; end
	ofs=1;
	while( DawnMod_Words.arrMessageFilters[pos+ofs]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos+ofs],"@@")==1)then
			return "";
			end
		if ((string.find(DawnMod_Words.arrMessageFilters[pos+ofs],"@!")==1) or
		    (DawnMod_Words.arrMessageFilters[pos+ofs]=="{PLACEHOLDER}") )then
			ofs=ofs-1;
			pos=pos+1;
			end
		if (ofs==nIndex) then
			return DawnMod_Words.arrMessageFilters[pos+ofs];
			end
		ofs=ofs+1;
		end
	return "";
	end


function DawnModFilters:CheckMessageAgainstCategoryGroup( stMsgIn, stCategory, stGroup)
    local   cnt, stMsg, stFilter, pos;
    
    assertstr(stMsgIn);
    assertstr(stCategory);
    assertstr(stGroup);
    
    pos = self:FindGroupInCategory(stCategory, stGroup);
    if (pos == 0) then 
--        PrintDbg("FALSE1   cat:"..stCategory.." grp:"..stGroup.." msg = "..stMsgIn);
        return false; 
    end
    pos = pos + 1;
    
    stMsg = string.upper(stMsgIn);
    
    while (true) do
        stFilter = DawnMod_Words.arrMessageFilters[pos];
        if ((stFilter == nil) or (string.sub(stFilter,1,2)=="@@")) then
--            PrintDbg("FALSE2   cat:"..stCategory.." grp:"..stGroup.." msg = "..stMsgIn);
            return false; 
            end
            
        stFilter = string.upper(stFilter);
        
        if ( (stFilter~="") and
            (stFilter~="{PLACEHOLDER}") and
            (string.sub(stFilter,1,2)~="!@") ) then
                if (string.find(stMsg, stFilter) ~= nil) then 
--                PrintDbg("TRUE   cat:"..stCategory.." grp:"..stGroup.." msg = "..stMsgIn);
                return true; 
                end
            end
        pos = pos + 1;
        end
--    PrintDbg("FALSE3   cat:"..stCategory.." grp:"..stGroup.." msg = "..stMsgIn);
    return false;
    end


function DawnModFilters:FindFilterByGroup( stGroup, nIndex)
	assertstr(stGroup);
	assertnum(nIndex);
	local	pos, ofs;
	
	pos = DawnModFilters:FindGroup(stGroup);
	if (pos==0) then return ""; end
	ofs=1;
	while (DawnMod_Words.arrMessageFilters[pos+ofs]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos+ofs],"@@")==1)then
			return "";
			end
		if ((string.find(DawnMod_Words.arrMessageFilters[pos+ofs],"@!")==1) or
		    (DawnMod_Words.arrMessageFilters[pos+ofs]=="{PLACEHOLDER}") )then
			ofs=ofs-1;
			pos=pos+1;
			end
		if (ofs==nIndex) then
			return DawnMod_Words.arrMessageFilters[pos+ofs];
			end
		ofs=ofs+1;
		end
	return "";
	end


function DawnModFilters:GetGroupByIndex( nIndex)
	local	pos=1;
	local	group=0;
	
	assertnum(nIndex);
	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@@")==1) then
			group=group+1;
			if (group==nIndex) then
				return string.sub(DawnMod_Words.arrMessageFilters[pos],3);
				end
			end
		pos=pos+1;
		end
	return "";
	end


function DawnModFilters:FindOption( stGroup, tabOptionsList)
	local	pos=1;
	local	tabOptions;
	
	assertstr(stGroup);
	tabOptions = tabOptionsList;
	if (tabOptions==nil) then
		tabOptions = FilterOptions();
		end

	while (tabOptions[pos]) do
		if (tabOptions[pos]==stGroup) then
			return pos;
			end
		pos=pos+2;
		end
	return 0;
	end


function DawnModFilters:SetOption( stGroup, bNewValue, tabOptionsList)
	local	tabOptions, pos;
	
	assertstr(stGroup);
	assertbool(bNewValue);
	tabOptions = tabOptionsList;
	if (tabOptions==nil) then
		tabOptions = FilterOptions();
		end
	pos = self:FindOption( stGroup, tabOptions);
	
	if (bNewValue) then
		if (pos>0) then
			tabOptions[pos+1] = bNewValue;
		else	table.insert(tabOptions, stGroup);
			table.insert(tabOptions, bNewValue);
			end
	else	if (pos>0) then
			table.remove(tabOptions, pos);
			table.remove(tabOptions, pos);
			end
		end
	end


function DawnModFilters:GetOption( stGroup, tabOptionsList)
	local	tabOpt, pos;

	assertstr(stGroup);
	assert((type(tabOptionsList)=="table") or (tabOptionsList==nil));
	tabOpt = tabOptionsList;
	if (tabOpt==nil) then
		tabOpt = FilterOptions();
		end
	pos = self:FindOption( stGroup, tabOpt);
	if (pos>0) then
		return tabOpt[pos+1];
		end
	return false;
	end


function DawnModFilters:PrintOptions( tabOptionsList)
	local	tabOptions, pos;
	
	tabOptions = tabOptionsList;
	if (tabOptions==nil) then
		tabOptions = FilterOptions();
		end
	pos=1;
	while(tabOptions[pos]) do
		PrintDbg("  "..tostring(tabOptions[pos])..": "..tostring(tabOptions[pos+1]));
		pos=pos+2;
		end
	end

