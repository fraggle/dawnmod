
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end
local function StringMatch(st1,st2) assertstr(st1);assertstr(st2);return (st1==st2); end




local	arrGroupStrings	= {
	"@ANSWER",
	"@FRIEND",
	"@GUILD",
	"@PARTY",
	"@RAID"
	};


local	Known_Answer	= 1;
local	Known_Friend	= 2;
local	Known_Guild	= 3;
local	Known_Party	= 4;
local	Known_Raid	= 5;

DawnModWhitelist = {};

function DawnModWhitelist:New(obj)
	obj = obj or {};

	setmetatable(obj,self);
    	self.__index   = self;
    	
	obj.Known_Answer	= 1;
	obj.Known_Friend	= 2;
	obj.Known_Guild		= 3;
	obj.Known_Party		= 4;
	obj.Known_Raid		= 5;

	return obj
	end


function DawnModWhitelist:CompareWhitelists( lista, listb)
	
	return true;
	end


function DawnModWhitelist:AssertList( list)
	local	pos=1;
	local	resa, resb;
	
--	if (pos==1) then return true; end
	asserttab(list);
	while (list[pos]) do
		if (type(list[pos])~="string") then return false; end
		resa = string.find(list[pos],"@");
		resb = DawnModWhitelist:IsGroupString(list[pos]);
		
		if (resa and (resb==0) ) then
			return false;
			end
		pos=pos+1;
		end
	return true;
	end


function DawnModWhitelist:NumEntries( listIn)
	local	pos=1;
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	
	while(list[pos]) do
		pos=pos+1;
		end
	return (pos-1);
	end


function DawnModWhitelist:AlphaSort( listIn)
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	table.sort(list);
	end


function DawnModWhitelist:GetEntry( nIndex, listIn)
	local	pos = 1;
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	assertnum(nIndex);
	
	while (list[pos] and (pos<nIndex)) do
		pos=pos+1;
		end
	if ((pos==nIndex) and list[pos]) then return list[pos]; end
	return nil;
	end
	

function DawnModWhitelist:Begin( listIn)
	end


function DawnModWhitelist:Finish()
	end


function DawnModWhitelist:FindName( stName, listIn)
	local	pos= 1;
	local	stUprName;
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	
	assertstr(stName);
	stUprName = string.upper(stName);
	while (list[pos]) do
		if (string.upper(list[pos])==stUprName) then
			return pos;
			end
		pos=pos+1;
		end
	return 0;
	end


function DawnModWhitelist:RemoveName( stName, listIn)
	local	stUprName, pos;
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	
	assertstr(stName);
	stUprName=string.upper(stName);
	pos = self:FindName( stUprName);
	if (pos>0) then
		table.remove( list, pos);
		end
	end


function DawnModWhitelist:FixName( stName)
	local st, pos,tmp;
	
	assertstr(stName);
	st = stName;
	if (string.len(st)==0) then return nil;end
	st = DawnMod:StripPadding(st);
	if (string.find(st,"@") and (self:IsGroupString(st)==0)) then return nil;end
	if (self:IsGroupString(st)~=0) then return string.upper(st); end
	if (string.find(st,"[%d%s%c%z]")) then return nil; end
	pos = string.find(st,"-");
	if ((pos==1)or(pos==string.len(st))) then return nil;end
	if (pos) then
		tmp = string.sub(st,pos);
		st = string.sub(st,1,pos-1);
	else	tmp = "";
		end
	st = DawnMod:StrCapitalize(st)..tmp;
	st=DawnMod:GetLongName(st);
	return st;
	end
	

function DawnModWhitelist:AddName( stName, listIn)
	local	st;
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	
	assertstr(stName);
	
	if (self:FindName( st)==0) then return false; end
	st = self:FixName( st);
	if (st==nil) then return false; end
	table.insert(list,stUprName);
	return true;
	end


function DawnModWhitelist:IsGroupString( stName)
	local	stUprName;
	local	cnt=1;
	
	assertstr(stName);
	stUprName=string.upper(stName);
	while(arrGroupStrings[cnt]) do
		if (stUprName == arrGroupStrings[cnt]) then
			return cnt;
			end
		cnt=cnt+1;
		end
	return 0;
	end


function DawnModWhitelist:Clear( listIn)
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	table.wipe( list);
	end


function DawnModWhitelist:IsWhitelisted( stName, listIn)
	local	cnt = 1;
	local	stUprName;
	local	bKnownChecked = false;
	local	bAnswer,bFriend,bGuild,bParty,bRaid, wlGroup;
	local list=listIn;if(list==nil)then list=DawnMod:CurConfig()[DawnModConfigItem_Whitelist];end
	
	assertstr(stName);
	stUprName=string.upper(stName);
	while (list[cnt]) do
		wlGroup = self:IsGroupString(list[cnt]);
		if (wlGroup>0) then
			if (not bKnownChecked) then
				bAnswer,bFriend,bGuild,bParty,bRaid =
					DawnMod:Known():IsKnown( stName);
				bKnownChecked = true;
				end
			if ( ((wlGroup == Known_Answer) and bAnswer) or
			     ((wlGroup == Known_Friend) and bFriend) or
			     ((wlGroup == Known_Guild) and bGuild) or
			     ((wlGroup == Known_Party) and bParty) or
			     ((wlGroup == Known_Raid) and bRaid) ) then
				return true;
				end
		else
			if (stUprName==string.upper(list[cnt])) then return true; end
			end
		cnt=cnt+1;
		end
			
	return false;
	end

