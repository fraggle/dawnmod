## Interface: 30300
## Title: DawnMod
## Notes: A chat filter to get rid of spam and make the world a better place.
## Author: Dawn
## Version: 1.20
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: DawnModDB
## X-Category: Chat/Communication

DawnMod_Config.lua
DawnMod_MiniMap.lua
DawnMod_ChanChg.lua
DawnMod_Filters.lua
DawnMod_Known.lua
DawnMod_MsgCache.lua
DawnMod_PosterCache.lua
DawnMod_Reply.lua
DawnMod_Words.lua
DawnMod_Whitelist.lua
DawnMod.lua
DawnMod_CmdLine.lua
DawnMod_Upgrade.lua
