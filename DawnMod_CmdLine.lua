
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end
local function StringMatch(st1,st2) assertstr(st1);assertstr(st2);return (st1==st2); end


local function StRemoveX( stSrc, stRem)
	local	stOut, stPre, stPost, pos, bMore;

	assertstr(stSrc);
	assertstr(stRem);
	bMore=true;
	stOut = stSrc;
	while (bMore) do
		pos = string.find(stOut, stRem);
		if (pos==nil) then bMore = false;
		else	if (pos==1) then stPre="";
			else	stPre=string.sub(stOut,1,pos-1);
				end
			pos = pos+string.len(stRem);
			if (pos==string.len(stOut)) then stPost="";
			else	stPost = string.sub(stOut,pos);
				end
			stOut=stPre..stPost;
			end
		end
	return stOut;
	end


local function ChIsAlpha( ch) return ( ((ch>="a")and(ch<="z")) or ((ch>="A")and(ch<="Z")) ); end

local function ChIsNum( ch) return ((ch>="0")and(ch<="9")); end




-- slash handler error message types
local	ShErrType_Numeric	= 1;
local	ShErrType_TrueFalse	= 2;
local	ShErrType_String	= 3;



local	AS_Achievement		= 1;
local	AS_Statistic		= 2;
local	AS_GuildAchievement	= 3;

local   GtType_Whitespace       =  9;

local	arrCmdLine	= {
	"Enable","Enables this mod.",
	"Disable","Disables this mod.",
	"Reset","Resets the configuration to default.",
	"Config","Displays the current configuration.",
	"TestMode","Toggles test mode.",

	"AllowCaps [Y/N]","Should ALL CAPS be allowed.",
    "BlockChannelInvites [Y/N]","Should chat channel invites be blocked.",
	"BlockGlobalAscii [Y/N]","Should ascii 'art' be blocked.",
	"BlockGlobalForeign [Y/N]","Should foreign language text.",
	"BlockGlobalMultiline [Y/N]","Should multi-line spam be blocked.",
	"BlockRaidZoneGeneral [Y/N]","Blocks the 'general' channel when in a raid zone.",
	"BlockPartyZoneGeneral [Y/N]","Blocks the 'general' channel when in a party zone.",
	"BlockRecount [Y/N]","Blocks recount output.",
	"BlockRecountExceptParty [Y/N]","Block recount except in party.",
	"BlockRecountExceptRaid [Y/N]","Blocks recount except in raid.",
	"BlockWhispers [Y/N]","Blocks all whispers.",
	"BlockWhispersExceptAnswers [Y/N]","Ignores 'BlockWhispers' for people you've whispered.",
	"BlockWhispersExceptFriends [Y/N]","Ignores 'BlockWhispers' for friends.",
	"BlockWhispersExceptGuild [Y/N]","Ignores 'BlockWhispers' for guildmates.",
	"BlockWhispersExceptParty [Y/N]","Ignores 'BlockWhispers' for party members.",
	"BlockWhispersExceptRaid [Y/N]","Ignores 'BlockWhispers' for raid members.",
	"CommunicationEnabled [Y/N]","Allows "..DawnMod_IdString.." to communicate with other users.",
	"FixCaps [Y/N]","Fixes ALL CAPS..or just ignores them.",
	"FixChannelChanged [Y/N]","Fix the 'channel changed' messages.",
    "FixChatStickies [Y/N]","Fix which chat channels are sticky",
	"FixFastTrack [Y/N]","Fix loot messages with 'fast track' or 'cash flow' active.",
	"FixSPelling [Y/N]","Should spelling be fixed.",
	"FixSWearing [Y/N]","Should swearing be fixed.",
	"GetAchievement [name]","Lists the achievement.",
	"GetAttune","Lists your character's current level of attunement",
	"GetStatistic [name]","Lists the statistic.",
	"InviteAcceptAnswers [num]","Accept invites from people you've whispered.",
	"InviteAcceptAnswersNotify [Y/N]","Notify you about invites from people you've whispered.",
	"InviteAcceptFriends [num]","Accept invites from friends.",
	"InviteAcceptFriendsNotify [Y/N]","Notify you about invites from friends.",
	"InviteAcceptGuild [num]","Accept invites from guild members.",
	"InviteAcceptGuildNotify [Y/N]","Notify you about invites from guild members.",
	"InviteAcceptUnknown [num]","Accept invites from unknown people.",
	"InviteAcceptUnknownNotify [Y/N]","Notify you about invites from unknown people.",
	"InviteAcceptWhitelist [num]","Accept invites from whitelisted people.",
	"InviteAcceptWhitelistNotify [Y/N]","Notify you about invites from whitelisted.",
	"KarmaUse [Y/N]","Should karma be used.",
	"KarmaIgnore [Y/N]","Ignore people who get low karma.",
	"KarmaSave [Y/N]","Store karma data between session.",
	"KarmaSuspendGRoup [Y/N]","Suspend karma handling for group members.",
	"KarmaSuspendGUild [Y/N]","Suspend karma handling for guild chat.",
	"LockOutMessageTime [num secs]","Minimum secs between repeated messages.",
	"LockOutMessageGlobalOnly [Y/N]","Lockout messages by time for global channels only.",
	"LockOutPosterTime [num secs]","Minimum secs between posts from one user.",
	"LockOutPosterGlobalOnly [Y/N]","Lockout poster by time for global channels only.",
	"MessageFilterNpc [Y/N]","Should NPC chatter be filtered.",
	"MessageFilterSystem [Y/N]","Should system messages be filtered",
	"PosterCacheLength [num]","Number of posters to remember.",
	"PosterCachePurgeDays [num]","Number of days before poster info gets purged.",
	"PosterCacheSave [num]","Should the poster chace be saved between sessions.",
	"ShowCombatFlags [Y/N]","Show combat flags",
	"ShowMinimapButton [Y/N]","Should the minimap button be visible.",
	"SpamAnnoy [Y/N]","Should annoying stuff be blocked.",
	"SpamGOld [Y/N]","Should gold spam be blocked.",
	"SpamGUild [Y/N]","Should guild spam be blocked.",
	"SpamLink [Y/N]","Should link spam be blocked.",
	"SpamMod [Y/N]","Should mod spam be blocked.",
	"StripGlobalChannelRaidMarks [Y/N]","Should raid marks be stripped",
	"StripGlobalUrls [Y/N]","Should URLs blocked.",
	"StripPlayerFlags [Y/N]","Should player flags (AFK & DND) be stripped.",
	"TrapWhispersCombatOnly [Y/N]","Only does TRAPPARTY or TRAPRAID if you're in combat.",
	"TrapWhispersParty [Y/N]","Traps whispers while you're in a party.",
	"TrapWhispersRaid [Y/N]","Traps whispers while you're in a raid.",
	"TrapWhisperReplyOnlyKnow [Y/N]","Only auto-replies to 'known' senders",
	"UpgradeNotificationEnabled [Y/N]","Tells you if there's an upgrade available."
	};


local	shd = {
	bHandled,
	numArgs,
	arrArgs
	};




local function ShowMsgCacheEntry( nIndex, bReal)
	local	str, rbTime, rbString;

	assert(type(nIndex)=="number");
	assert(type(bReal)=="boolean");
	rbString = DawnMod:MessageCache():GetEntryString( nIndex);
	rbTime = DawnMod:MessageCache():GetEntryTime( nIndex);
	if (rbTime==nil) then return false;
	else	if (bReal) then
			rbString = DawnMod:ShowReal(rbString);
			end
		PrintMsg("#"..tostring(nIndex).." ("..tostring(rbTime).."):  "..rbString);
		return true;
		end
	return false;
	end


local function ShowMsgCacheEntriesRealByName( stName)
	local	nIndex;

	assert(type(stName)=="string");
	nIndex=0;
	stUpperName=string.upper(stName);
	for nIndex=0,DawnMod:MessageCache():Length()-1 do
		if (string.find(string.upper(DawnMod:MessageCache():GetEntryString(nIndex)), stUpperName)==1) then
			ShowMsgCacheEntry( nIndex, true);
			end
		end
	end


local function ShowNearestMsgCacheEntryReal( nFindTime)
	assert(type(nFindTime)=="number");
	ShowMsgCacheEntry( DawnMod:MessageCache():FindNearestEntryByTime( nFindTime),true);
	end


local function MsgCacheShow()
	local nIndex;

	for nIndex=0,DawnMod:MessageCache():Length()-1 do
		ShowMsgCacheEntry( nIndex, false);
		end
	end


local function CompareAchievementLinks(a,b)
	return string.sub(a,string.find(a,"%[")+1) <
		string.sub(b,string.find(b,"%[")+1);
	end


local function GetFixedAchievementNameText( stAch)
	local	st, stPre, stPost;

	st = stAch;
	StRemoveX(st,"(10 Player)");
	StRemoveX(st,"(25 Player)");
	return st;
	end


local function GetAchievementList( stAchievementName, achType)
	local	stAchUpr,cats,catnum, numAchs, achnum, achId,achName, output,
		tempNum, count;

	stAchUpr=string.upper(stAchievementName);
	output = {};
	if (achType == AS_Achievement) then cats = GetCategoryList(); end
	if (achType == AS_Statistic) then cats = GetStatisticsCategoryList(); end
	if (achType == AS_GuildAchievement) then cats = GetGuildCategoryList(); end
	catnum = 1;
	count=0;
	while (cats[catnum]~=nil) do
		numAchs = GetCategoryNumAchievements(cats[catnum])
		for achnum=1,numAchs do
			achId,achName =GetAchievementInfo( cats[catnum],achnum);
			if (string.find( GetFixedAchievementNameText(string.upper(achName)),stAchUpr)) then
				if (achType==AS_Statistic) then
					tempNum = GetStatistic(achId);
					if (not ChIsNum(string.sub(tempNum,1,1))) then
						tempNum="0";
						end
					table.insert(output,achName..": "..tempNum);
				else	table.insert(output,GetAchievementLink(achId));
					end
				count=count+1;
				end
			end
		bFirst = false;
		catnum = catnum+1;
		end
	if (achType == AS_Statistic) then
		table.sort(output);
	else	table.sort(output,CompareAchievementLinks);
		end;
	return count,output;
	end


local function GetAchievementByName( stAchievementName, achType, careAboutHeroic, wantHeroicAch)
	local	stAchUpr,cats,catnum, numAchs, achnum, achId,achName, output,
		tempNum, count, s, isHeroic;

	stAchUpr=string.upper(stAchievementName);
	if (achType == AS_Achievement) then cats = GetCategoryList(); end
	if (achType == AS_Statistic) then cats = GetStatisticsCategoryList(); end
	if (achType == AS_GuildAchievement) then cats = GetGuildCategoryList(); end
	catnum = 1;
	count=0;
	output = {};
	while (cats[catnum]~=nil) do
		numAchs = GetCategoryNumAchievements(cats[catnum])
		for achnum=1,numAchs do
			achId,achName,point,completed =GetAchievementInfo( cats[catnum],achnum);
			s = string.find(string.upper(achName),stAchUpr);
			isHeroic = (string.find(string.upper(achName),"HEROIC") ~= nil);
			if ((s ~= nil) and ((not careAboutHeroic) or (isHeroic == wantHeroicAch))) then
				table.insert(output, completed);
				table.insert(output, achId);
				count=count+1;
			end
		end
		bFirst = false;
		catnum = catnum+1;
	end
	return count,output;
end


function DawnMod:PrintAttunes()
	local nax10, nax25, uld10, uld25, toc10, toc25, icc10, icc25, ach10, ach25,
		achnax10,achnax25,achuld10,achuld25,achtoc10,achtoc25,achicc10,achicc25,
		st,cnt,achList,numAch,st, ten, twentyfive;

	numAch,achList = GetAchievementByName("axxramas",AS_Achievement, true, false);
	nax10 = achList[1];
	achnax10 = achList[2];
	nax25 = achList[3];
	achnax25 = achList[4];

	numAch,achList = GetAchievementByName("the secrets of uld",AS_Achievement, true, false);
	uld10 = achList[1];
	achuld10 = achList[2];
	uld25 = achList[3];
	achuld25 = achList[4];

	numAch,achList = GetAchievementByName("call of the cru",AS_Achievement, true, false);
	toc10 = achList[1];
	achtoc10 = achList[2];
	toc25 = achList[3];
	achtoc25 = achList[4];

	numAch,achList = GetAchievementByName("fall of the lich k",AS_Achievement, true, false);
	icc10 = achList[1];
	achicc10 = achList[2];
	icc25 = achList[3];
	achicc25 = achList[4];

	-- PrintMsg("nax10: "..GetAchievementLink(achnax10));
	-- PrintMsg("uld10: "..GetAchievementLink(achuld10));
	-- PrintMsg("toc10: "..GetAchievementLink(achtoc10));
	-- PrintMsg("icc10: "..GetAchievementLink(achicc10));

	-- PrintMsg("nax25: "..GetAchievementLink(achnax25));
	-- PrintMsg("uld25: "..GetAchievementLink(achuld25));
	-- PrintMsg("toc25: "..GetAchievementLink(achtoc25));
	-- PrintMsg("icc25: "..GetAchievementLink(achicc25));


	if (nax10) then
		ten = "Uld";
		ach10 = achuld10;
	else
		ten = "Nax";
		ach10 = achnax10;
	end
	if (uld10) then
		ten = "ToC";
		ach10 = achtoc10;
	end
	if (toc10) then
		ten = "ICC";
		ach10 = achicc10;
	end
	if (icc10) then
		ten = "RS";
		ach10 = nil;
	end

	if (nax25) then
		twentyfive = "Uld";
		ach25 = achuld25;
	else
		twentyfive = "Nax";
		ach25 = achnax25;
	end
	if (uld25) then
		twentyfive = "ToC";
		ach25 = achtoc25;
	end
	if (toc25) then
		twentyfive = "ICC";
		ach25 = achicc25;
	end
	if (icc25) then
		twentyfive = "RS";
		ach25 = nil;
	end

	PrintMsg("You are attuned to: "..ten.."10 and "..twentyfive.."25");
	if (ach10 ~= nil) then
		PrintMsg("10 player achievement needed: "..GetAchievementLink(ach10));
	end
	if (ach25 ~= nil) then
		PrintMsg("25 player achievement needed: "..GetAchievementLink(ach25));
	end

end



local function GetTocVersionString()
	local ver,build,date,tocver;
	ver,build,date,tocver = GetBuildInfo();
	return tocver;
	end


local function PrintConfigItem( configNum, configToPrint, cfgIn)
	local	str, cnt, numPadding;

	assertnum(configNum);
	assertnum(configToPrint);
	asserttab(cfgIn);
	if ((configNum~=0) and (configNum~=configToPrint)) then return; end

	str="";
	str=DawnMod:GetConfigDataValue( configToPrint, DawnModConfigDataType_TextDescription)..": ";
	if (configNum==0) then
		numPadding= 5;
		for cnt=0,numPadding do
			str=str.." ";
			end
		end
	DawnMod:PrintInfoItem(str, tostring(cfgIn[configToPrint]));
	end


function DawnMod:PrintConfig( configNum, cfgIn, bPrintHeaderIn)
	local	cfg, index, numCfg, bPrint, bPrintHeader;

	cfg = cfgIn;
	if (cfg==nil) then cfg = DawnMod:CurConfig(); end

	if ((bPrintHeaderIn==nil) or (type(bPrintHeaderIn)~="boolean")) then
		bPrintHeader=true;
	else	bPrintHeader=bPrintHeaderIn;
		end

	numCfg = DawnMod:CfgGetNumDatas();

	if (bPrintHeader) then DawnMod:PrintHeader(); end

        for index=2,numCfg do
        	bPrint=true;
        	if ( (index==DawnModConfigItem_FilterOptions) or
        		(index==DawnModConfigItem_Whitelist) ) then
        		bPrint=false;
        		end
        	if (bPrint) then PrintConfigItem( configNum,index,cfg); end
        	end
	end






function DawnMod:PrintAchievementList( stAchievementName, achType)
	local numAch, achList, cnt, stWord, stS, stHave, stAch;

	numAch, achList = GetAchievementList( string.upper(stAchievementName), achType);
	if (achType==AS_Statistic) then stWord = "statistic";end
	if (achType==AS_Achievement) then stWord = "achievement"; end
	if (achType==AS_GuildAchievement) then stWord = "guild achievement";end
	if (numAch == 0) then stHave = "No ";
	else	stHave = numAch; end
	if (numAch == 1) then stS = "";
	else	stS = "s";end

	DawnMod:PrintHeader();
	PrintMsg(stHave.." "..stWord..stS.." found matching '"..stAchievementName.."'.");
	for cnt=1,numAch do
		PrintMsg("#"..tostring(cnt)..": "..achList[cnt]);
		end
	end


local function InitArgs( stArgs)
	local   cnt, numArgs, args, bDone, stToken,tokenType,firstWord;

	args = {};

	for cnt=0,10 do	args[cnt]="";end

	cnt     = 0;
	numArgs = 0;
	bDone = false;
	firstWord=1;

	while (not bDone) do
		stToken,tokenType,cnt=DawnMod:GetToken( stArgs, cnt);
		if (stToken~=nil) then
			if (tokenType~=GtType_Whitespace) then
				if (firstWord==1) then
					numArgs=numArgs+1;
					args[numArgs] = "";
					firstWord=0;
					end
				args[numArgs]=args[numArgs]..stToken;
			else	firstWord=1;
				end
		else	bDone = true;
			end
		end
	return numArgs,args;
	end


local function DisplayShortHelp( stMsgIn)
	assertstr(stMsgIn);
	DawnMod:PrintHeader();
	if (stMsgIn==nil) then PrintMsg("Messages: "..DawnMod:ChatMsgCountString( msgCountData.msgPassTotal, msgCountData.msgTotal, GetTime()-msgCountData.timeTotal)); end
	PrintMsg("Use /dm help  or /dm ?   for a command list");
	if (stMsgIn ~= nil) then DawnMod:PrintInfoItem("Unknown command: ",stMsgIn); end
	end




local function SH_GetTF( argstr)
	local bTrueOrFalse, bTrue;

	bTrueOrFalse = (StringMatch(argstr,"ON") or
			StringMatch(argstr,"OFF") or
			StringMatch(argstr,"TRUE") or
			StringMatch(argstr,"FALSE") or
			StringMatch(argstr,"YES") or
			StringMatch(argstr,"NO") or
			StringMatch(argstr,"Y") or
			StringMatch(argstr,"N") or
			StringMatch(argstr,"1") or
			StringMatch(argstr,"0")
			);
	bTrue =(StringMatch(argstr,"ON") or
		StringMatch(argstr,"TRUE") or
		StringMatch(argstr,"YES") or
		StringMatch(argstr,"Y") or
		StringMatch(argstr,"1")
		);
	return bTrueOrFalse, bTrue;
	end


local function SH_ErrorMsg( stOpt, errcode, minNum, maxNum)
	local	stErr, stNum;

	PrintDbg("err = "..tostring(errcode));
	if (errcode == ShErrType_Numeric) then stErr="numeric value";end
	if (errcode == ShErrType_TrueFalse) then stErr="true/false";end
	if (errcode == ShErrType_String) then stErr="string";end

	stNum="";
	if (minNum~=nil) then stNum = " ("..tostring(minNum).." <= number <= "..tostring(maxNum)..")"; end
	DawnMod:PrintHeader();
	DawnMod:PrintInfoItem( stOpt.." requires a ["..stErr.."] argument.".. stNum);
	shd.bHandled = true;
	end


local function SH_HandleTF( cfgNo, str1,str2,str3,str4,str5)
	local	bPrintCfg, newCfgValue, bPass;

	newCfgValue = cfgValue;
	bPass = false;

	if ((str1~=nil)and StringMatch(str1,shd.arrArgs[1])) then bPass=true;end
	if ((str2~=nil)and StringMatch(str2,shd.arrArgs[1])) then bPass=true;end
	if ((str3~=nil)and StringMatch(str3,shd.arrArgs[1])) then bPass=true;end
	if ((str4~=nil)and StringMatch(str4,shd.arrArgs[1])) then bPass=true;end
	if ((str5~=nil)and StringMatch(str5,shd.arrArgs[1])) then bPass=true;end
	if (not bPass) then return; end
	shd.bHandled = true;

	bPrintCfg = true;
	if (shd.numArgs>1) then
		bTrueOrFalse, bTrue = SH_GetTF(shd.arrArgs[2]);
		if (bTrueOrFalse) then
			DawnMod:CfgSetValue( cfgNo, bTrue);
		else	SH_ErrorMsg(shd.arrArgs[1],ShErrType_TrueFalse);
			bPrintCfg = false;
			end
		end
	if (bPrintCfg) then DawnMod:PrintConfig(cfgNo); end
	end




local function SH_HandleNum( cfgNo, valmin,valmax, str1,str2,str3,str4,str5)
	local	bPrintCfg, cfgval, bPass;

	bPass = false;

	if ((str1~=nil)and StringMatch(str1,shd.arrArgs[1])) then bPass=true;end
	if ((str2~=nil)and StringMatch(str2,shd.arrArgs[1])) then bPass=true;end
	if ((str3~=nil)and StringMatch(str3,shd.arrArgs[1])) then bPass=true;end
	if ((str4~=nil)and StringMatch(str4,shd.arrArgs[1])) then bPass=true;end
	if ((str5~=nil)and StringMatch(str5,shd.arrArgs[1])) then bPass=true;end
	if (not bPass) then return; end

	shd.bHandled = true;
	bPrintCfg=true;

	if (shd.numArgs>1) then
		cfgval=tonumber(shd.arrArgs[2]);
		if ((cfgval==nil)or(cfgval<valmin)or(cfgval>valmax)) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_Numeric,valmin,valmax);
			bPrintCfg=false;
		else	DawnMod:CfgSetValue( cfgNo, cfgval);
			end
		end
	if (bPrintCfg) then DawnMod:PrintConfig(cfgNo);end
	end


function DawnMod:PrintPosterList( stPartName)
	local	index, stName,karmaScore, lastSeen;

	PrintMsg("PosterCache entries: "..tostring(DawnMod:PosterCache():GetNumEntries()).."  (def score: "..tostring(DawnMod:PosterCache():GetDefaultScore())..")");
	if (DawnMod:PosterCache():GetNumEntries()==0) then return end
	for index=1, DawnMod:PosterCache():GetNumEntries() do
		stName,karmaScore,lastSeen=DawnMod:PosterCache():GetEntry(index);
		if ((stPartName=="") or (string.find(stName,stPartName)==1) or (string.find(string.sub(stName,string.find(stName,"-")+1),stPartName)==1)) then
			PrintMsg("#"..tostring(index)..": "..tostring(stName).." : "..tostring(karmaScore).." : "..tostring(lastSeen));
			end
		end
	end




local function ShDoKarmaMsg( index)
	local	stName,curScore;

	stName,curScore = DawnMod:PosterCache():GetEntry(index);
	PrintMsg("#"..tostring(index).." "..stName.." = "..tostring(curScore));
	end


local function ShDoKarma( nameOrNo, stScoreChange)
	local	index, bByName, stUpr, scoreChange;

	scoreChange = tonumber(stScoreChange);
	bByName = true;
	index = tonumber(nameOrNo);
	if (index~=nil) then bByName=false;end

	if (bByName) then
		stUpr=string.upper(nameOrNo);
		for index=0,DawnMod:PosterCache():GetNumEntries()-1 do
			if (string.find(DawnMod:GetShortName(DawnMod:PosterCache():GetEntry(index)),stUpr)==1) then
				DawnMod:PosterCache():ChangeScoreByIndex( index, scoreChange);
				ShDoKarmaMsg( index);
				end
			end
	else	DawnMod:PosterCache():ChangeScoreByIndex(index,scoreChange);
		ShDoKarmaMsg( index);
		end
	end



function DawnMod:SlashHandlerExplain( stMsgIn)
	local	stMsgUpr, index, bFound, foundMsg;

	stMsgUpr	= string.upper( stMsgIn);
	foundMsg	= "";

	index = 1;
	while ((foundMsg=="") and (DawnMod_Words.arrAllowedCaps[index]~=nil) ) do
		if (StringMatch(DawnMod_Words.arrAllowedCaps[index], stMsgUpr) and
		    (DawnMod_Words.arrAllowedCaps[index+1]~="") ) then
		    	foundMsg = DawnMod_Words.arrAllowedCaps[index+1];
			end
		index=index+2;
		end

	index = 1;
	while ((foundMsg=="") and (DawnMod_Words.arrMixed[index]~=nil) ) do
		if (StringMatch(string.upper(DawnMod_Words.arrMixed[index]), stMsgUpr) and
		    (DawnMod_Words.arrMixed[index+1]~="") ) then
		    	foundMsg = DawnMod_Words.arrMixed[index+1];
			end
		index=index+2;
		end

	DawnMod:PrintHeader();
	DawnMod:PrintInfoItem("Explain:  "..string.upper(stMsgIn));
	if (foundMsg ~= "") then
		DawnMod:PrintInfoItem("",foundMsg);
	else	if (stMsgIn=="") then
		else	DawnMod:PrintInfoItem("No match found for '"..stMsgIn.."'.");
			end
		end
	end

local function ShowWhitelist()
	local	listlen, stName,pos, st;

	listLen = DawnModWhitelist:NumEntries();
	if (listLen==1) then st="entry"; else st = "entries"; end
	DawnMod:PrintInfoItem(tostring(listLen).." "..st.." in the whitelist.");
	DawnModWhitelist:AlphaSort();
	pos=1;
	while(pos<=listLen) do
		DawnMod:PrintInfoItem("", DawnModWhitelist:GetEntry(pos));
		pos=pos+1;
		end
	end


local function DoMessageFilterOption()
	local	catNum,groupNum, stCat,stGroup, bTF, bTrue;

	if ((shd.numArgs>1) and StringMatch(shd.arrArgs[2],"SHOW")) then
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("- Message Filters -");
		catNum=1;
		stCat=" ";
		while (stCat~="") do
			stCat=DawnModFilters:GetCategoryByIndex( catNum);
			if (stCat~="") then
				DawnMod:PrintInfoItem("","-"..stCat);
				stGroup=" ";
				groupNum=1;
				while(stGroup~="") do
					stGroup=DawnModFilters:GetGroupInCategoryByIndex( stCat, groupNum);
					if (stGroup~="") then
						DawnMod:PrintInfoItem(stGroup..": ",tostring(DawnModFilters:GetOption( stGroup)));
						end
					groupNum=groupNum+1;
					end
				end
			catNum=catNum+1;
			end
		return;
		end

	if (shd.numArgs>1) then
		groupNum = DawnModFilters:FindGroup( shd.arrArgs[2]);
		if (groupNum>0) then
			stGroup = string.sub(DawnMod_Words.arrMessageFilters[groupNum],2)
			end
		if (groupNum==0) then
			DawnMod:PrintHeader();
			DawnMod:PrintInfoItem("Invalid group name: ",shd.arrArgs[2]);
			return;
			end
		if (shd.numArgs==2) then
			DawnMod:PrintHeader();
			DawnMod:PrintInfoItem(
				stGroup..": ",
				tostring(DawnModFilters:GetOption( stGroup)));
			return;
			end
		if (shd.numArgs==3) then
			bTF, bTrue = SH_GetTF(shd.arrArgs[3]);
			if (not bTF) then
				SH_ErrorMsg( "'Message Filter' arg #3", ShErrType_TrueFalse);
				return
				end
			DawnModFilters:SetOption( stGroup, bTrue);
			DawnMod:PrintHeader();
			DawnMod:PrintInfoItem(
				stGroup.." is now ",
				tostring(DawnModFilters:GetOption( stGroup)));
			return
			end
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("Invalid number of arguments.");
		end
	end--fucntion



function DawnMod:SlashHandler(stMsgIn)
	local stMsg, bTrueFalse, bTrue, tempNum, bIsNum;

	shd.bHandled        = false;
	shd.numArgs, shd.arrArgs = InitArgs( string.upper(stMsgIn));

--	for tempNum=1,shd.numArgs do PrintMsg("#"..tostring(tempNum)..": >"..shd.arrArgs[tempNum].."<"); end

	if (StringMatch(shd.arrArgs[1],"ENABLE") or StringMatch(shd.arrArgs[1],"DISABLE")) then
		shd.bHandled = true;
		DawnMod:CfgSetValue(DawnModConfigItem_Enabled, StringMatch(shd.arrArgs[1],"ENABLE") );
		DawnMod:PrintConfig(DawnModConfigItem_Enabled);
		return end

	if (not DawnMod:CfgGetValue(DawnModConfigItem_Enabled)) then
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem(DawnMod_IdString.." is disabled.  /DM ENABLE to enable it again.");
		return;end

	if (DawnMod_ConfigWindowVisible) then
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("Slash commands are disabled while the configuration window is open.");
		return end

	if (shd.numArgs == 0) then
		DawnMod:ShowConfigWindow();
		return;end

	if (StringMatch(shd.arrArgs[1],"TW")) then
		shd.bHandled = true;
		DawnMod:PrintCombatWhispers( false);
		end

	if (StringMatch(shd.arrArgs[1],"TWS")) then
		shd.bHandled = true;
		DawnMod:PrintCombatWhispers(true);
		end

	if (StringMatch(shd.arrArgs[1],"TWCOUNT") or StringMatch(shd.arrArgs[1],"TWC")) then
		shd.bHandled = true;
		if (numCombatWhispers==1) then tempNum="";else tempNum="s";end
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("There are "..tostring(numCombatWhispers).." whisper"..tempNum.." trapped.");
		end

	if (StringMatch(shd.arrArgs[1],"CHECKPARSE")) then
		shd.bHandled = true;
		if (shd.numArgs == 1) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_String);
		else
			DawnMod:CheckParse( DawnMod:StripPadding(string.sub( stMsgIn,string.len(shd.arrArgs[1])+1)));
			end
		end

	if (StringMatch(shd.arrArgs[1],"CHECKSF")) then
		shd.bHandled = true;
		if (shd.numArgs == 1) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_String);
		else
			tempNum = DawnMod:StripPadding(string.sub(stMsgIn,string.len(shd.arrArgs[1])+1));
			PrintMsg("Checking: "..tempNum);
			PrintMsg("Output: "..DawnMod:SwearFilter( tempNum));
			end
		end

	if (StringMatch(shd.arrArgs[1],"DAWNSET")) then
		shd.bHandled = true;
		bTrue = DawnMod:IsDebug();
		DawnMod:CfgReset( DawnMod:CurConfig(), false, false);
		for cnt=1,DawnMod:CfgGetNumDatas() do
			if (DawnMod:GetConfigDataValue( cnt,DawnModConfigDataType_AssertInfo)~="T") then
				DawnMod:CfgSetValue(cnt, DawnMod:GetConfigDataValue(cnt,DawnModConfigDataType_DawnSet));
				end
			end
		DawnMod:SetDebug( bTrue);
		DawnMod:CfgChanged();
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("Dawnset activated.");
		end

	if (StringMatch(shd.arrArgs[1],"DOTEST")) then
		shd.bHandled = true;
		local args = {};
		for tempNum=1,10 do
			args[tempNum]=shd.arrArgs[tempNum+1];
			end
		DawnMod:DoTest( args);
		end

	if (StringMatch(shd.arrArgs[1],"PCLIST")) then
		shd.bHandled=true;
		tmpNum="";
		if (shd.numArgs>=2) then
			tmpNum = shd.arrArgs[2];
			end
		DawnMod:PrintPosterList(string.upper(tmpNum));
		end

	if (StringMatch(shd.arrArgs[1],"KARMACHANGE")) then
		shd.bHandled=true;
		if ((shd.numArgs<3) or (tonumber(shd.arrArgs[3])==nil)) then
			DawnMod:PrintHeader();
			DawnMod:PrintInfoItem(shd.arrArgs[1]," requires [number/name] [scoreChange]");
		else	ShDoKarma( shd.arrArgs[2], shd.arrArgs[3]);
			end
		end

	if (StringMatch(shd.arrArgs[1],"PCWIPE")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("Poster cache wiped.");
		DawnMod:PosterCache():Wipe();
		end

	if (StringMatch(shd.arrArgs[1],"GETATTUNE") or StringMatch(shd.arrArgs[1],"GETATT")) then
		shd.bHandled=true;
		DawnMod:PrintAttunes();
	end


		if (StringMatch(shd.arrArgs[1],"GETACH") or StringMatch(shd.arrArgs[1],"GETACHIEVEMENT") or StringMatch(shd.arrArgs[1],"GA")) then
		shd.bHandled=true;
		if (shd.numArgs==1) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_String);
		else	DawnMod:PrintAchievementList(string.sub(stMsgIn, string.len(shd.arrArgs[1])+2),AS_Achievement);
			end
		end

	if (StringMatch(shd.arrArgs[1],"GETSTAT") or StringMatch(shd.arrArgs[1],"GETSTATISTIC") or StringMatch(shd.arrArgs[1],"GS")) then
		shd.bHandled=true;
		if (shd.numArgs==1) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_String);
		else	DawnMod:PrintAchievementList(string.sub(stMsgIn, string.len(shd.arrArgs[1])+2),AS_Statistic);
			end
		end

	if (StringMatch(shd.arrArgs[1],"GETGACH") or StringMatch(shd.arrArgs[1],"GETGUILDACHIEVEMENT") or StringMatch(shd.arrArgs[1],"GGA")) then
		shd.bHandled=true;
		if (shd.numArgs==1) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_String);
		else	DawnMod:PrintAchievementList(string.sub(stMsgIn, string.len(shd.arrArgs[1])+2),AS_GuildAchievement);
			end
		end

	if (StringMatch(shd.arrArgs[1],"CFG") or StringMatch(shd.arrArgs[1],"CONFIG")) then
		shd.bHandled = true; DawnMod:PrintConfig(0);
		end

	if (StringMatch(shd.arrArgs[1],"TOC")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("TOC Interface:",GetTocVersionString());
		end

	if (StringMatch(shd.arrArgs[1],"RESET")) then
		tempNum = DawnMod:IsDebug()
		DawnMod:SetCurConfig( DawnMod:CfgReset( DawnMod:CurConfig(), true, true));
		DawnMod:SetDebug( tempNum);
		shd.bHandled = true;
		end

	if (StringMatch(shd.arrArgs[1],"TESTER")) then
		if (DawnMod_TestNum==nil) then
			DawnMod:PrintHeader();
			DawnMod:PrintInfoItem("Tester code is not present.");
			end
		if (shd.numArgs==1) then
			DawnMod_NextTest();return end
		if (StringMatch(shd.arrArgs[2],"RESET")) then
			DawnMod_ResetTestNum(); return end
		tempNum = tonumber(shd.arrArgs[2]);
		if (tempNum~=nil) then	DawnMod_Tester(tempNum); end
		return
		end

	if (StringMatch(shd.arrArgs[1],"DEBUG") or StringMatch(shd.arrArgs[1],"DBG")) then
		shd.bHandled = true;
		if (shd.numArgs>=2) then
			bTrueOrFalse, bTrue = SH_GetTF(shd.arrArgs[2]);
		else	bTrueOrFalse = false;
			end
		if (not bTrueOrFalse) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_TrueFalse);
		else	DawnMod:SetDebug( bTrue);
			DawnMod:PrintHeader();
			DawnMod:PrintInfoItem("Debug mode: ",tostring(DawnMod:IsDebug()));
			end
		end

	if (StringMatch(shd.arrArgs[1],"ZI")) then
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("","Zone Text");
		DawnMod:PrintInfoItem("GetRealZoneText","          "..GetRealZoneText());
		DawnMod:PrintInfoItem("GetZoneText","                 "..GetZoneText());
		DawnMod:PrintInfoItem("GetMinimapZoneText","    "..GetMinimapZoneText());
		DawnMod:PrintInfoItem("GetSubZoneText","           "..GetSubZoneText());
		DawnMod:PrintInfoItem("GetZonePVPInfo", "           "..GetZonePVPInfo());
		DawnMod:PrintInfoItem("IsRaidZone", "                    "..tostring(DawnMod:PlayerIsInRaidZone()));
		DawnMod:PrintInfoItem("Player pos","                     "..DawnMod:GetPlayerPosString());
		shd.bHandled = true;
		end

	if (StringMatch(shd.arrArgs[1],"DEV")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("","Dev Command List");
		DawnMod:PrintInfoItem("CHATSTART","                Starts all chat.");
		DawnMod:PrintInfoItem("CHATSTOP","                  Stops all chat.");
		DawnMod:PrintInfoItem("CHECKPARSE [string]","       Does a test parse of [string].");
		DawnMod:PrintInfoItem("CHECKSF [string]","         Does a swear filter of [string].");
		DawnMod:PrintInfoItem("DAWNSET","                    Sets the config to the values I use.");
		DawnMod:PrintInfoItem("DeBuG [Y/N]","                  Debug mode.");
		DawnMod:PrintInfoItem("PCLIST","                          Lists the poster data..");
		DawnMod:PrintInfoItem("PCWIPE","                        Resets all poster data.");
		DawnMod:PrintInfoItem("KARMACHANGE [name|index] [scoreChange]","");
		DawnMod:PrintInfoItem("","                                       Changes someone's karma.");
		DawnMod:PrintInfoItem("MSGTICK [Y/N]","             Periodically shows message counter.");
		DawnMod:PrintInfoItem("MCWIPE","                        Empty out the message cache.");
		DawnMod:PrintInfoItem("MCENTTIME [time]","       Shows the message cache entry at [time].");
		DawnMod:PrintInfoItem("MCENTNO [num]","          Shows the message cache entry at index [num].");
		DawnMod:PrintInfoItem("MCSHOW","                     Shows the contents of the message cache.");
		DawnMod:PrintInfoItem("TOC","                               Display current TOC.");
		DawnMod:PrintInfoItem("ZI","                                   Display zone info.");
		end

	if (StringMatch(shd.arrArgs[1],"MCSHOW")) then
		shd.bHandled = true; DawnMod:BlockChat( true); MsgCacheShow();
		end

	if (StringMatch(shd.arrArgs[1],"MCWIPE")) then
		shd.bHandled = true; DawnMod:PrintHeader(); DawnMod:PrintInfoItem("Message cache cleared.",""); DawnMod:MessageCache():Wipe();
		end

	if (StringMatch(shd.arrArgs[1],"CHATSTOP") or StringMatch(shd.arrArgs[1],"STOPCHAT")) then
		shd.bHandled = true; DawnMod:BlockChat( true);
		DawnMod:PrintHeader();DawnMod:PrintInfoItem("Chat stopped.","");
		end

	if (StringMatch(shd.arrArgs[1],"CHATSTART") or StringMatch(shd.arrArgs[1],"STARTCHAT")) then
		shd.bHandled = true; DawnMod:BlockChat( false);
		DawnMod:PrintHeader();DawnMod:PrintInfoItem("Chat started.","");
		end

	SH_HandleTF( DawnModConfigItem_AllowCaps, "ALLOWCAPS","AC");
	SH_HandleTF( DawnModConfigItem_BlockChannelInvite, "BLOCKCHANNELINVITES","BCI");
	SH_HandleTF( DawnModConfigItem_BlockGlobalChannelAscii, "BLOCKGLOBALASCII","BGA");
	SH_HandleTF( DawnModConfigItem_BlockGlobalChannelForeign, "BLOCKGLOBALFOREIGN","BGF");
	SH_HandleTF( DawnModConfigItem_BlockGlobalChannelMultiline,"BLOCKGLOBALMULTILINE","BGML");
	SH_HandleTF( DawnModConfigItem_BlockRaidZoneGeneral, "BLOCKRAIDZONEGENERAL","BRZG");
	SH_HandleTF( DawnModConfigItem_BlockPartyZoneGeneral, "BLOCKPARTYZONEGENERAL","BPZG");
	SH_HandleTF( DawnModConfigItem_BlockRecount, "BLOCKRECOUNT","BR");
	SH_HandleTF( DawnModConfigItem_BlockRecountExceptParty, "BLOCKRECOUNTEXCEPTPARTY","BREP");
	SH_HandleTF( DawnModConfigItem_BlockRecountExceptRaid, "BLOCKRECOUNTEXCEPTRAID","BRER");
	SH_HandleTF( DawnModConfigItem_BlockWhispers, "BLOCKWHISPERS","BW");
	SH_HandleTF( DawnModConfigItem_BlockWhispersExceptAnswers, "BLOCKWHISPERSEXCEPTANSWERS","BWEA");
	SH_HandleTF( DawnModConfigItem_BlockWhispersExceptFriends, "BLOCKWHISPERSEXCEPTFRIENDS","BWEF");
	SH_HandleTF( DawnModConfigItem_BlockWhispersExceptGuild, "BLOCKWHISPERSEXCEPT","BWEG");
	SH_HandleTF( DawnModConfigItem_BlockWhispersExceptParty, "BLOCKWHISPERSEXCEPT","BWEP");
	SH_HandleTF( DawnModConfigItem_BlockWhispersExceptRaid, "BLOCKWHISPERSEXCEPT","BWER");
	SH_HandleTF( DawnModConfigItem_CommunicationEnabled, "COMMUNICATIONENABLED","CE");
	SH_HandleTF( DawnModConfigItem_FixCaps, "FIXCAPS","FC");
	SH_HandleTF( DawnModConfigItem_FixChannelChanged, "FIXCHANNELCHANGED","FCC");
	SH_HandleTF( DawnModConfigItem_FixChatStickies, "FIXCHATSTICKIES","FCS");
	SH_HandleTF( DawnModConfigItem_FixFastTrack, "FIXFASTTRACK","FFT");
	SH_HandleTF( DawnModConfigItem_FixSpelling, "FIXSPELLING","SPELL","SPELLING","FSP");
	SH_HandleTF( DawnModConfigItem_FixSwearing, "FIXSWEARING","SWEARFILTER","FSW");
	SH_HandleTF( DawnModConfigItem_KarmaUse, "KARMAUSE","KU","USEKARMA","UK");
	SH_HandleTF( DawnModConfigItem_KarmaPlonk, "KARMAIGNORE","KI","KARMAPLONK","KP");
	SH_HandleTF( DawnModConfigItem_PosterCacheSave, "POSTERCACHESAVE","PCS","KARMASAVE","KS");
	SH_HandleTF( DawnModConfigItem_KarmaSuspendGroup, "KARMASUSPENDGROUP","KSGR");
	SH_HandleTF( DawnModConfigItem_KarmaSuspendGuild, "KARMASUSPENDGUILD","KSGU");
	SH_HandleTF( DawnModConfigItem_KillSpamAnnoy, "ANNOY","ANNOYSPAM","SPAMANNOY","SA");
	SH_HandleTF( DawnModConfigItem_KillSpamGold, "GOLD", "GOLDSPAM", "SPAMGOLD","SGO");
	SH_HandleTF( DawnModConfigItem_KillSpamGuild,"GUILD","GUILDSPAM","SPAMGUILD","SGU");
	SH_HandleTF( DawnModConfigItem_KillSpamLink, "LINK", "LINKSPAM", "SPAMLINK","SL");
	SH_HandleTF( DawnModConfigItem_KillSpamMod,"MOD","MODSPAM","SPAMMOD","SM");
	SH_HandleNum( DawnModConfigItem_LockOutMsgTime, DawnMod_MinLockOutTimeMsg, DawnMod_MaxLockOutTimeMsg, "LOCKOUTMSGTIME","LMT","LOMT");
	SH_HandleTF( DawnModConfigItem_LockOutMsgGlobalOnly, "LOCKOUTMESSAGEGLOBALONLY","LOMGO","LMGO");
	SH_HandleNum( DawnModConfigItem_LockOutPosterTime, DawnMod_MinLockOutPosterTime, DawnMod_MaxLockOutPosterTime, "LOCKOUTPOSTERTIME","LPT","LOPT");
	SH_HandleTF( DawnModConfigItem_LockOutPosterGlobalOnly, "LOCKOUTPOSTERGLOBALONLY","LOPGO","LPGO");
	SH_HandleNum( DawnModConfigItem_PosterCacheLength, DawnMod_MinPosterCacheLength, DawnMod_MaxPosterCacheLength, "POSTERCACHELENGTH","PCL");
	SH_HandleNum( DawnModConfigItem_PosterCachePurgeDays, DawnMod_MinPosterCachePurgeDays, DawnMod_MaxPosterCachePurgeDays, "POSTERCACHEPURGEDAYS","PCPD","PCP");
	SH_HandleTF( DawnModConfigItem_ShowCombatFlags, "SHOWCOMBATFLAGS","SCF");
	SH_HandleTF( DawnModConfigItem_ShowMinimapButton, "SHOWMINIMAPBUTTON","SMB","SMMB");
	SH_HandleTF( DawnModConfigItem_StripGlobalChannelRaidMarks, "STRIPGLOBALCHANNELRAIDMARKS","SGCRM","SGCR","SGRM","SGR");
	SH_HandleTF( DawnModConfigItem_StripGlobalChannelUrls, "STRIPGLOBALURLS","SGCU","SGU");
	SH_HandleTF( DawnModConfigItem_StripPlayerFlags,"STRIPPLAYERFLAGS","SPF");
	SH_HandleTF( DawnModConfigItem_TestMode,"TESTMODE","TM");
	SH_HandleTF( DawnModConfigItem_TrapWhispersCombatOnly, "TRAPWHISPERSCOMBATONLY","TWCO");
	SH_HandleTF( DawnModConfigItem_TrapWhispersParty, "TRAPWHISPERSPARTY", "TWP");
	SH_HandleTF( DawnModConfigItem_TrapWhispersRaid,"TRAPWHISPERSRAID","TWR");
	SH_HandleTF( DawnModConfigItem_TrapWhispersReplyOnlyKnown,"TRAPWHISPERREPLYONLYKNOWN","TWROK");
	SH_HandleTF( DawnModConfigItem_UpdateNotificationEnabled, "UPGRADENOTIFICATIONENABLED","UNE");

	if (string.find(shd.arrArgs[1],"^INVITEACCEPT") or string.find(shd.arrArgs[1],"^IA")) then
		local	val,fl,cfgNo,bFlag,bOk,st;
		cfgNo=0;
		bOk=false;
		if (StringMatch(shd.arrArgs[1],"INVITEACCEPTANSWERS")or StringMatch(shd.arrArgs[1],"IAA") or
		    StringMatch(shd.arrArgs[1],"INVITEACCEPTANSWERSNOTIFY")or StringMatch(shd.arrArgs[1],"IAAN")) then
			cfgNo=DawnModConfigItem_InviteAcceptAnswers;
			end
		if (StringMatch(shd.arrArgs[1],"INVITEACCEPTFRIENDS")or StringMatch(shd.arrArgs[1],"IAF") or
		    StringMatch(shd.arrArgs[1],"INVITEACCEPTFRIENDSNOTIFY")or StringMatch(shd.arrArgs[1],"IAFN")) then
			cfgNo=DawnModConfigItem_InviteAcceptFriends;
			end
		if (StringMatch(shd.arrArgs[1],"INVITEACCEPTGUILD")or StringMatch(shd.arrArgs[1],"IAG") or
		    StringMatch(shd.arrArgs[1],"INVITEACCEPTGUILDNOTIFY")or StringMatch(shd.arrArgs[1],"IAGN")) then
			cfgNo=DawnModConfigItem_InviteAcceptGuild;
			end
		if (StringMatch(shd.arrArgs[1],"INVITEACCEPTWHITELIST")or StringMatch(shd.arrArgs[1],"IAW") or
		    StringMatch(shd.arrArgs[1],"INVITEACCEPTWHITELISTNOTIFY")or StringMatch(shd.arrArgs[1],"IAWN")) then
			cfgNo=DawnModConfigItem_InviteAcceptWhitelist;
			end
		if (StringMatch(shd.arrArgs[1],"INVITEACCEPTUNKNOWN")or StringMatch(shd.arrArgs[1],"IAU") or
		    StringMatch(shd.arrArgs[1],"INVITEACCEPTUNKNOWNNOTIFY")or StringMatch(shd.arrArgs[1],"IAUN")) then
			cfgNo=DawnModConfigItem_InviteAcceptUnknown;
			end
		bFlag = (((string.len(shd.arrArgs[1])<7) and (string.find(shd.arrArgs[1],"N")~=nil)) or (string.find(shd.arrArgs[1],"NOTIFY")~=nil));

		if (cfgNo>0) then
			shd.bHandled = true;
			if (shd.numArgs~=2) then
				SH_ErrorMsg(shd.arrArgs[1], (bFlag and ShErrType_TrueFalse or ShErrType_Numeric));
			else	val,fl = DawnMod:InviteFlags( DawnMod:CfgGetValue( cfgNo));
				bTrueOrFalse,bTrue = SH_GetTF( shd.arrArgs[2]);
				if (bFlag) then
					if (not bTrueOrFalse) then
						SH_ErrorMsg(shd.arrArgs[1], (bFlag and ShErrType_TrueFalse or ShErrType_Numeric));
					else	fl = (not bTrue);
						bOk=true;
						end
				else
					tempNum=tonumber(shd.arrArgs[2]);
					if ((tempNum==nil)or(tempNum<0)or(tempNum>2)) then
						SH_ErrorMsg(shd.arrArgs[1], ShErrType_Numeric,0,2);
					else	val=tempNum;
						bOk=true;
						end
					end
				if (bOk) then
					DawnMod:CfgSetValue(cfgNo,(fl and DawnModInviteFlag_DontNotify or 0) + val);
					if (bFlag) then st=tostring(fl); else st=tostring(val);end
					DawnMod:PrintHeader();
					DawnMod:PrintInfoItem( DawnMod:GetConfigDataValue(cfgNo,DawnModConfigDataType_TextDescription)..
						(bFlag and " (notifications)" or "")..": ",st);
					end
				end
			end
		end

	if (StringMatch(shd.arrArgs[1],"MCENTTIME")) then
		shd.bHandled = true;
		if (shd.numArgs>=2) then tempNum=tonumber(shd.arrArgs[2]);end
		if ((shd.numArgs<2)or(tempNum==nil)) then
			SH_ErrorMsg(shd.arrArgs[1],ShErrType_Numeric);
		else	ShowNearestMsgCacheEntryReal(tempNum);
			end
		end

	if (StringMatch(shd.arrArgs[1],"MCENTNO")) then
		shd.bHandled = true;
		if (shd.numArgs>=2) then tempNum=tonumber(shd.arrArgs[2]);end
		if ((shd.numArgs<2)or(tempNum==nil)) then
			if (DawnMod:MessageCache():Length()==0) then
				DawnMod:PrintInfoItem("There are no msgCache entries yet.","");
			else	SH_ErrorMsg(shd.arrArgs[1],ShErrType_Numeric, 0, DawnMod:MessageCache():Length());
				end
		else	ShowMsgCacheEntry( tempNum, true);
			end
		end

	if (StringMatch(shd.arrArgs[1],"REMENTNAME")) then
		shd.bHandled = true;
		ShowMsgCacheEntriesRealByName( shd.arrArgs[2]);
		end

	if (StringMatch(shd.arrArgs[1],"DEMOMODE") or
	    StringMatch(shd.arrArgs[1],"DEMO") ) then
		shd.bHandled = true;
		DawnMod:CfgSetValue(DawnModConfigItem_TestMode,true);
		DawnMod:PrintHeader();
		if (string.find(shd.arrArgs[1],"DEMO")~=nil) then
			DawnMod.bTickMessage = DawnMod:CfgGetValue(DawnModConfigItem_TestMode);
			if (DawnMod.bTickMessage) then DawnMod:ResetMsgPeriodData(); end
			DawnMod:PrintInfoItem("DemoMode: ",DawnMod:CfgGetValue(DawnModConfigItem_TestMode));
		else	DawnMod:PrintInfoItem("TestMode: ",DawnMod:CfgGetValue(DawnModConfigItem_TestMode));
			end
		end

	if (StringMatch(shd.arrArgs[1],"MSGTICK") or StringMatch(shd.arrArgs[1],"MT") or
	    StringMatch(shd.arrArgs[1],"TICKMSG") or StringMatch(shd.arrArgs[1],"TM")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		if(shd.numArgs>=2) then
			bTrueOrFalse, bTrue = SH_GetTF(shd.arrArgs[2]);
			if (bTrueOrFalse) then
				DawnMod.bTickMessage = bTrue;
			else	SH_ErrorMsg(shd.arrArgs[1],ShErrType_TrueFalse);
				bTrueOrFalse = false;
				end
		else	bTrueOrFalse = true;
			end
		if (bTrueOrFalse) then
			if (DawnMod.bTickMessage) then tempNum = "S";
			else tempNum = "Not s"; end
			DawnMod:PrintInfoItem(tempNum.."howing periodic message counts.","");
			end
		if (DawnMod.bTickMessage) then DawnMod:ResetMsgPeriodData(); end
		end


	if (StringMatch(shd.arrArgs[1],"VC") or StringMatch(shd.arrArgs[1],"VERSIONCHECK")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		tempNum="";
		if (shd.numArgs>=2) then tempNum=shd.arrArgs[2]; end
		DawnMod:DoVersionCheck(tempNum);
		end

	if (StringMatch(shd.arrArgs[1],"WLA") or StringMatch(shd.arrArgs[1],"WHITELISTADD")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		if (shd.numArgs>=2) then
			tmpNum=DawnModWhitelist:FixName(DawnMod:StripPadding(string.sub(stMsgIn, string.find(stMsgIn," ")+1)));
			if (tmpNum==nil) then
				DawnMod:PrintInfoItem("Invalid name");
			else
				if (DawnModWhitelist:FindName( tmpNum)>0) then
					DawnMod:PrintInfoItem("Name already exists: ",tmpNum);
				else	DawnMod:PrintInfoItem("Added to whitelist: ",tmpNum);
					DawnModWhitelist:AddName( tmpNum);
					end
				end
		else	DawnMod:PrintInfoItem("WhiteListAdd requires a [string] argument.");
			end
		end

	if (StringMatch(shd.arrArgs[1],"WLR") or StringMatch(shd.arrArgs[1],"WHITELISTREMOVE")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		if (shd.numArgs>=2) then
			tmpNum=DawnModWhitelist:FixName(DawnMod:StripPadding(string.sub(stMsgIn, string.find(stMsgIn," ")+1)));
			if ((tmpNum==nil)or (DawnModWhitelist:FindName( tmpNum)==0)) then
				DawnMod:PrintInfoItem("Name not found: ",tmpNum);
			else	DawnModWhitelist:RemoveName( tmpNum);
				DawnMod:PrintInfoItem("Removed from whitelist: ",tmpNum);
				end
		else	DawnMod:PrintInfoItem("WhiteListRemove requires a [string] argument.");
			end
		end

	if (StringMatch(shd.arrArgs[1],"WLS") or StringMatch(shd.arrArgs[1],"WHITELISTSHOW")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		ShowWhitelist();
		end

	if (StringMatch(shd.arrArgs[1],"WLC") or StringMatch(shd.arrArgs[1],"WHITELISTCLEAR")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("Whitelist cleared.");
		DawnModWhitelist:Clear();
		end

	if (StringMatch(shd.arrArgs[1],"MF") or StringMatch(shd.arrArgs[1],"MESSAGEFILTER")) then
		shd.bHandled = true;
		DoMessageFilterOption();
		end

	if (StringMatch(shd.arrArgs[1],"SNC") or StringMatch(shd.arrArgs[1],"SHOWNPCCHATTER")) then
		shd.bHandled = true;
		tmpNum=1;
		DawnMod:PrintHeader();
		if (DawnMod.arrNpcChatter[1]==nil) then
			DawnMod:PrintInfoItem("No Npc chatter cached.");
			end
		while(DawnMod.arrNpcChatter[tmpNum])do
			DawnMod:PrintInfoItem("",DawnMod.arrNpcChatter[tmpNum]);
			tmpNum=tmpNum+1;
			end
		end

	if (StringMatch(shd.arrArgs[1],"CNC") or StringMatch(shd.arrArgs[1],"CLEARNPCCHATTER")) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("Npc chatter table cleared.");
		table.wipe(DawnMod.arrNpcChatter);
		end

	if ( (not shd.bHandled) and ( (shd.numArgs==1) and (StringMatch(shd.arrArgs[1],"HELP") or StringMatch(shd.arrArgs[1],"?"))) ) then
		shd.bHandled = true;
		DawnMod:PrintHeader();
		DawnMod:PrintInfoItem("","Command List");
		DawnMod:PrintInfoItem("Help","     Displays this command listing.");

		tmpNum=1;
		while(arrCmdLine[tmpNum]) do
			DawnMod:PrintInfoItem( arrCmdLine[tmpNum],"     "..arrCmdLine[tmpNum+1]);
			tmpNum=tmpNum+2;
			end
		end
	if (not shd.bHandled) then DisplayShortHelp( shd.arrArgs[1]); end
	end


