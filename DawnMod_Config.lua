

--	tex:SetTexture("Interface\\CHARACTERFRAME\\UI-BarFill-Simple")

DmCfg = {};

local	CheckboxSize		= 25;
local	ButtonHeight		= 25;
local	ButtonTagFont		= "GameFontNormal";
local	ButtonTagFontSize	= 12;
local	HeaderXOfs		    = 150;
local	HeaderTagFont		= "GameFontHighlightLarge";
local	SliderHeight		= 15;

local	DmCfgFrame = nil;
local	arrCategories = {};
local	arrWigets = {};

local	CS_CbChecked	= 1;
local	CS_CbUnchecked	= 2;
local	CS_ButtonClick	= 3;

local	colEnabledR, colEnabledG, colEnabledB,colEnabledA;

local	arrSliders = {};

local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg);end

local function Whitelist()
	local	tbl;

	if (DawnMod_ConfigWindowVisible) then
		tbl = DawnMod:TempConfig()[DawnModConfigItem_Whitelist];
	else	tbl = DawnMod:CurConfig()[DawnModConfigItem_Whitelist];
		end
	return tbl;
	end

local function FilterOptions()
	local	tbl;

	if (DawnMod_ConfigWindowVisible) then
		tbl = DawnMod:TempConfig()[DawnModConfigItem_FilterOptions];
	else	tbl = DawnMod:CurConfig()[DawnModConfigItem_FilterOptions];
		end
	return tbl;
	end


local function PlayConfigSound( sndNo)
	local	stSnd;

	stSnd = nil;
	if (sndNo==CS_CbChecked) then stSnd="igMainMenuOptionCheckBoxOn";end
	if (sndNo==CS_CbUnchecked) then stSnd="igMainMenuOptionCheckBoxOff";end
	if (sndNo==CS_ButtonClick) then stSnd="igMainMenuOption";end
	if (stSnd~=nil) then PlaySound(stSnd); end
	end


local function FindStringInTable( str, tab)
	local	cnt=1;

	while( tab[cnt]) do
		if (string.upper(tab[cnt])==string.upper(str)) then return cnt; end
		cnt=cnt+1;
		end
	return 0;
	end


local function WlGetItemName(itemNo)
	return Whitelist()[itemNo];
	end

local function WlItemClick(itemNo, obj)
	local	wig=DmCfg:FindWiget(nil,"WhitelistDelete",nil);
	local	scrollWig = DmCfg:FindWiget(nil,"WhitelistNames",nil);

	if (scrollWig.selected) then
		wig.tooltip[2]=" ";
		wig.tooltip[3]= Whitelist()[itemNo];
	else	wig.tooltip[2]=nil;
		end
	DmCfg:FindWiget(nil,"WhitelistNew",nil).frame:ClearFocus();
	DmCfg:ConfigChanged();
	end

local function WlGetNumItems()
	local cnt=0;

	while(Whitelist()[cnt+1]) do
		cnt=cnt+1;
		end
	return cnt;
	end


local function WlGetTooltip(itemNo)
	return nil;
	end


local function WlebOnEnterPressed( obj)
	local	st = obj:GetText();
	local	wig;

	if (string.len( st)<1) then return end
	st = DawnModWhitelist:FixName(st);
	if (st==nil) then return end
	if (FindStringInTable( st, Whitelist())~=0) then return end

	wig = DmCfg:FindWiget(nil,"WhitelistNames",nil);
	if (wig.selected and (st<Whitelist()[wig.selected])) then
		wig.selected = wig.selected+1;
		end
	table.insert( Whitelist(), st);
	table.sort( Whitelist());
	obj:SetText("");
	DmCfg:ConfigChanged();
	end


local function WhitelistDelete_OnClick()
	local	wig=DmCfg:FindWiget(nil,"WhitelistNames",nil);

	table.remove( Whitelist(), wig.selected);
	table.sort( Whitelist());
	wig.selected = nil;

	wig=DmCfg:FindWiget(nil,"WhitelistDelete",nil);
	wig.tooltip[2]=nil;
	wig.tooltip[3]=Whitelist()[itemNo];
	DmCfg:FindWiget(nil,"WhitelistNew",nil).frame:ClearFocus();

	DmCfg:ConfigChanged();
	end


function DmCfg:CountWigets() local pos=1;while(arrWigets[pos])do pos=pos+1;end return (pos-1);end

function DmCfg:FindWiget( idNum, stName, stType)
	local	pos=1;

	while (arrWigets[pos]) do
--		PrintDbg("#"..tostring(arrWigets[pos].idNum).." nam = >"..tostring(arrWigets[pos].stName).."< type = >"..tostring(arrWigets[pos].stType).."<");
		if ( ((idNum==nil) or (idNum==arrWigets[pos].idNum)) and
		     ((stName==nil) or (stName==arrWigets[pos].stName)) and
		     ((stType==nil) or (stType==arrWigets[pos].stType)) ) then
			return arrWigets[pos];
			end
		pos=pos+1;
		end
	return nil;
	end


function DmCfg:FindWigetByFrame( frame)
	local	pos=1;

	while (arrWigets[pos]) do
		if (arrWigets[pos].frame == frame) then
			return arrWigets[pos];
			end
		pos=pos+1;
		end
	return nil;
	end


function DmCfg:AddWigetToList( frame, stName, stType, idNum)
	local	info = {};

	info.frame = frame;
	info.stName = stName;
	info.idNum = idNum;
	info.stType = stType;
	table.insert(arrWigets, info);
	return info;
	end


local function AddTag( parentFrame, stNameIn, idNum, xpos,ypos, stTag, nHeight, stInheritIn)
	local tag, stInherit, stName;

	stInherit = stInheritIn;
	if (stInherit==nil) then stInherit = "GameFontNormal"; end

	stName = stNameIn;
	if (stName==nil) then stName="";end

	tag = parentFrame:CreateFontString(stName,"ARTWORK",stInherit);
	tag:SetPoint("TOPLEFT", xpos, ypos);
	if (nHeight) then tag:SetTextHeight(nHeight); end
	tag:SetText(stTag);

	DmCfg:AddWigetToList( tag, stName, "TEXT", idNum);
	return tag;
	end


local function AddCheckBox( parentFrame, stNameIn, idNum, xpos, ypos, stTag, fnOnClick, tooltip)
	local	cb, stName,wig;

	stName=stNameIn;if(stName==nil)then stName="";end

	cb = CreateFrame("CheckButton", stName, parentFrame, "ChatConfigCheckButtonTemplate");

	cb:SetID( idNum);
	cb:SetPoint("TOPLEFT", xpos, ypos);
	cb:SetWidth(CheckboxSize);
	cb:SetHeight(CheckboxSize);
	cb:SetHitRectInsets(0, 0, 0, 0);
	cb:RegisterForClicks("AnyUp");

	cb:SetScript("OnEnter", function(obj)DmCfg:CheckBox_OnEnter(obj);end);
	cb:SetScript("OnLeave", function(obj)DmCfg:CheckBox_OnLeave(obj);end);
	cb:SetScript("OnClick", function(obj)DmCfg:CheckBox_OnClick(obj);end);
	cb:SetScript("OnShow",  function(obj)DmCfg:CheckBox_OnShow(obj);end);

	cb:SetFontString( AddTag( cb, stName.."Font",idNum,CheckboxSize+3,-7,stTag,ButtonTagFontSize,ButtonTagFont) );
	wig=DmCfg:AddWigetToList( cb, stName, "CHECKBOX", idNum);
	wig.fnOnClick = fnOnClick;
	wig.tooltip = tooltip;
	return cb;
	end



local function AddSlider( parentFrame, stNameIn, idNum, xpos, ypos, nWidth, stTag)
	local	slider, stName;

	stName=stNameIn;if(stName==nil)then stName="";end

	slider = CreateFrame("Slider", stName, parentFrame, "OptionsSliderTemplate");
	slider:SetID( idNum);
	slider:SetPoint("TOPLEFT", xpos, ypos);
	slider:SetWidth(nWidth);
	slider:SetHeight( SliderHeight);
--	slider:SetHitRectInsets(0, 0, 0, 0);
	slider:SetScript("OnLoad", function(obj)DmCfg:Slider_OnLoad(obj);end);
	slider:SetScript("OnShow", function(obj)DmCfg:Slider_OnShow(obj);end);
	slider:SetScript("OnValueChanged", function(obj)DmCfg:Slider_OnValueChanged(obj);end);

	DmCfg:AddWigetToList( slider, stName, "SLIDER", idNum);
	return slider;
	end


function DmCfg:SFButton_OnClick(obj)
	local	wig=DmCfg:FindWigetByFrame(obj);
	local	ofs=FauxScrollFrame_GetOffset(wig.scrollWiget.frame);
	local	st, pos;

	st = string.sub(wig.stName, string.find( wig.stName,"Button")+string.len("Button") );
	pos = tonumber(st)+ofs;

	if (wig.scrollWiget.bStickyClick) then
		if (wig.scrollWiget.selected and (wig.scrollWiget.selected==pos)) then
			wig.scrollWiget.selected = nil;
		else	wig.scrollWiget.selected = pos;
			end
	else	wig.scrollWiget.selected = pos;
		end
	wig.scrollWiget.fnItemClick(pos, obj);
	PlayConfigSound(CS_CbChecked);
	DmCfg:ConfigChanged();
	end


local function AddSFTag( parentFrame, stNameIn, idNum, xpos,ypos, stTag, nHeight, stInheritIn)
	local tag, stInherit, stName;

	stInherit = stInheritIn;
	if (stInherit==nil) then stInherit = "GameFontNormal"; end

	stName = stNameIn;
	if (stName==nil) then stName="";end

	tag = parentFrame:CreateFontString(stName,"ARTWORK",stInherit);
	tag:SetPoint("TOPLEFT", parentFrame,0,-1);
	if (nHeight) then tag:SetTextHeight(nHeight); end
	tag:SetText(stTag);

	DmCfg:AddWigetToList( tag, stName, "TEXT", idNum);
	return tag;
	end


local function SFButton_OnEnter(obj)
	local	wig=DmCfg:FindWigetByFrame(obj);
	local	ofs, btnNum;

	ofs = FauxScrollFrame_GetOffset(wig.scrollWiget.frame);
	btnNum = tonumber(string.sub(wig.stName, string.find(wig.stName,"Button")+string.len("Button")));
	DmCfg:DoTooltipShow( obj, wig.scrollWiget.fnGetTooltip(ofs+btnNum));
	end


local function AddSFButton( parentFrame, stNameIn, xpos, ypos, nWidth, nHeight, stParentName)
	local	stName, btn, wig,bk;

	stName=stNameIn;if(stName==nil)then stName="";end
	btn = CreateFrame("Button", stName, parentFrame, "FriendsFrameIgnoreButtonTemplate");

	btn:SetPoint("TOPLEFT", xpos, ypos);
	btn:SetWidth(nWidth);
	btn:SetHeight(16);
	btn:SetHitRectInsets(0, 0, 0, 0);
	btn:RegisterForClicks("AnyUp");
--	btn:EnableMouseWheel(true);

	btn:SetScript("OnClick",function(obj)DmCfg:SFButton_OnClick(obj);end);
	btn:SetScript("OnEnter",function(obj)SFButton_OnEnter(obj);end);
	btn:SetScript("OnLeave",function(obj)DmCfg:DoTooltipHide(obj);end);

	btn:SetFontString( AddSFTag( btn, stName.."Font",0,xpos,ypos,"",ButtonTagFontSize,ButtonTagFont) );

	bk = btn:CreateTexture(nil,"BACKGROUND");
	bk:SetTexture(0, 0, 0, 0.9)
	bk:SetPoint("TOPLEFT", btn,0,0);
	bk:SetPoint("BOTTOMRIGHT", btn,0,0);

	wig=DmCfg:AddWigetToList( btn, stName, "SFBUTTON", 0);
	wig.scrollWiget = DmCfg:FindWiget(nil,stParentName,"SCROLLBARBTN");
	return btn;
	end


function DmCfg:ScrollFrameBtn_OnUpdate( obj)
	local	cnt, stObjName, fnt, btn, ofs, wig;

	ofs = FauxScrollFrame_GetOffset(obj);
	wig = DmCfg:FindWigetByFrame(obj);
	stObjName = wig.stName;
--	PrintDbg("onupdate numitems = "..tostring(wig.fnGetNumItems()));

--	PrintDbg("num = "..tostring(wig.numOptions));
	for cnt=1,wig.numOptions do
		fnt = DmCfg:FindWiget( nil, stObjName.."Button"..tostring(cnt).."Font",nil).frame;
		btn = DmCfg:FindWiget( nil, stObjName.."Button"..tostring(cnt),nil).frame;
		if (cnt+ofs > wig.fnGetNumItems()) then
			btn:Hide();btn:Disable();
		else	btn:Enable();btn:Show();
			fnt:SetText( wig.fnGetItemName(cnt+ofs));
			if (wig.bStickyClick) then
				if ((cnt+ofs)==wig.selected) then
					btn:LockHighlight();
				else	btn:UnlockHighlight();
					end
				end
			end
		end

	FauxScrollFrame_Update(obj,wig.fnGetNumItems(),wig.numOptions,16);
	end


function DmCfg:ScrollFrameBtn_OnVerticalScroll(obj, offset)
	FauxScrollFrame_OnVerticalScroll(obj, offset,16);
	DmCfg:ScrollFrameBtn_OnUpdate(obj);
	end


local function AddScrollFrame( parentFrame, stNameIn, xpos, ypos, nWidth, nHeight, numOptions, bStickyClick,
				fnGetNumItems, fnGetItemName, fnItemClick, fnGetTooltip)
	local	frScroll, frChild, stName, cnt, btn, tex, wig;

	stName = stNameIn;if(stName==nil)then stName="";end
	frScroll  = CreateFrame("ScrollFrame", stName, parentFrame, "FauxScrollFrameTemplate");
	frScroll:SetPoint("TOPLEFT", xpos+nWidth-20, ypos);
	frScroll:SetWidth(16);
	frScroll:SetHeight(nHeight);
--	frScroll:SetScript("OnUpdate",function(obj)DmCfg:ScrollFrameBtn_OnUpdate(obj);end);
	frScroll:SetScript("OnShow",function(obj)DmCfg:ScrollFrameBtn_OnUpdate(obj);end);
	frScroll:SetScript("OnVerticalScroll",function(obj,offset)DmCfg:ScrollFrameBtn_OnVerticalScroll(obj,offset);end);
	frScroll:EnableMouseWheel(true);

	tex = frScroll:CreateTexture(nil, "BACKGROUND");
	tex:SetTexture(0, 0, 0, 0.6)
	tex:SetPoint("TOPLEFT", frScroll,23,-5);
	tex:SetPoint("BOTTOMRIGHT", frScroll,20,5);

	wig=DmCfg:AddWigetToList( frScroll, stName, "SCROLLBARBTN", 0);
	wig.fnGetNumItems = fnGetNumItems;
	wig.fnGetItemName = fnGetItemName;
	wig.fnItemClick = fnItemClick;
	wig.fnGetTooltip = fnGetTooltip;
	wig.bStickyClick = bStickyClick;
	wig.selected = nil;
	wig.selectedButton = 0;
	wig.numOptions = numOptions;

	for cnt=0,wig.numOptions-1 do
		btn=AddSFButton( parentFrame, stName.."Button"..tostring(cnt+1), xpos,ypos-(cnt*16),nWidth,16, stName);
		tex = btn:CreateTexture(nil, "BACKGROUND");
		tex:SetHeight(16)
		tex:SetWidth(nWidth);
		tex:SetPoint("TOPLEFT", btn,  0, 0)
		tex:SetPoint("BOTTOMRIGHT", btn, 0, 0)
		tex:SetTexture("Interface\\Common\\Common-Input-Border")
--		tex:SetTexCoord(0.0625, 0.9375, 0, 0.625)
		tex:SetTexCoord(0.01, 0.01, 0.01, 0.01)
		end

	end


local function AddEditBox( parentFrame, stNameIn, idNum, xpos,ypos, nWidth,nHeight, fnOnEnterPressed)
	local	stName, fr, bk, wig;

	stName = stNameIn;if(stName==nil)then stName="";end
	fr  = CreateFrame("EditBox", stName, parentFrame);
	fr:SetAutoFocus(false)
	fr:SetFontObject(GameFontHighlight)
	fr:SetPoint("TOPLEFT", xpos, ypos);
	fr:SetWidth(nWidth);
	fr:SetHeight(nHeight);
	fr:SetMultiLine(false);
	fr:SetMaxLetters(50);
	fr:SetJustifyH("LEFT");
	fr:EnableMouse(true);
	fr:SetHitRectInsets(0, 0, 70, 70);
	wig=DmCfg:AddWigetToList( fr, stName, "EDITBOX", idNum);
	wig.fnOnEnterPressed = fnOnEnterPressed;


	fr:SetScript("OnEscapePressed", function(obj)obj:ClearFocus();end);
	fr:SetScript("OnEnterPressed", function(obj)DmCfg:FindWigetByFrame(obj).fnOnEnterPressed(obj);end);
	fr:SetScript("OnTabPressed", function(obj)PrintDbg("Tab");end);


	local left = fr:CreateTexture(nil, "BACKGROUND")
	left:SetWidth(8) left:SetHeight(20)
	left:SetPoint("LEFT", -5, 0)
	left:SetTexture("Interface\\Common\\Common-Input-Border")
	left:SetTexCoord(0, 0.0625, 0, 0.625)

	local right = fr:CreateTexture(nil, "BACKGROUND")
	right:SetWidth(8) right:SetHeight(20)
	right:SetPoint("RIGHT", 0, 0)
	right:SetTexture("Interface\\Common\\Common-Input-Border")
	right:SetTexCoord(0.9375, 1, 0, 0.625)

	local center = fr:CreateTexture(nil, "BACKGROUND")
	center:SetHeight(20)
	center:SetPoint("RIGHT", right, "LEFT", 0, 0)
	center:SetPoint("LEFT", left, "RIGHT", 0, 0)
	center:SetTexture("Interface\\Common\\Common-Input-Border")
	center:SetTexCoord(0.0625, 0.9375, 0, 0.625)

	return fr;
	end


function DmCfg:Button_OnClick(obj)
	local	wig=DmCfg:FindWigetByFrame(obj);
	if (wig.fnOnClick) then
		wig.fnOnClick(obj);
		end
	PlayConfigSound(CS_ButtonClick);
	end


function DmCfg:Button_OnEnter(obj)
	local	wig=DmCfg:FindWigetByFrame(obj);
	DmCfg:DoTooltipShow( obj, wig.tooltip)
	end

function DmCfg:Button_OnLeave(obj)
	DmCfg:DoTooltipHide(obj);
	end

local function AddButton( parentFrame, stNameIn, idNum, xpos,ypos, nWidth, tag, fnOnClick, tooltip)
	local	stName, fr, wig;

	stName = stNameIn;if(stName==nil)then stName="";end
	fr  = CreateFrame("Button", stName, parentFrame, "UIPanelButtonTemplate");
	fr:SetPoint("TOPLEFT", xpos, ypos);
	fr:SetWidth(nWidth);
	fr:SetHeight(ButtonHeight);
	fr:SetHitRectInsets(0, 0, 0, 0);
	fr:SetText(tag);
	fr:RegisterForClicks("AnyUp");

	wig=DmCfg:AddWigetToList( fr, stName, "BUTTON", idNum);
	wig.fnOnClick	= fnOnClick;
	wig.tooltip	= tooltip;

	fr:SetScript("OnClick",function(obj)DmCfg:Button_OnClick(obj);end);
	fr:SetScript("OnEnter",function(obj)DmCfg:Button_OnEnter(obj);end);
	fr:SetScript("OnLeave",function(obj)DmCfg:Button_OnLeave(obj);end);
--	fr:SetScript("OnShow",function(obj)PrintDbg("btn->onshow");end);

	return fr;
	end



local function ConfigCheckBox_OnClick( obj, bChecked)

	local	cfgNum = obj:GetID();

	DawnMod:CfgSetValue( cfgNum, bChecked, DawnMod:TempConfig());
	DmCfg:ConfigChanged( true);
	if (DawnMod:IsDebug()) then
		DawnMod:PrintConfig( cfgNum, DawnMod:TempConfig(), false);
		end
	end

local function AddConfigCheckBox( parentFrame, idNum, xpos,ypos,stTag)
	local	tab,pos,tmp;

	tab = {};
	tab[1] = DawnMod:GetConfigDataValue( idNum, DawnModConfigDataType_TextDescription);
	tmp = DawnMod:GetConfigDataValue( idNum, DawnModConfigDataType_LongDescription);
	pos=1;
	while(tmp[pos]) do tab[pos+1]=tmp[pos];pos=pos+1;end
	tab[pos+1]=nil;

	AddCheckBox( parentFrame, "",idNum,xpos,ypos,stTag,
		ConfigCheckBox_OnClick,
		tab
		);
	end


local function WhitelistCheckBox_OnClick( obj, bChecked)
	local	wig=DmCfg:FindWigetByFrame(obj);
	local	st, pos;

	st="@"..string.sub(wig.stName, string.len("WhitelistCb")+1);

	pos = FindStringInTable( st, Whitelist());
	wig = DmCfg:FindWiget(nil,"WhitelistNames",nil);
	if (bChecked) then
		if (pos==0) then
			if (wig.selected and (Whitelist()[wig.selected]>st)) then
				wig.selected = wig.selected+1;
				end
			table.insert(Whitelist(), string.upper(st));
			end
	else
		if (pos>0) then
			if (wig.selected and (Whitelist()[wig.selected]>st)) then
				wig.selected = wig.selected-1;
				end
			table.remove(Whitelist(), pos);
			end
		end
	table.sort( Whitelist());
	end


local function InviteCheckBox_OnClick( obj, bChecked)
	local	wig,stCategory,cfgNo, stAction, val, flagNotify, flagWord,
		action, bDone;

	wig=DmCfg:FindWigetByFrame(obj);
	stCategory = string.sub( wig.stName, string.len("Invite")+1);
	if (string.find(stCategory,"Ask")) then stAction = "Ask"; action = DawnModInviteValue_Ask; end
	if (string.find(stCategory,"Accept")) then stAction = "Accept"; action = DawnModInviteValue_Accept; end
	if (string.find(stCategory,"Deny")) then stAction = "Deny"; action = DawnModInviteValue_Deny;end
	if (string.find(stCategory,"Notify")) then stAction = "Notify"; action = DawnModInviteFlag_DontNotify; end
	if (string.find(stCategory,"Word")) then stAction = "Word"; action = DawnModInviteFlag_InviteWord; end
	stCategory=string.sub(stCategory,1,string.len(stCategory)-string.len(stAction));
	cfgNo = wig.idNum;
	val, flagNotify,flagWord = DawnMod:InviteFlags( DawnMod:CfgGetValue( cfgNo,DawnMod:TempConfig()));
	bDone = false;
	if (action==DawnModInviteFlag_DontNotify) then
		flagNotify = (not flagNotify);
		bDone=true;
		end
	if (action==DawnModInviteFlag_InviteWord) then
		flagWord = (not flagWord);
		bDone=true;
		end
	if ((not bDone) and (action ~= val)) then
		val = action;
		end
	val = bit.bor( val,
		      (flagNotify and DawnModInviteFlag_DontNotify or 0),
		      (flagWord and DawnModInviteFlag_InviteWord or 0) );
	DawnMod:CfgSetValue( cfgNo, val, DawnMod:TempConfig());
	end


local function InviteWordEdit_OnEnterPressed( obj)
	local	st = obj:GetText();
	local	wig;

	if ((st==nil)or(type(st)~="string")) then return end
	if (string.len( st)<1) then return end
	DawnMod:CfgSetValue( DawnModConfigItem_InviteWord, st, DawnMod:TempConfig());
	obj:ClearFocus();
	DmCfg:ConfigChanged();
	end


local function makeMovable(frame)
	local mover = _G[frame:GetName() .. "Mover"] or CreateFrame("Frame", frame:GetName() .. "Mover", frame)
	mover:EnableMouse(true)
	mover:SetPoint("TOP", frame, "TOP", 0, 10)
	mover:SetWidth(160)
	mover:SetHeight(40)
	mover:SetScript("OnMouseDown", function(self)self:GetParent():StartMoving()end)
	mover:SetScript("OnMouseUp", function(self)self:GetParent():StopMovingOrSizing()end)
	frame:SetMovable(true)
	frame:ClearAllPoints()
	frame:SetPoint("CENTER")
	end


local function CheckBoxShowTooltips_OnClick( obj, bChecked)
	DawnMod:SetConfigWindowShowTooltips(bChecked);
	end


local function AddCheckBoxShowTooltips(parentFrame)
	local	wig, cb;
	cb=AddCheckBox( parentFrame, "ShowTooltips"..parentFrame.name, 0, 385, -400, "Show tooltips",
		CheckBoxShowTooltips_OnClick,nil);
	wig=DmCfg:FindWiget(nil,"ShowTooltips"..parentFrame.name.."Font");
	wig.frame:SetPoint("TOPLEFT",-90,-5);
	end



local function AddCategory( stName, parentFrame)
	local	fr, bk, parentName;

	if (parentFrame) then parentName = parentFrame.name; else parentName=nil;end
	fr = CreateFrame("Frame", nil, UIParent);
	fr.parent = parentName;
	fr.name = stName;

	fr.default = function(o) PrintDbg(o.name.."->default"); DmCfg:Defaults_OnClick();end
	if (parentFrame==nil) then
		fr.cancel = function(o) PrintDbg(o.name.."->cancel"); DmCfg:Cancel_OnClick();end
		fr.okay = function(o) PrintDbg(o.name.."->ok");DmCfg:Save_OnClick();end
		fr.refresh = function(o) PrintDbg(o.name.."->refresh"); DawnMod:OnConfigWindowShow();DmCfg:ConfigChanged(); end
		end

	fr:SetScript("OnMouseUp",function(t,f)if(f=="LeftButton")then t:StopMovingOrSizing();end;end);
	fr:SetScript("OnEnter",function(o)DmCfg.lastCategory=o;end);


	bk = fr:CreateTexture(nil, "BACKGROUND");
	bk:SetTexture(0, 0, 0, 0.6)
	bk:SetPoint("TOPLEFT", fr,0,0);
	bk:SetPoint("BOTTOMRIGHT", fr,0,0);


	InterfaceOptions_AddCategory( fr);
	DmCfg:AddWigetToList( fr, stName, "FRAME", 0)

	table.insert(arrCategories, fr);

	if (parentFrame) then
		AddCheckBoxShowTooltips( fr);
		end

	return fr;
	end


local function FiltersGetNumItems()
	local	bExpanded=false;
	local	pos=1;
	local	cnt=0;
	local	catNo=0;

	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@@")==1) then
			if (bExpanded) then cnt=cnt+1; end
			end

		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@#")==1) then
			cnt=cnt+1;
			catNo=catNo+1;
			bExpanded = DawnMod:GetCategoryExpanded(catNo);
			end
		pos=pos+1;
		end
	return cnt;
	end


local function GetFilterItemNameByIndex( index)
	local	pos=1;
	local	cnt=0;
	local	catNo=0;
	local	bExpanded=true;

	while (DawnMod_Words.arrMessageFilters[pos]) do
		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@@")==1) then
			if (bExpanded) then cnt=cnt+1;end
			end

		if (string.find(DawnMod_Words.arrMessageFilters[pos],"@#")==1) then
			cnt=cnt+1;
			catNo=catNo+1;
			bExpanded=DawnMod:GetCategoryExpanded(catNo);
			end

		if (cnt==index) then
			return DawnMod_Words.arrMessageFilters[pos];
			end
		pos=pos+1;
		end
	end


local function FiltersGetOptionColour( bEnabled)
	local	st="|cFF%.2X%.2X%.2X";
	if (bEnabled) then
		st=format(st,colEnabledR*255,colEnabledG*255,colEnabledB*255);
	else	st=format(st,0.7*255,0.7*255,0.7*255);
		end
	return st;
	end


local function FiltersGetItemName( itemNo)
	local	tmp,val,opt;

	tmp = GetFilterItemNameByIndex( itemNo);
	if (string.find(tmp,"@#")) then
		val = "|cFFFFFFFF".. (DawnMod:GetCategoryExpanded( DawnModFilters:GetCategoryIndex(tmp)) and "-" or "+") .." ";
	else	opt=DawnModFilters:GetOption( string.sub(tmp,3), DawnMod:TempConfig()[DawnModConfigItem_FilterOptions]);
		if (opt) then
			val = FiltersGetOptionColour(false).."   ";
		else	val = FiltersGetOptionColour(true).."   ";
			end
		end
	tmp = val.. string.sub(tmp,3) .."|r";
	return tmp;
	end


local function FiltersItemClick( itemNo, obj)
	local	stItemName, tmp, bIsCat;

	stItemName = GetFilterItemNameByIndex(itemNo);
	bIsCat = (string.find(stItemName,"@#")==1);
	stItemName=string.sub(stItemName,3);
        if (bIsCat) then
		tmp = DawnModFilters:GetCategoryIndex(stItemName);
		DawnMod:SetCategoryExpanded( tmp, not DawnMod:GetCategoryExpanded(tmp));
        else
		tmp = DawnModFilters:GetOption( stItemName, DawnMod:TempConfig()[DawnModConfigItem_FilterOptions]);
		tmp = (not tmp);
		DawnModFilters:SetOption( stItemName, tmp, DawnMod:TempConfig()[DawnModConfigItem_FilterOptions]);

		DmCfg:DoTooltipHide( obj);
		SFButton_OnEnter(obj)
		end
	end


local	filter_tt_table = {};

local function FiltersGetItemTooltip( itemNo)
	local	stItemName, opt, bIsCat, stComment;

	stItemName = GetFilterItemNameByIndex(itemNo);
	bIsCat = (string.find(stItemName,"@#")==1);
	stItemName=string.sub(stItemName,3);
	if (not bIsCat) then
		stComment = DawnModFilters:GetGroupComment( stItemName);
		opt=DawnModFilters:GetOption( stItemName, DawnMod:TempConfig()[DawnModConfigItem_FilterOptions]);
		stItemName = FiltersGetOptionColour( not opt)..stItemName.."|r";
	else
		stComment = DawnModFilters:GetCategoryComment( stItemName);
		stItemName = "|cFFFFFFFF"..stItemName.."|r";
		end

	filter_tt_table[1] = stItemName;
	filter_tt_table[2] = stComment;
	filter_tt_table[3] = nil;
	return filter_tt_table;
	end




local function FiltersButton_OnClick( obj)
	local	wig=DmCfg:FindWigetByFrame(obj);
	local	val,stGroup, pos;


	val = (string.find(wig.stName,"Enable")~=nil);
	PrintDbg("val = "..tostring(val));
	pos=1;
	stGroup = " ";
	while (stGroup~="") do
		stGroup=DawnModFilters:GetGroupByIndex(pos);
		if (stGroup~="") then
			DawnModFilters:SetOption( stGroup, val, DawnMod:TempConfig()[DawnModConfigItem_FilterOptions]);
			end
		pos=pos+1;
		end

	wig=DmCfg:FindWiget(nil,"FiltersList","SCROLLBARBTN");
	DmCfg:ConfigChanged();
	end


local	posy;
local function Row(rowNum) return posy-(rowNum*(CheckboxSize)); end
local function HeaderOfs() posy = posy-15;end



function DmCfg:New(obj)
	obj = obj or {};

	setmetatable(obj,self);
    	self.__index   = self;

    	self.bConfigChanged	= false;
    	self.lastCategory	= nil;

	return obj
	end


function DmCfg:Begin()
	local	curfr, fr;

	if (DmCfgFrame~=nil) then return end
	self.bConfigChanged	= false;
	self.lastCategory	= nil;
--	DawnMod:OnConfigWindowShow();

	DmCfgFrame = AddCategory( DawnMod_IdString, nil);
	curfr = DmCfgFrame;

        posy = -40;
        AddTag( curfr, nil, 0,HeaderXOfs,Row(0), DawnMod_IdString,16,"GameFontHighlightLarge");
        AddTag( curfr, nil, 0,HeaderXOfs,Row(1), " v"..DawnMod:GetVersionString(false),14,"GameFontHighlight");
        AddTag( curfr, nil, 0,30, Row(3), "Please see 'DawnMod.txt' (in the zip file) for more", ButtonTagFontSize, ButtonTagFont);
        AddTag( curfr, nil, 0,30, Row(4), "configuration options.", ButtonTagFontSize, ButtonTagFont);

	AddTag( curfr, nil, 0,30, Row(6), "The code to make the Blizzard interface options window", ButtonTagFontSize, ButtonTagFont);
	AddTag( curfr, nil, 0,30, Row(7), "movable is from |cFFFFFFFFLibBetterBlizzOption v1.0|r by Antiarc.", ButtonTagFontSize, ButtonTagFont);


-------------

	curfr = AddCategory("General", DmCfgFrame);

        posy = -10;
        AddTag( curfr, nil, 0,HeaderXOfs,posy, "General",nil,HeaderTagFont);
        HeaderOfs();

	AddConfigCheckBox( curfr, DawnModConfigItem_Enabled, 10, Row(1), "Enabled");
	AddConfigCheckBox( curfr, DawnModConfigItem_TestMode, 120, Row(1), "Test Mode");
	AddConfigCheckBox( curfr, DawnModConfigItem_ShowMinimapButton, 240, Row(1), "Show minimap button");
	AddConfigCheckBox( curfr, DawnModConfigItem_CommunicationEnabled, 10, Row(2), "Addon communication");
	AddConfigCheckBox( curfr, DawnModConfigItem_UpdateNotificationEnabled, 200, Row(2),"Get update notifications");
	AddConfigCheckBox( curfr, DawnModConfigItem_ShowCombatFlags, 10, Row(3),"Show combat flags");

	posy = -160;
	AddTag( curfr, nil, 0,150,posy,"Fixes",nil,HeaderTagFont);
	HeaderOfs();

	AddConfigCheckBox( curfr, DawnModConfigItem_FixCaps, 10, Row(1), "Fix bad capitalization");
	AddConfigCheckBox( curfr, DawnModConfigItem_AllowCaps, 200, Row(1), "Allow ALL CAPS");
	AddConfigCheckBox( curfr, DawnModConfigItem_FixSpelling, 10, Row(2), "Fix spelling");
	AddConfigCheckBox( curfr, DawnModConfigItem_FixSwearing, 200, Row(2), "Fix swearing");
	AddConfigCheckBox( curfr, DawnModConfigItem_StripPlayerFlags, 10, Row(3), "Strip player flags");
	AddConfigCheckBox( curfr, DawnModConfigItem_FixChannelChanged, 200, Row(3),"Fix 'channel changed'");

------------------

	curfr = AddCategory("Blocks & Spam", DmCfgFrame);

        posy = -10;
        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Block",nil,HeaderTagFont);
--        HeaderOfs();
--	posy = posy+10;

	AddConfigCheckBox( curfr, DawnModConfigItem_BlockRecount, 10, Row(1), "Block recount,   except while in a...");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockRecountExceptParty, 50, Row(2), "Party");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockRecountExceptRaid, 200, Row(2), "Raid");

	posy = posy-5;
	AddTag( curfr, nil, 0, 10, Row(3.5), "Block instance general chat while in a ...", ButtonTagFontSize, ButtonTagFont);
	posy = posy+7;
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockPartyZoneGeneral, 50, Row(4.5), "Party");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockRaidZoneGeneral, 200, Row(4.5), "Raid");

	posy = posy-5;
	AddTag( curfr, nil, 0, 10, Row(6), "Block 'global channel':", ButtonTagFontSize, ButtonTagFont);
	posy = posy+7;
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockGlobalChannelAscii, 10, Row(7), "ASCII 'art'");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockGlobalChannelForeign, 125, Row(7), "Foreign language");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockGlobalChannelMultiline, 275, Row(7), "Multiline macros");

	posy = posy-5;
	AddTag( curfr, nil, 0, 10, Row(8.5), "Strip 'global channel':", ButtonTagFontSize, ButtonTagFont);
	posy = posy+7;
	AddConfigCheckBox( curfr, DawnModConfigItem_StripGlobalChannelRaidMarks, 50, Row(9.5), "Raid marks");
	AddConfigCheckBox( curfr, DawnModConfigItem_StripGlobalChannelUrls, 200, Row(9.5), "Urls");

        posy = -290;
        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Kill Spam",nil,HeaderTagFont);
--        HeaderOfs();
--        posy = posy+10;

	AddConfigCheckBox( curfr, DawnModConfigItem_KillSpamAnnoy, 10, Row(1), "Annoying");
	AddConfigCheckBox( curfr, DawnModConfigItem_KillSpamLink, 200, Row(1), "Link");
	AddConfigCheckBox( curfr, DawnModConfigItem_KillSpamGold, 10, Row(2), "Gold");
	AddConfigCheckBox( curfr, DawnModConfigItem_KillSpamGuild, 200, Row(2), "Guild");
	AddConfigCheckBox( curfr, DawnModConfigItem_KillSpamMod, 10, Row(3), "Mod/Addon");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockChannelInvite, 200, Row(3), "Channel Invites");

------------------

	curfr = AddCategory("Lockous & Karma", DmCfgFrame);

        posy = -10;
        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Lockouts",nil,HeaderTagFont);
        HeaderOfs();

	AddTag( curfr, nil, 0, 10, Row(1), "Message lockout period", ButtonTagFontSize, ButtonTagFont);
	AddSlider( curfr, "LockOutMsgTime", DawnModConfigItem_LockOutMsgTime, 150, Row(2), 200,"")
	AddConfigCheckBox( curfr, DawnModConfigItem_LockOutMsgGlobalOnly, 150, Row(3), "Only global channels");

	AddTag( curfr, nil, 0, 10, Row(4.5), "Poster lockout period", ButtonTagFontSize, ButtonTagFont);
	AddSlider( curfr, "LockOutPosterTime", DawnModConfigItem_LockOutPosterTime, 150, Row(5.5), 200,"Blah")
	AddConfigCheckBox( curfr, DawnModConfigItem_LockOutPosterGlobalOnly, 150, Row(6.5), "Only global channels");

        posy = -240;
        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Karma",nil,HeaderTagFont);

	AddConfigCheckBox( curfr, DawnModConfigItem_KarmaUse, 10, Row(1), "Use karma");
	AddConfigCheckBox( curfr, DawnModConfigItem_PosterCacheSave, 160, Row(1), "Save");
	AddConfigCheckBox( curfr, DawnModConfigItem_KarmaPlonk, 250, Row(1), "Ignore for bad");

	posy = posy-5;
	AddTag( curfr, nil, 0, 10, Row(2), "Suspend karma for: ", ButtonTagFontSize, ButtonTagFont);
	posy = posy+7;
	AddConfigCheckBox( curfr, DawnModConfigItem_KarmaSuspendGroup, 160, Row(2), "Group");
	AddConfigCheckBox( curfr, DawnModConfigItem_KarmaSuspendGuild, 250, Row(2), "Guild");

	AddTag( curfr, nil, 0, 10, Row(4), "No. of karmas", ButtonTagFontSize, ButtonTagFont);
	AddSlider( curfr, "PosterCacheLength", DawnModConfigItem_PosterCacheLength, 120, Row(4), 280,"")

	AddTag( curfr, nil, 0, 10, Row(5.5), "Purge after (days)", ButtonTagFontSize, ButtonTagFont);
	AddSlider( curfr, "PosterCachePurgeDays", DawnModConfigItem_PosterCachePurgeDays, 140, Row(5.5), 200,"")
------------------

	curfr = AddCategory("Whispers", DmCfgFrame);

        posy = -10;
        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Block Whispers",nil,HeaderTagFont);
        HeaderOfs();

	posy = posy +10;
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockWhispers, 10, Row(1), "Block whispers");
	AddTag( curfr, nil, 0, 80, Row(2), "except from...", ButtonTagFontSize, ButtonTagFont);
	posy = posy+8;
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockWhispersExceptAnswers, 200, Row(2), "Answers");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockWhispersExceptFriends, 10, Row(3), "Friends");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockWhispersExceptGuild, 200, Row(3), "Guild");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockWhispersExceptParty, 10, Row(4), "Party");
	AddConfigCheckBox( curfr, DawnModConfigItem_BlockWhispersExceptRaid, 200, Row(4), "Raid");


	posy = -190;
	AddTag( curfr, nil, 0,HeaderXOfs,posy,"Trap Whispers",nil,HeaderTagFont);
	HeaderOfs();
	posy = posy-5;
	AddTag( curfr, nil, 0, 10, Row(1), "Trap whispers while in", ButtonTagFontSize, ButtonTagFont);
	posy = posy+7;
	AddConfigCheckBox( curfr, DawnModConfigItem_TrapWhispersParty, 160, Row(1), "Party");
	AddConfigCheckBox( curfr, DawnModConfigItem_TrapWhispersRaid, 250, Row(1), "Raid");
	AddConfigCheckBox( curfr, DawnModConfigItem_TrapWhispersCombatOnly, 150, Row(2), "Only while in combat");
	AddConfigCheckBox( curfr, DawnModConfigItem_TrapWhispersReplyOnlyKnown,100,Row(3),"Only auto-reply if sender is known");

------------------

        local	colAccept=100;
        local	colAsk=150;
        local	colDeny=200;
        local	colNotify=270;
        local	colWord=340;

	curfr = AddCategory("Invites", DmCfgFrame);
        posy = -10;

        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Invites",nil,HeaderTagFont);
        HeaderOfs();

        AddTag( curfr, nil, 0, 100, Row(1), "Accept", ButtonTagFontSize, ButtonTagFont);
        AddTag( curfr, nil, 0, 150, Row(1), "Ask", ButtonTagFontSize, ButtonTagFont);
        AddTag( curfr, nil, 0, 200, Row(1), "Deny", ButtonTagFontSize, ButtonTagFont);
        AddTag( curfr, nil, 0, 265, Row(1), "Don't", ButtonTagFontSize, ButtonTagFont);
        AddTag( curfr, nil, 0, 260, Row(1.5), "tell me", ButtonTagFontSize, ButtonTagFont);

        AddTag( curfr, nil, 0, 340, Row(1), "Invite", ButtonTagFontSize, ButtonTagFont);
        AddTag( curfr, nil, 0, 340, Row(1.5), "word", ButtonTagFontSize, ButtonTagFont);

        AddTag( curfr, nil, 0, 10, Row(2), "Friends", ButtonTagFontSize, ButtonTagFont);
	AddCheckBox( curfr, "InviteFriendsAccept", DawnModConfigItem_InviteAcceptFriends, colAccept, Row(2), "",
		InviteCheckBox_OnClick, {"Accept invites from friends."});
	AddCheckBox( curfr, "InviteFriendsAsk", DawnModConfigItem_InviteAcceptFriends, colAsk, Row(2), "",
		InviteCheckBox_OnClick, {"Ask about invites from friends."});
	AddCheckBox( curfr, "InviteFriendsDeny", DawnModConfigItem_InviteAcceptFriends, colDeny, Row(2), "",
		InviteCheckBox_OnClick, {"Deny invites from friends."});
	AddCheckBox( curfr, "InviteFriendsNotify", DawnModConfigItem_InviteAcceptFriends, colNotify, Row(2), "",
		InviteCheckBox_OnClick, {"Don't notify me about invites from friends."});
	AddCheckBox( curfr, "InviteFriendsWord", DawnModConfigItem_InviteAcceptFriends, colWord, Row(2), "",
		InviteCheckBox_OnClick, {"Friends get invited when they whisper you 'word'."});

        AddTag( curfr, nil, 0, 10, Row(3), "Guild", ButtonTagFontSize, ButtonTagFont);
	AddCheckBox( curfr, "InviteGuildAccept", DawnModConfigItem_InviteAcceptGuild, colAccept, Row(3), "",
		InviteCheckBox_OnClick, {"Accept invites from guild members."});
	AddCheckBox( curfr, "InviteGuildAsk", DawnModConfigItem_InviteAcceptGuild, colAsk, Row(3), "",
		InviteCheckBox_OnClick, {"Ask about invites from guild members."});
	AddCheckBox( curfr, "InviteGuildDeny", DawnModConfigItem_InviteAcceptGuild, colDeny, Row(3), "",
		InviteCheckBox_OnClick, {"Deny invites from guild members."});
	AddCheckBox( curfr, "InviteGuildNotify", DawnModConfigItem_InviteAcceptGuild, colNotify, Row(3), "",
		InviteCheckBox_OnClick, {"Don't notify me about invites from guild members."});
	AddCheckBox( curfr, "InviteGuildWord", DawnModConfigItem_InviteAcceptGuild, colWord, Row(3), "",
		InviteCheckBox_OnClick, {"Guild members get invited when they whisper you 'word'."});

        AddTag( curfr, nil, 0, 10, Row(4), "Whitelist", ButtonTagFontSize, ButtonTagFont);
	AddCheckBox( curfr, "InviteWhitelistAccept", DawnModConfigItem_InviteAcceptWhitelist, colAccept, Row(4), "",
		InviteCheckBox_OnClick, {"Accept invites from whitelist."});
	AddCheckBox( curfr, "InviteWhitelistAsk", DawnModConfigItem_InviteAcceptWhitelist, colAsk, Row(4), "",
		InviteCheckBox_OnClick, {"Ask about invites from whitelist."});
	AddCheckBox( curfr, "InviteWhitelistDeny", DawnModConfigItem_InviteAcceptWhitelist, colDeny, Row(4), "",
		InviteCheckBox_OnClick, {"Deny invites from whitelist."});
	AddCheckBox( curfr, "InviteWhitelistNotify", DawnModConfigItem_InviteAcceptWhitelist, colNotify, Row(4), "",
		InviteCheckBox_OnClick, {"Don't notify me about invites from whitelist."});
	AddCheckBox( curfr, "InviteWhitelistWord", DawnModConfigItem_InviteAcceptWhitelist, colWord, Row(4), "",
		InviteCheckBox_OnClick, {"Whitelisted people get invited when they whisper you 'word'."});

        AddTag( curfr, nil, 0, 10, Row(5), "Answers", ButtonTagFontSize, ButtonTagFont);
	AddCheckBox( curfr, "InviteAnswersAccept", DawnModConfigItem_InviteAcceptAnswers, colAccept, Row(5), "",
		InviteCheckBox_OnClick, {"Accept invites from people you've whispered."});
	AddCheckBox( curfr, "InviteAnswersAsk", DawnModConfigItem_InviteAcceptAnswers, colAsk, Row(5), "",
		InviteCheckBox_OnClick, {"Ask about invites from people you've whispered."});
	AddCheckBox( curfr, "InviteAnswersDeny", DawnModConfigItem_InviteAcceptAnswers, colDeny, Row(5), "",
		InviteCheckBox_OnClick, {"Deny invites from people you've whispered."});
	AddCheckBox( curfr, "InviteAnswersNotify", DawnModConfigItem_InviteAcceptAnswers, colNotify, Row(5), "",
		InviteCheckBox_OnClick, {"Don't notify me about invites from people you've whispered."});
	AddCheckBox( curfr, "InviteAnswersWord", DawnModConfigItem_InviteAcceptAnswers, colWord, Row(5), "",
		InviteCheckBox_OnClick, {"People you've whispered get invited when they whisper you 'word'."});

        AddTag( curfr, nil, 0, 10, Row(6), "Unknown", ButtonTagFontSize, ButtonTagFont);
	AddCheckBox( curfr, "InviteUnknownAccept", DawnModConfigItem_InviteAcceptUnknown, colAccept, Row(6), "",
		InviteCheckBox_OnClick, {"Accept invites from unknown people."});
	AddCheckBox( curfr, "InviteUnknownAsk", DawnModConfigItem_InviteAcceptUnknown, colAsk, Row(6), "",
		InviteCheckBox_OnClick, {"Ask about invites from unknown people."});
	AddCheckBox( curfr, "InviteUnknownDeny", DawnModConfigItem_InviteAcceptUnknown, colDeny, Row(6), "",
		InviteCheckBox_OnClick, {"Deny invites from unknown people."});
	AddCheckBox( curfr, "InviteUnknownNotify", DawnModConfigItem_InviteAcceptUnknown, colNotify, Row(6), "",
		InviteCheckBox_OnClick, {"Don't notify me about invites from unknown people."});
	AddCheckBox( curfr, "InviteUnknownWord", DawnModConfigItem_InviteAcceptUnknown, colWord, Row(6), "",
		InviteCheckBox_OnClick, {"Unknown people you've whispered get invited when they whisper you 'word'."});

	AddConfigCheckBox( curfr, DawnModConfigItem_InviteOverridePve,80,Row(8),"Override if queued for PvE");
	AddConfigCheckBox( curfr, DawnModConfigItem_InviteOverridePvp,80,Row(9),"Override if queued for PvP");

	AddTag( curfr, nil, 0, 10, -290, "Invite word", ButtonTagFontSize, ButtonTagFont);
	AddEditBox( curfr, "InviteWordEdit", nil, 90, -220, 300, 150,
		InviteWordEdit_OnEnterPressed);

	AddTag( curfr, nil, 0,10, Row(12), "|cFFA0A0A0Checks are done from the top of the list down.  For example, if|r", ButtonTagFontSize, ButtonTagFont);
	AddTag( curfr, nil, 0,10, Row(12.75), "|cFFA0A0A0you are invited by someone who is both on your friends list and is|r", ButtonTagFontSize, ButtonTagFont);
	AddTag( curfr, nil, 0,10, Row(13.5), "|cFFA0A0A0in the same guild as you, only the setting for 'friends' applies.|r", ButtonTagFontSize, ButtonTagFont);
------------------

	curfr = AddCategory("Filters", DmCfgFrame);
        posy = -10;

        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Filters",nil,HeaderTagFont);

	AddButton( curfr, "FiltersBtn_EnableAll", 0, 50,-60, 120, "Filter all",
		FiltersButton_OnClick, {"Enable all filters"});
	AddButton( curfr, "FiltersBtn_DisableAll", 0, 200,-60, 120, "Allow all",
		FiltersButton_OnClick, {"Disable all filters"});

	AddScrollFrame( curfr, "FiltersList", 50, -100, 300, 260, 16,false,
		FiltersGetNumItems, FiltersGetItemName, FiltersItemClick, FiltersGetItemTooltip);

------------------

	curfr = AddCategory("Whitelist", DmCfgFrame);
        posy = -10;

        AddTag( curfr, nil, 0,HeaderXOfs,posy, "Whitelist",nil,HeaderTagFont);

	AddTag( curfr, nil, 0, 10, -55, "Add name", ButtonTagFontSize, ButtonTagFont);
	AddEditBox( curfr, "WhitelistNew", nil, 80, 16, 300, 150,
		WlebOnEnterPressed);


	AddScrollFrame( curfr, "WhitelistNames", 10, -110, 200, 160, 10,true,
		WlGetNumItems, WlGetItemName, WlItemClick, WlGetTooltip);
	AddButton( curfr, "WhitelistDelete", 0, 50,-290, 100, "delete",
		WhitelistDelete_OnClick, {"delete entry"});


	AddTag( curfr, nil, 0, 250, Row(4), "Whitelist groups", ButtonTagFontSize, ButtonTagFont);
	AddCheckBox( curfr, "WhitelistCbAnswer", 0, 250, Row(5), "Answers",
		WhitelistCheckBox_OnClick, {"Whitelist people who you've whispered."});
	AddCheckBox( curfr, "WhitelistCbFriend", 0, 250, Row(6), "Friends",
		WhitelistCheckBox_OnClick, {"Whitelist friends."});
	AddCheckBox( curfr, "WhitelistCbGuild", 0, 250, Row(7), "Guild",
		WhitelistCheckBox_OnClick, {"Whitelist guild members"});
	AddCheckBox( curfr, "WhitelistCbParty", 0, 250, Row(8), "Party",
		WhitelistCheckBox_OnClick, {"Whitelist party members"});
	AddCheckBox( curfr, "WhitelistCbRaid", 0, 250, Row(9), "Raid",
		WhitelistCheckBox_OnClick, {"Whitelist raid members"});

	AddTag( curfr, nil, 0,10, Row(13), "|cFFA0A0A0Messages from people on the whitelist|r", ButtonTagFontSize, ButtonTagFont);
	AddTag( curfr, nil, 0,10, Row(13.75), "|cFFA0A0A0will not be blocked, filtered or|r", ButtonTagFontSize, ButtonTagFont);
	AddTag( curfr, nil, 0,10, Row(14.5), "|cFFA0A0A0modified in any way.|r", ButtonTagFontSize, ButtonTagFont);

--------------

--	PrintDbg("wigets = "..tostring(DmCfg:CountWigets()));


	colEnabledR,colEnabledG,colEnabledB,colEnabledA =
		DmCfg:FindWiget( DawnModConfigItem_Enabled,nil,"TEXT").frame:GetTextColor();

	DmCfg:ConfigChanged();

        makeMovable(InterfaceOptionsFrame);

--	if (UnitName("PLAYER")=="Woozie") then DawnMod:ShowConfigWindow(); end
	end



--------------

function DmCfg:GetFrame()
	if (self.lastCategory) then
		return self.lastCategory;
	else	return DmCfg:FindWiget( nil, "General", "FRAME").frame;
		end
	end


function DmCfg:CheckBox_OnShow( obj, bChecked)
--	PrintDbg("cb->onshow");
	end



--local function AddCheckBox( parentFrame, stNameIn, idNum, xpos, ypos, stTag)
--AddCheckBox( curfr, nil, DawnModConfigItem_LockOutMsgGlobalOnly, 150, Row(3), "Only global channels");


function DmCfg:CheckBox_OnClick( obj)
	local	bChecked, wig;

--	PrintDbg("CheckBox_onClick");
	bChecked = (obj:GetChecked() == 1);

	if (bChecked) then
		PlayConfigSound(CS_CbChecked);
	else	PlayConfigSound(CS_CbUnchecked);
		end
	wig = DmCfg:FindWigetByFrame(obj);
	if (wig.fnOnClick) then wig.fnOnClick(obj, bChecked); end
	DmCfg:ConfigChanged();
	end


function DmCfg:CheckBox_OnEnter( obj)
	local	wig=DmCfg:FindWigetByFrame(obj);
	DmCfg:DoTooltipShow( obj, wig.tooltip);
	end


function DmCfg:CheckBox_OnLeave( obj)
	DmCfg:DoTooltipHide(obj);
	end


function DmCfg:DoTooltipShow( obj, tooltip)
	local	index = 2, st;

	if (not DawnMod:GetConfigWindowShowTooltips() or (tooltip==nil)) then return end

	if (type(tooltip)=="table") then
		st = tooltip[1];
	else	st = tooltip;
		end

	GameTooltip:SetOwner(obj, "ANCHOR_BOTTOM")
	GameTooltip:SetText(st,NORMAL_FONT_COLOR.r, NORMAL_FONT_COLOR.g, NORMAL_FONT_COLOR.b, 1);

	if (type(tooltip)=="table") then
		while (tooltip[index] ~= nil) do
			GameTooltip:AddLine( tooltip[index]);
			index=index+1;
			end
		end
	GameTooltip:Show();
	end


function DmCfg:DoTooltipHide( obj)
	if (GameTooltip:GetOwner()==obj) then GameTooltip:Hide();end
	end


function DmCfg:Cancel_OnClick()
	DawnMod:OnConfigWindowClose();
	end


function DmCfg:Defaults_OnClick()
	DawnMod:OnConfigWindowDefault();
	DmCfg:ConfigChanged();
	end


function DmCfg:Save_OnClick()
	DawnMod:OnConfigWindowSave();
	DawnMod:OnConfigWindowClose();
	end


function DmCfg:Slider_OnLoad( obj)
	local	minval,maxval,step, stStr, cfgNum;

	minval,maxval,step,stStr = DawnMod:GetNumericValueData(obj:GetID());

--	assert(step~=0);

        if (minval==nil) then PrintDbg("#"..tostring(obj:GetID()).." minval is nil.");end
        if (maxval==nil) then PrintDbg("#"..tostring(obj:GetID()).." maxval is nil.");end
        if (step==0) then PrintDbg("#"..tostring(obj:GetID()).." step is broken.");end

	_G[obj:GetName().."Text"]:SetText("" );
	_G[obj:GetName().."High"]:SetText(tostring(maxval));
	_G[obj:GetName().."Low"]:SetText(tostring(minval));
	obj:SetMinMaxValues(minval,maxval);
	obj:SetValueStep(step);

	end


function DmCfg:Slider_OnValueChanged( obj)
	DawnMod:CfgSetValue( obj:GetID(), obj:GetValue(), DawnMod:TempConfig());
	_G[obj:GetName().."Text"]:SetText(tostring(obj:GetValue()));
	DmCfg:ConfigChanged( true);
	if (DawnMod:IsDebug()) then DawnMod:PrintConfig( obj:GetID(), DawnMod:TempConfig(), false); end
	end


function DmCfg:Slider_OnShow( obj)
	local	temp;

	DmCfg:Slider_OnLoad(obj); -- This doesn't appear to be getting called by the blizzard UI

	temp = DawnMod:CfgGetValue( obj:GetID(), DawnMod:TempConfig());
	if (temp) then
		obj:SetValue( temp);
		_G[obj:GetName().."Text"]:SetText(tostring(temp));
		end
	end



function DmCfg:SetSlidersTooltipState( bEnabled)
	local cnt, tab, tabIndex, stTT;

	cnt = 1;

	while(arrWigets[cnt]) do
		if (arrWigets[cnt].stType=="SLIDER") then
			stTT="";
			if (bEnabled) then
				stTT = DawnMod:GetConfigDataValue( arrWigets[cnt].idNum, DawnModConfigDataType_TextDescription).."\n";
				tab = DawnMod:GetConfigDataValue( arrWigets[cnt].idNum, DawnModConfigDataType_LongDescription);
				tabIndex = 1;
				while (tab[tabIndex] ~= nil) do
					stTT = stTT..tab[tabIndex];
					tabIndex=tabIndex+1;
					end
				end
			arrWigets[cnt].frame.tooltipText = stTT;
			end
		cnt=cnt+1;
		end
	end

--	SetInviteCheckBoxes( arrWigets[pos], tmp, "Invite"..tmp2)
local function SetInviteCheckBoxes( wigAsk)
	local	wig, valInvite, valNotifyFlag, valInviteFlag, stNameStub;

    	if (string.find(wigAsk.stName,"Friend")) then stNameStub="Friends"; end
    	if (string.find(wigAsk.stName,"Answers")) then stNameStub="Answers"; end
    	if (string.find(wigAsk.stName,"Guild")) then stNameStub="Guild"; end
    	if (string.find(wigAsk.stName,"Whitelist")) then stNameStub="Whitelist"; end
    	if (string.find(wigAsk.stName,"Unknown")) then stNameStub="Unknown"; end
    	stNameStub = "Invite"..stNameStub;

	valInvite, valNotifyFlag,valInviteFlag = DawnMod:InviteFlags( DawnMod:CfgGetValue( wigAsk.idNum,DawnMod:TempConfig()));
--	PrintDbg("#"..tostring(wigAsk.idNum).."  valInv = "..tostring(valInvite).."  fl = "..tostring(valNotifyFlag));

	DmCfg:FindWiget( nil, stNameStub.."Notify", "CHECKBOX").frame:SetChecked( valNotifyFlag);
	DmCfg:FindWiget( nil, stNameStub.."Word", "CHECKBOX").frame:SetChecked( valInviteFlag);
	DmCfg:FindWiget( nil, stNameStub.."Accept", "CHECKBOX").frame:SetChecked( (valInvite==DawnModInviteValue_Accept) );
	DmCfg:FindWiget( nil, stNameStub.."Ask", "CHECKBOX").frame:SetChecked( (valInvite==DawnModInviteValue_Ask) );
	DmCfg:FindWiget( nil, stNameStub.."Deny", "CHECKBOX").frame:SetChecked( (valInvite==DawnModInviteValue_Deny) );
	end


function DmCfg:Refresh()
	local	pos, tmp, tmp2, bHandled;

--	PrintDbg("-Refresh "..(DawnMod_ConfigWindowVisible and "shown" or "hidden"));
	if (not DawnMod_ConfigWindowVisible) then return end
	pos=1;
	while(arrWigets[pos] ~=nil)do
		bHandled = false;
--		PrintDbg("("..tostring(pos)..") #"..tostring(arrWigets[pos].idNum).." nam = >"..tostring(arrWigets[pos].stName).."< type = >"..tostring(arrWigets[pos].stType).."<");

		if (string.find(arrWigets[pos].stName,"ShowTooltips") and
		    (arrWigets[pos].stType=="CHECKBOX") ) then
			if (DawnMod:GetConfigWindowShowTooltips()) then
				tmp=1;
			else	tmp=0;
				end
			arrWigets[pos].frame:SetChecked(tmp);
			end

		if (string.find(arrWigets[pos].stName,"WhitelistCb") and
		    (arrWigets[pos].stType=="CHECKBOX") ) then
			tmp="@"..string.sub(arrWigets[pos].stName, string.len("WhitelistCb")+1);
			tmp = FindStringInTable( tmp, Whitelist());
			if (tmp>0) then tmp=1;else tmp=0;end
			arrWigets[pos].frame:SetChecked(tmp);
			end

		if (string.find(arrWigets[pos].stName,"^Invite")) then
			if (string.find(arrWigets[pos].stName,"Ask") and
			    (arrWigets[pos].stType=="CHECKBOX") ) then
--			    	PrintDbg("wig = "..arrWigets[pos].stName);
			    	SetInviteCheckBoxes( arrWigets[pos]);
			    	end
		    	bHandled = true;
		    	end
		if (string.find(arrWigets[pos].stName,"InviteWordEdit")) then
			arrWigets[pos].frame:SetText( DawnMod:CfgGetValue( DawnModConfigItem_InviteWord, DawnMod:TempConfig()));
			arrWigets[pos].frame:SetCursorPosition(0);
			bHandled=true;
			end

		if ((arrWigets[pos].idNum~=0) and (not bHandled)) then
			if (arrWigets[pos].stType=="CHECKBOX")then
				if (arrWigets[pos].idNum~=0) then
				    	tmp=DawnMod:CfgGetValue( arrWigets[pos].idNum, DawnMod:TempConfig());
				    	if (tmp)then tmp=1;else tmp=0;end
					arrWigets[pos].frame:SetChecked(tmp);
					end
				end
			if (arrWigets[pos].stType=="SLIDER")then
				tmp=DawnMod:CfgGetValue( arrWigets[pos].idNum, DawnMod:TempConfig());
				arrWigets[pos].frame:SetValue(tmp);
--				PrintDbg("nam = "..arrWigets[pos].stName.." = "..tostring(tmp));
--				_G[arrWigets[pos].frame:GetName().."Text"]:SetText(tostring(tmp));
				end
			end
		if (arrWigets[pos].stType=="SCROLLBARBTN")then
			arrWigets[pos].frame:UpdateScrollChildRect();
			DmCfg:ScrollFrameBtn_OnUpdate( arrWigets[pos].frame);
			end
		pos=pos+1;
		end
	end


function DmCfg:ConfigChanged()
--	PrintDbg("update");
	if (not DawnMod_ConfigWindowVisible) then return end
	DmCfg:SetSlidersTooltipState( DawnMod:GetConfigWindowShowTooltips());

	if (DawnMod:CfgGetValue( DawnModConfigItem_CommunicationEnabled, DawnMod:TempConfig()) ) then
		DmCfg:FindWiget( DawnModConfigItem_UpdateNotificationEnabled,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_UpdateNotificationEnabled,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
	else	DmCfg:FindWiget( DawnModConfigItem_UpdateNotificationEnabled,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_UpdateNotificationEnabled,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		end

	if (DawnMod:CfgGetValue( DawnModConfigItem_FixCaps, DawnMod:TempConfig()) ) then
		DmCfg:FindWiget( DawnModConfigItem_AllowCaps,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_AllowCaps,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
	else	DmCfg:FindWiget( DawnModConfigItem_AllowCaps,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_AllowCaps,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
		end

	if (DawnMod:CfgGetValue( DawnModConfigItem_BlockWhispers, DawnMod:TempConfig()) ) then
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptAnswers,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptAnswers,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptFriends,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptFriends,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptGuild,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptGuild,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptParty,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptParty,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptRaid,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptRaid,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
	else
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptAnswers,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptAnswers,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptFriends,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptFriends,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptGuild,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptGuild,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptParty,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptParty,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptRaid,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_BlockWhispersExceptRaid,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		end

	if (DawnMod:CfgGetValue( DawnModConfigItem_BlockRecount, DawnMod:TempConfig()) ) then
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptParty,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptParty,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptRaid,nil,"CHECKBOX").frame:Enable();
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptRaid,nil,"TEXT").frame:SetTextColor(colEnabledR,colEnabledG,colEnabledB,1);
	else
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptParty,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptParty,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptRaid,nil,"CHECKBOX").frame:Disable();
		DmCfg:FindWiget( DawnModConfigItem_BlockRecountExceptRaid,nil,"TEXT").frame:SetTextColor(0.5,0.5,0.5,1);
		end

	if (Whitelist()[1]==nil) then
		DmCfg:FindWiget(nil,"WhitelistDelete",nil).frame:Hide();
	else	DmCfg:FindWiget(nil,"WhitelistDelete",nil).frame:Show();
		end

	if (DmCfg:FindWiget(nil,"WhitelistNames",nil).selected) then
		DmCfg:FindWiget(nil,"WhitelistDelete",nil).frame:Enable();
	else	DmCfg:FindWiget(nil,"WhitelistDelete",nil).frame:Disable();
		end

	DmCfg:Refresh();
	end


