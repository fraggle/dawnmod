
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end

local	PceName		= 1;
local	PceScore	= 2;
local	PceLastSeen	= 3;

local	DawnModPosterData = {
	scoreMin,
	scoreMax,
	scoreDef,
	maxStored,
	num	= 0,
	arr	= {} -- [1]name#1, [2]score#1, [3]lastseen#1, [4]name#2, [5]score#2, [6]lastseen#2,
	};

DawnModPosterCache = {};

function DawnModPosterCache:New(obj)
	obj = obj or {};

    	obj.scoreMin = 0;
  	obj.scoreMax = 0;

	setmetatable(obj,self);
    	self.__index   = self;
    	
	return obj
	end


function DawnModPosterCache:Begin( scoreMinIn, scoreMaxIn, scoreDefIn, posterDataIn)
	local	bDataInBad;
	
	assertnum(scoreMinIn);
	assertnum(scoreMaxIn);
	assertnum(scoreDefIn);
	assert( (type(posterDataIn)=="table") or (posterDataIn==nil) );
	bDataInBad = false;
	
	if (posterDataIn) then
		DawnModPosterData = posterDataIn;
		if (not self:Assert()) then bDataInBad = true; end
		end
	
	if ((posterDataIn==nil) or bDataInBad) then
		DawnModPosterData = {};
		DawnModPosterData.scoreMin	= scoreMinIn;
		DawnModPosterData.scoreMax	= scoreMaxIn;
		DawnModPosterData.scoreDef	= scoreDefIn;
		DawnModPosterData.maxStored	= 0;
		DawnModPosterData.num		= 0;
		DawnModPosterData.arr		= DawnModPosterData.arr or {};
		end
	
	if (not self:Assert()) then
		PrintDbg( "DawnModPosterCache_Begin: Self assertion failed.");
		end
	end


function DawnModPosterCache:Finish()
	local	index, tds;
	
	tds = DawnMod:TimeDateString();
	for index=1,DawnModPosterData.num do
		if (type(DawnModPosterData.arr[index][PceLastSeen]) == "number") then
			DawnModPosterData.arr[index][PceLastSeen] = tds;
			end
		end
	return DawnModPosterData;
	end


function DawnModPosterCache:DoPurge( numDays)
	local	secs, curtdnum, index, cnt, tempnum, tdstr, bDel, numPurged;
	
	numPurged	= 0;
	secs		= numDays * 24 * 60 * 60;
	curtdnum	= DawnMod:TimeDateNum(DawnMod:TimeDateString());
	assert( curtdnum);
	

	index = 1;
	while (index<=DawnModPosterData.num) do
		bDel = false;
		if (type(DawnModPosterData.arr[index][PceLastSeen])=="string") then
			tempnum = DawnMod:TimeDateNum( DawnModPosterData.arr[index][PceLastSeen]);
			if (tempnum==nil) then
				PrintDbg("DawnModPosterCache_DoPurge: Index #"..tostring(index).." has invalid time-date '"..tostring(tdstr).."'.");
				bDel = true;
				end
			if ((not bDel) and (tempnum>curtdnum)) then
				PrintDbg("DawnModPosterCache_DoPurge: time-date at index #"..tostring(index).." is newer than the current date!");
				bDel = true;
				end
			if ((not bDel) and (curtdnum-tempnum>=secs)) then
				bDel = true;
				end
			end
		if (bDel) then
			self:DeleteEntry(index);
			numPurged = numPurged + 1;
		else	index = index+1;
			end
		end
	return numPurged;
	end	


function DawnModPosterCache:Assert()
	local	index, tmp, curTime;
	
	if ((DawnModPosterData==nil)or(type(DawnModPosterData)~="table")) then
		PrintDbg("PosterCache_Assert: data is nil or not a table.");
		return false;end
	if (type(DawnModPosterData.scoreMin)~="number") then
		PrintDbg("PosterCache_Assert: data.scoreMin is not a number.");
		return false;end
	if (type(DawnModPosterData.scoreMax)~="number") then
		PrintDbg("PosterCache_Assert: data.scoreMax is not a number.");
		return false;end
	if (type(DawnModPosterData.scoreDef)~="number") then
		PrintDbg("PosterCache_Assert: data.scoreDef is not a number.");
		return false;end
	if (type(DawnModPosterData.maxStored)~="number") then
		PrintDbg("PosterCache_Assert: data.maxStored is not a number.");
		return false;end
	if (type(DawnModPosterData.num)~="number") then
		PrintDbg("PosterCache_Assert: data.num is not a number.");
		return false;end
	if ((DawnModPosterData.arr==nil) or (type(DawnModPosterData.arr)~="table")) then
		PrintDbg("PosterCache_Assert: data.arr is nil or not a table.");
		return false;end

	if (DawnModPosterData.scoreMin>DawnModPosterData.scoreMax) then
		PrintDbg("PosterCache_Assert: data.min > data.max");
		return false;end
	if (DawnModPosterData.num>DawnModPosterData.maxStored) then
		PrintDbg("PosterCache_Assert: data.num ("..tostring(DawnModPosterData.num)..") > data.maxStored ("..tostring(DawnModPosterData.maxStored)..")");
		 return false;end
	
	index=0;
	while (DawnModPosterData.arr[index+1]~=nil) do
		index=index+1;
		end
	if (DawnModPosterData.num~=index) then
		PrintDbg("PosterCache_Assert: Number of entries in the table ("..tostring(index)..") doesn't match data.num ("..tostring(DawnModPosterData.num)..")");
		return false; end
	
	for index=1, DawnModPosterData.num do
		if ((type(DawnModPosterData.arr[index][PceName])~="string") or (DawnModPosterData.arr[index][PceName]=="")) then 
			PrintDbg("PosterCache_Assert: #"..tostring(index)..".name = nil (or is an empty string).");
			return false;end
		if ( (type(DawnModPosterData.arr[index][PceScore])~="number") or
		     (DawnModPosterData.arr[index][PceScore]<DawnModPosterData.scoreMin) or
		     (DawnModPosterData.arr[index][PceScore]>DawnModPosterData.scoreMax) ) then
			PrintDbg("PosterCache_Assert: #"..tostring(index).."[PceScore] "..tostring(DawnModPosterData.arr[index][PceScore]).." is out of range.");
		     	return false; end
		tmp= DawnModPosterData.arr[index][PceLastSeen];
		if ((type(tmp)~="string") and (type(tmp)~="number")) then
			PrintDbg("PosterCache_Assert: #"..tostring(index).."[PceLastSeen] is neither a number or a string (is: "..tostring(type(tmp))..").");
			return false; end
		if ((type(tmp)=="string") and (DawnMod:ParseTimeDateString(tmp)==nil)) then
			PrintDbg("PosterCache_Assert: #"..tostring(index).."[PceLastSeen] is an invalid time-date string.");
			return false; end
		curTime = GetTime();
		if ((type(tmp)=="number") and (tmp>curTime)) then
			PrintDbg("PosterCache_Assert: #"..tostring(index)..".last has an invalid value.");
			return false; end
		end
	return true;
	end


function DawnModPosterCache:Wipe()
	DawnModPosterData.num=0;
	table.wipe(DawnModPosterData.arr);
	end


function DawnModPosterCache:GetNumEntries()
	return DawnModPosterData.num;
	end


function DawnModPosterCache:GetDefaultScore()
	return DawnModPosterData.scoreDef;
	end


function DawnModPosterCache:GetMaxStored()
	return DawnModPosterData.maxStored;
	end


function DawnModPosterCache:SetMaxStored( newMaxStored)
	assertnum(newMaxStored);
	assert(newMaxStored >= DawnMod_MinPosterCacheLength);
	assert(newMaxStored <= DawnMod_MaxPosterCacheLength);
	
	DawnModPosterData.maxStored = newMaxStored;
	if (DawnModPosterData.maxStored<DawnModPosterData.num) then
		PrintDbg("karma_SetMaxStored: (max="..tostring(newMaxStored)..") Removing "..tostring(DawnModPosterData.num-DawnModPosterData.maxStored).." entries.");
		end
	while (DawnModPosterData.num>DawnModPosterData.maxStored) do
		self:DeleteEntry(DawnModPosterData.num);
		end
	end	


function DawnModPosterCache:DeleteEntry( index)
	local	cnt;
	
	assertnum(index);
	assert( (index>=0) and (index<=DawnModPosterData.num));
	
	table.remove(DawnModPosterData.arr, index);
	DawnModPosterData.num=DawnModPosterData.num-1;
	end



function DawnModPosterCache:GetEntry( index)
	local	ls;

	assertnum(index);
	assert( (index>=0) and (index<=DawnModPosterData.num));

	ls = DawnModPosterData.arr[index][PceLastSeen];
	if (type(ls)=="string") then ls = -1; end;
	return DawnModPosterData.arr[index][PceName],
		DawnModPosterData.arr[index][PceScore],
		ls;
	end


function DawnModPosterCache:FindName( stName)
	local index, stUprName;
	
	assertstr(stName);
	index=1;
	stUprName = string.upper(tostring(stName));
	while (index<=DawnModPosterData.num) do
		if (DawnModPosterData.arr[index][PceName] == stUprName) then
			return index;
			end
		index=index+1;
		end
	return -1;
	end


function DawnModPosterCache:GetScore( stName)
	local	index;
	
	assertstr(stName);
	index = self:FindName( stName);
	if (index>=1) then return DawnModPosterData.arr[index][PceScore]; end
	return DawnModPosterData.scoreDef;
	end


function DawnModPosterCache:GetLastSeenByName( stName)
	local	num;
	
	assertstr(stName);
	num=self:FindName(stName);
	if (num>=0) then
		num = DawnModPosterData.arr[num][PceLastSeen];
		if (type(num)=="number") then return num;end
		end
	return -1;
	end


function DawnModPosterCache:FindReplacable()
	local	index, difClosest, indexClosest, tmp;
	
	if (DawnModPosterData.num==0) then return -1; end
	if (DawnModPosterData.num==1) then return 1; end
	
	indexClosest = 1;
	if (DawnModPosterData.arr[1][PceScore]>DawnModPosterData.scoreDef) then
		difClosest = DawnModPosterData.arr[1][PceScore]-DawnModPosterData.scoreDef;
	else	difClosest = DawnModPosterData.scoreDef - DawnModPosterData.arr[1][PceScore];
		end
	index=2;
	while (index<=DawnModPosterData.num) do
		tmp=DawnModPosterData.arr[index][PceScore];
		if (tmp==DawnModPosterData.scoreDef) then return index; end
		if (tmp>DawnModPosterData.scoreDef) then
			tmp = tmp - DawnModPosterData.scoreDef;
		else	tmp = DawnModPosterData.scoreDef - tmp;
			end
		if (tmp<difClosest) then
			indexClosest = index;
			difClosest = tmp;
			end
		index=index+1;
		end
	return indexClosest;
	end


function DawnModPosterCache:AddEntry( stName, score, lastSeenIn)
	local	index, stUprName, newScore, lastSeen, tmp, bNew;
	
	assertstr(stName);
	assert( (type(score)=="number") or (score==nil));
	assert( (type(lastSeenIn)=="number") or (lastSeenIn==nil));

	stUprName = string.upper(stName);
	index = self:FindName( stUprName);                     
	bNew = false;
	if (index<0) then
		if (DawnModPosterData.num<DawnModPosterData.maxStored) then
			DawnModPosterData.num = DawnModPosterData.num+1;
			index = DawnModPosterData.num;
			bNew = true;
		else	index=self:FindReplacable();
			end
		end
	
	newScore = score;
	if (newScore==nil) then newScore = DawnModPosterData.scoreDef; end
	lastSeen = lastSeenIn;
	if (lastSeen==nil) then lastSeen = -1; end
	
	if (bNew) then tmp = {}; else tmp = DawnModPosterData.arr[index]; end
	tmp[PceName]	= stUprName;
	tmp[PceScore]	= newScore;
	tmp[PceLastSeen]= lastSeen;
	if (bNew) then table.insert(DawnModPosterData.arr,tmp); end
	return index;
	end


function DawnModPosterCache:GetLastSeenByIndex( index)
	local ls;
	assertnum(index);
	assert( (index>=1) and (index<=DawnModPosterData.num));
	ls = DawnModPosterData.arr[index][PceLastSeen];
	if (type(ls)=="string") then ls = -1; end
	return ls;
	end


function DawnModPosterCache:ChangeScoreByIndex( index, scoreChange, curTime)
	local	bChanged;
	
	assertnum(index);
	assert( (index>=1) and (index<=DawnModPosterData.num));
	assertnum(scoreChange);
	assertnum(curTime);
	
	if ((index<1) or (index>DawnModPosterData.num)) then return nil; end
	
	bChanged = false;
	if ( (scoreChange>0) and (DawnModPosterData.arr[index][PceScore]+scoreChange>DawnModPosterData.scoreMax) ) then
		DawnModPosterData.arr[index][PceScore] = DawnModPosterData.scoreMax;
		bChanged=true;
		end
	if ( (scoreChange<0) and (DawnModPosterData.arr[index][PceScore]+scoreChange<DawnModPosterData.scoreMin) ) then
		DawnModPosterData.arr[index][PceScore] = DawnModPosterData.scoreMin;
		bChanged=true;
		end
		
	if (not bChanged) then
		DawnModPosterData.arr[index][PceScore] = DawnModPosterData.arr[index][PceScore] + scoreChange;
		end

	if (curTime~=0) then self:SetLastSeenByIndex( index, curTime); end
	return DawnModPosterData.arr[index][PceScore];
	end


function DawnModPosterCache:ChangeScoreByName( stName, scoreChange, curTime)
	local	index, stUprName, bChanged;
	
	assertstr(stName);
	assertnum(scoreChange);
	stUprName = string.upper(stName);
	index = self:FindName( stUprName);
	if (index<0) then
		index = self:AddEntry(stUprName);
		end
	return self:ChangeScoreByIndex( index, scoreChange, curTime);
	end


function DawnModPosterCache:SetLastSeenByIndex( index, atTime)
	assertnum(index);
	assert( (index>=1) and (index<=DawnModPosterData.num));
	assertnum(atTime);
	DawnModPosterData.arr[index][PceLastSeen] = atTime;
	end


function DawnModPosterCache:SetLastSeenByName( stName, atTime)
	local	stUprName,index;
	
	assertstr(stName);
	assertnum(atTime);
	
	stUprName = string.upper(stName);
	index = self:FindName(stUprName);
	if (index<0) then index = self:AddEntry(stUprName);end
	return self:SetLastSeenByIndex( index, atTime);
	end
	