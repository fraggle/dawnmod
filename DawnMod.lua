

---------------------------
--- Defines     ---
---------------------------
    DawnMod_IdString            = "DawnMod"; -- can not be more than 16 characters
    DawnMod_ConfigVersion       = 5;
    DawnMod_DBVersion           = 4;

local   DawnMod_ForceDebugMode      = false;

    DawnMod_MinLockOutTimeMsg       = 0;
    DawnMod_MaxLockOutTimeMsg       = 180;
    DawnMod_MinPosterCacheLength    = 10;
    DawnMod_MaxPosterCacheLength    = 1000;
    DawnMod_MinLockOutPosterTime    = 0;
    DawnMod_MaxLockOutPosterTime    = 180;
    DawnMod_MinPosterCachePurgeDays = 0;
    DawnMod_MaxPosterCachePurgeDays = 365;

local   DawnMod_DefLockOutTimePoster    = 5;
local   DawnMod_DefMsgCacheLength       = 80;
local   DawnMod_DefLockOutTimeMsg       = 20;   -- Minimum time (in secs) between spamming the same message
local   DawnMod_DefPosterCacheLength    = 100;
local   DawnMod_DefPosterCachePurgeDays = 3;

    DawnMod_ChangeChannelWaitTime   = 0.5;
local   DawnMod_MsgTickPeriod       = 30;

local   DawnMod_DefFailScore        = 100;

local   DawnMod_MultilineMinTime    = 2;    -- Anything less than this between messages, it's considered multiline

local   DawnMod_FailMessageColour   = "|cFFA0A0A0";
local   DawnMod_FailLinkColour      = "|cFFA0A0A0";

local   Karma_MaxStored             = 100;
local   Karma_ScoreMin              = 0;
local   Karma_ScoreMax              = 200;
local   Karma_ScoreDef              = 130;
local   Karma_ScoreBlockBelow       = 90;



--------------------------
--      Globals         --
--------------------------

local   TimeDateInfo = { year, month, day, hour, min, sec, isdst };

local   bIgnoreOutgoing             = false;
local   bChatStop                   = false;
local   failReason                  = 0;
local   failLevel                   = 0;
local   bDebugOrTest                = false;
local   bMsgIsSystemText            = false;
local   bChannelChangeFixActive     = false;
local   numGroupMembers             = 0;

        DawnMod_ConfigWindowVisible = false;
local   bPreservedDebugMode         = false;

local   dmMsgCache          = nil;
local   dmPosterCache       = nil;
local   dmReply             = nil;
local   dmKnown             = nil;

local   combatWhispers          = {};
local   numCombatWhispers       = 0;
local   stLastCombatWhisperer   = "";
local   versionCheckSent        = 0;

    DawnMod         	    = {};

local   tempConfig          = {};
local   tooltipTable 		= {};

    DawnModDB           = nil;

local   prevMsgData = {
    failReason      = 0;
    failLevel       = 0;
    stMsg           = "";
    stChannel       = "";
    msgType         = 0;
    curTime         = 0;
    msgNo           = 0;
    stOut           = "";
    stSender        = "";
    stSenderFlags   = "";
    informMsgNo     = 0;
    informFailReason    = 0;
    };

local   recountTrackData = {
    bTracking       = false;
    stRecountSender = "";
    time            = 0;
    };

local   msgCountData = {
    msgPeriod       = 0;
    msgPassPeriod   = 0;
    timePeriod      = 0;
    ignoresPeriod   = 0;
    msgTotal        = 0;
    msgPassTotal    = 0;
    timeTotal       = 0;
    ignoresTotal    = 0;
    };


local   arrColourQuality = {
    "9D9D9D",   "grey",
    "FFFFFF",   "white",
    "1EFF00",   "green",
    "0070DD",   "blue",
    "A335EE",   "purple",
    "FF8000",   "orange"
    };

-- Achievement search types
local   AS_Achievement      = 1;
local   AS_Statistic        = 2;
local   AS_GuildAchievement = 3;


-- Message types
local   MsgType_Unknown         =  1;
local   MsgType_Battleground    =  2;
local   MsgType_Battlenet       =  3;
local   MsgType_BattlenetConversation = 4;
local   MsgType_BattlenetInform =  5;
local   MsgType_ChannelGlobal   =  6;
local   MsgType_ChannelPrivate  =  7;
local   MsgType_EmotePlayer     =  8;
local   MsgType_EmoteSystem     =  9;
local   MsgType_Guild           = 10;
local   MsgType_Party           = 11;
local   MsgType_Raid            = 12;
local   MsgType_Say             = 13;
local   MsgType_System          = 14;
local   MsgType_Whisper         = 15;
local   MsgType_WhisperInform   = 16;


local   MsgTypeDataPerLine  = 5;

--  Message name string,    score to be declared    track   ign on  karma
--              spam,           karma   karma   gain4
--                                  clean
local   arrMsgTypeData = {
    "Unknown",      0,          false,  false,    0,        -- 1 Unknown
    "Battleground",     DawnMod_DefFailScore,   true,   true,     1,        -- 2 Battleground
    "Battlenet",        DawnMod_DefFailScore *5,false,  false,  100,        -- 3 Battlenet
    "Battlenet Conversation",DawnMod_DefFailScore *5,false, false,  100,        -- 4 Battlenet
    "Battlenet Inform", DawnMod_DefFailScore *5,false,  false,  100,        -- 5 Battlenet Inform
    "Channel (global)", DawnMod_DefFailScore,   true,   true,     1,        -- 6 ChannelGlobal
    "Channel (private)",    DawnMod_DefFailScore *2,true,   false,    2,        -- 7 ChannelPrivate
    "Emote (player)",   DawnMod_DefFailScore,   true,   true,     1,        -- 8 EmotePlayer
    "Emote (system)",   DawnMod_DefFailScore,   true,   true,     1,        -- 9 EmoteSystem
    "Guild",        DawnMod_DefFailScore *3,true,   false,    5,        --10 Guild
    "Party",        DawnMod_DefFailScore *2,true,   true,     3,        --11 Party
    "Raid",         DawnMod_DefFailScore *2,true,   false,    3,        --12 Raid
    "Say",          DawnMod_DefFailScore,   true,   true,     2,        --13 Say
    "System",       0,          false,  false,    0,        --14 System (this channel doesn't get checked for spam)
    "Whisper",      DawnMod_DefFailScore,   true,   true,     2,        --15 Whisper
    "Whisper Inform",   DawnMod_DefFailScore,   false,  false,    0         --16 Whisper inform
    };


local   arrRaidMarks = {
    "{RT1}","{STAR}",
    "{RT2}","{COIN}","{CIRCLE}",
    "{RT3}","{DIAMOND}",
    "{RT4}","{TRIANGLE}",
    "{RT5}","{MOON}",
    "{RT6}","{SQUARE}",
    "{RT7}","{CROSS}","{X}",
    "{RT8}","{SKULL}"
    };


local   GtType_Unknown          =  1;
local   GtType_Alpha            =  2;
local   GtType_ControlCodes     =  3;
local   GtType_LinkText         =  4;
local   GtType_Num              =  5;
local   GtType_Punctuation      =  6;
local   GtType_RaidMark         =  7;
local   GtType_SystemText       =  8;
local   GtType_Whitespace       =  9;
local   GtType_UTF              = 10;


-- These are used internally by the parsing code (codes must be >= 100)
local   GtType_ControlCode_LinkPart0        = 101;
local   GtType_ControlCode_LinkPart1        = 102;
local   GtType_ControlCode_LinkPart2        = 103;
local   GtType_ControlCode_LinkPart3        = 104;
local   GtType_ControlCode_LinkPart4        = 105;
local   GtType_ControlCode_InColTextTrue    = 110;
local   GtType_ControlCode_InColTextFalse   = 111;

local   arrGtTypeName = {
    "Unknown",
    "Alpha",
    "ControlCodes",
    "Link Text",
    "Num",
    "Punctuation",
    "Raid Mark",
    "System Text",
    "Whitespace",
    "UTF-8"
    };


local   FailReason_Caps             =  1;
local   FailReason_Mod              =  2;
local   FailReason_Gold             =  3;
local   FailReason_Guild            =  4;
local   FailReason_Annoy            =  5;
local   FailReason_Link             =  6;
local   FailReason_RepeatMsg        =  7;
local   FailReason_Ascii            =  8;
local   FailReason_RaidZoneGeneral  =  9;
local   FailReason_PartyZoneGeneral = 10;
local   FailReason_EmptyString      = 11;
local   FailReason_Multiline        = 12;
local   FailReason_Swear            = 13;
local   FailReason_Spelling         = 14;
local   FailReason_Recount          = 15;
local   FailReason_Karma            = 16;
local   FailReason_CombatWhisper    = 17;
local   FailReason_DawnModOutgoing  = 18;
local   FailReason_RepeatPoster     = 19;
local   FailReason_UnknownSender    = 20;
local   FailReason_ForeignLanguage  = 21;
local   FailReason_MessageFilter    = 22;

local   FailLevel_Unknown           = 1;
local   FailLevel_Warning           = 2;
local   FailLevel_Fatal             = 3;


--  search index,           display string,     karma change    fail level
local   arrFailReasonData = {
    FailReason_Caps,        "caps",         -1,     FailLevel_Fatal,
    FailReason_Mod,         "mod spam",     -20,        FailLevel_Fatal,
    FailReason_Gold,        "gold spam",        -1000,      FailLevel_Fatal,
    FailReason_Guild,       "guild spam",       -20,        FailLevel_Fatal,
    FailReason_Annoy,       "annoying",     -50,        FailLevel_Fatal,
    FailReason_Link,        "link",         -50,        FailLevel_Fatal,
    FailReason_RepeatMsg,       "repeat message",   -5,     FailLevel_Fatal,
    FailReason_Ascii,       "ascii",        -20,        FailLevel_Fatal,
    FailReason_RaidZoneGeneral, "raid zone general",    0,      FailLevel_Fatal,
    FailReason_PartyZoneGeneral,    "party zone general",   0,      FailLevel_Fatal,
    FailReason_EmptyString,     "empty string",     -1,     FailLevel_Fatal,
    FailReason_Multiline,       "multi-line",       -10,        FailLevel_Fatal,
    FailReason_Swear,       "swear",        -20,        FailLevel_Warning,
    FailReason_Spelling,        "spell",        -3,     FailLevel_Warning,
    FailReason_Recount,     "recount",      -1000,      FailLevel_Fatal,
    FailReason_Karma,       "karma",        0,      FailLevel_Fatal,
    FailReason_CombatWhisper,   "combat whisper",   0,      FailLevel_Fatal,
    FailReason_DawnModOutgoing, "DawnModOutgoing",  0,      FailLevel_Warning,
    FailReason_RepeatPoster,    "repeat poster",    -5,     FailLevel_Fatal,
    FailReason_UnknownSender,   "unknown sender",   0,      FailLevel_Fatal,
    FailReason_ForeignLanguage, "foreign language", 0,      FailLevel_Fatal,
    FailReason_MessageFilter,   "message filter",   0,      FailLevel_Fatal
    };


DawnModInviteValue_Ask          = 0;
DawnModInviteValue_Accept       = 1;
DawnModInviteValue_Deny         = 2;
DawnModInviteFlag_InviteWord    = 64;
DawnModInviteFlag_DontNotify    = 128;

DawnModConfigItem_ConfigVersion         		=  1;   -- Must be the first entry
DawnModConfigItem_Enabled           			=  2;   -- Keep as 2nd entry
DawnModConfigItem_TestMode         				=  3;
DawnModConfigItem_ShowMinimapButton     		=  4;
DawnModConfigItem_AllowCaps         			=  5;
DawnModConfigItem_BlockChannelInvite            =  6;
DawnModConfigItem_BlockGlobalChannelAscii   	=  7;
DawnModConfigItem_BlockGlobalChannelForeign 	=  8;
DawnModConfigItem_BlockGlobalChannelMultiline 	=  9;
DawnModConfigItem_BlockRaidZoneGeneral      	= 10;
DawnModConfigItem_BlockPartyZoneGeneral     	= 11;
DawnModConfigItem_BlockRecount          		= 12;
DawnModConfigItem_BlockRecountExceptParty   	= 13;
DawnModConfigItem_BlockRecountExceptRaid  	  	= 14;
DawnModConfigItem_BlockWhispers        		 	= 15;
DawnModConfigItem_BlockWhispersExceptAnswers    = 16;
DawnModConfigItem_BlockWhispersExceptFriends    = 17;
DawnModConfigItem_BlockWhispersExceptGuild  	= 18;
DawnModConfigItem_BlockWhispersExceptParty  	= 19;
DawnModConfigItem_BlockWhispersExceptRaid   	= 20;
DawnModConfigItem_CommunicationEnabled      	= 21;
DawnModConfigItem_FixCaps           			= 22;
DawnModConfigItem_FixChannelChanged     		= 23;
DawnModConfigItem_FixChatStickies               = 24;
DawnModConfigItem_FixFastTrack          		= 25;
DawnModConfigItem_FixSpelling           		= 26;
DawnModConfigItem_FixSwearing           		= 27;
DawnModConfigItem_InviteAcceptAnswers       	= 28;
DawnModConfigItem_InviteAcceptFriends       	= 29;
DawnModConfigItem_InviteAcceptGuild     		= 30;
DawnModConfigItem_InviteAcceptUnknown       	= 31;
DawnModConfigItem_InviteAcceptWhitelist     	= 32;
DawnModConfigItem_InviteOverridePve     		= 33;
DawnModConfigItem_InviteOverridePvp     		= 34;
DawnModConfigItem_InviteWord            		= 35;
DawnModConfigItem_KarmaPlonk            		= 36;
DawnModConfigItem_KarmaSuspendGroup     		= 37;
DawnModConfigItem_KarmaSuspendGuild     		= 38;
DawnModConfigItem_KarmaUse          			= 39;
DawnModConfigItem_KillSpamAnnoy         		= 40;
DawnModConfigItem_KillSpamMod           		= 41;
DawnModConfigItem_KillSpamGold          		= 42;
DawnModConfigItem_KillSpamGuild         		= 43;
DawnModConfigItem_KillSpamLink          		= 44;
DawnModConfigItem_LockOutMsgTime        		= 45;
DawnModConfigItem_LockOutMsgGlobalOnly      	= 46;
DawnModConfigItem_LockOutPosterTime     		= 47;
DawnModConfigItem_LockOutPosterGlobalOnly   	= 48;
DawnModConfigItem_PosterCacheLength     		= 49;
DawnModConfigItem_PosterCachePurgeDays      	= 50;
DawnModConfigItem_PosterCacheSave       		= 51;
DawnModConfigItem_ShowCombatFlags       		= 52;
DawnModConfigItem_StripGlobalChannelUrls    	= 53;
DawnModConfigItem_StripGlobalChannelRaidMarks   = 54;
DawnModConfigItem_StripPlayerFlags      		= 55;
DawnModConfigItem_TrapWhispersCombatOnly    	= 56;
DawnModConfigItem_TrapWhispersParty     		= 57;
DawnModConfigItem_TrapWhispersRaid      		= 58;
DawnModConfigItem_TrapWhispersReplyOnlyKnown    = 59;
DawnModConfigItem_UpdateNotificationEnabled 	= 60;
DawnModConfigItem_FilterOptions         		= 61;
DawnModConfigItem_Whitelist         			= 62;


DawnModConfigDataType_TextDescription       = 2;
DawnModConfigDataType_DefaultValue     		= 3;
DawnModConfigDataType_DawnSet           	= 4;
DawnModConfigDataType_AssertInfo        	= 5;
DawnModConfigDataType_LongDescription       = 6;

local   DawnMod_ConfigEntriesPerLine        = 6;


-- These lines don't have to be in any particular order, but I keep them in
-- the same order as the values of the ConfigItem numbers for convenience.

local   arrDawnModConfigData = {
DawnModConfigItem_ConfigVersion,        "Configuration version",        DawnMod_ConfigVersion,  DawnMod_ConfigVersion,  "N",
    {""},
DawnModConfigItem_Enabled,          "Enabled",              true,           true,           "B",
    {"Enable/disable this mod."},
DawnModConfigItem_TestMode,         "Test mode",                false,          false,          "B",
    {"When test mode is active, you can see which messages","DawnMod would otherwise block and gives a brief explaination of why","a given message has been blocked.  Note: Testmode is computationally","expensive than the normal filtering process."},
DawnModConfigItem_ShowMinimapButton,        "Show minimap button",          true,           true,           "B",
    {"Should the minimap button be displayed."},
DawnModConfigItem_AllowCaps,            "Allow all caps",           false,          false,          "B",
    {"Should messages with ALL CAPS be allowed?","(Note: if FIXCAPS is true, this option will be ignored.)"},
DawnModConfigItem_BlockChannelInvite,   "Block channel invites",    false,          true,          "B",
    {"Should invites to chat channels be automatically blocked"},
DawnModConfigItem_BlockGlobalChannelAscii,  "Block global channel ascii",       true,           true,           "B",
    {"Should ascii 'art' be blocked from global chat channels."},
DawnModConfigItem_BlockGlobalChannelForeign,    "Block foreign language",       false,          true,           "B",
    {"Should messages containing foreign language (UTF-8)","characters be blocked "},
DawnModConfigItem_BlockGlobalChannelMultiline,  "Block global channel multiline",   false,           false,           "B",
    {"Should multi-line macros be blocked from global chat channels.","eg. /2 Somedude's [Enchanting]","/2 at low prices","/2 in Dalaran bank"},
DawnModConfigItem_BlockRaidZoneGeneral,     "Block raid zone general",      false,          true,           "B",
    {"Blocks the 'general' chat channel when you're in a raid zone."},
DawnModConfigItem_BlockPartyZoneGeneral,    "Block party zone general",     false,          true,           "B",
    {"Blocks the 'general' chat channel when you're in a party zone (5-man","instance)."},
DawnModConfigItem_BlockRecount,         "Block recount",            true,           true,           "B",
    {"Should recount output be blocked?"},
DawnModConfigItem_BlockRecountExceptParty,  "Block recount outside party",      true,           false,          "B",
    {"Should an exception to BLOCKRECOUNT be made when you're in a party."},
DawnModConfigItem_BlockRecountExceptRaid,   "Block recount outside raid",       true,           true,           "B",
    {"Should an exception to BLOCKRECOUNT be made when you're in a raid."},
DawnModConfigItem_BlockWhispers,        "Block whispers",           false,          false,          "B",
    {"Note that this option does not effect real-ID whispers."},
DawnModConfigItem_BlockWhispersExceptAnswers,   "Don't block whispers from people you whisper", true,       true,           "B",
    {"Makes an exception to 'BlockWhispers' for people to whom you have sent whispers"},
DawnModConfigItem_BlockWhispersExceptFriends,   "Don't block whispers from friends",    true,           true,           "B",
    {"Makes an exception to 'BlockWhispers' for friends."},
DawnModConfigItem_BlockWhispersExceptGuild, "Don't block whispers from guildmates", true,           true,           "B",
    {"Makes an exception to 'BlockWhispers' for guildmates."},
DawnModConfigItem_BlockWhispersExceptParty, "Don't block whispers from party members", true,        true,           "B",
    {"Makes an exception to 'BlockWhispers' for party members."},
DawnModConfigItem_BlockWhispersExceptRaid,  "Don't block whispers from raid members", true,         true,           "B",
    {"Makes an exception to 'BlockWhispers' for raid members."},
DawnModConfigItem_CommunicationEnabled,     "Communicate with other users",     false,          false,          "B",
    {"Allows "..DawnMod_IdString.." to communicate with other users of this mod"},
DawnModConfigItem_FixCaps,          "Fix bad capitalization",       true,           true,           "B",
    {"Should DawnMod fix incorrect usage of ALL CAPS words?","(note: if ALLOWCAPS is set to 'false' then any ALL CAPS message will be","discarded rather than fixed using this option)","ie.  'LFM FOR 25 MAN OS' would be changed to","'LFM for 25 man OS'"},
DawnModConfigItem_FixChannelChanged,        "Fix 'channel changed'",        true,           true,           "B",
    {"Consolidates the 'channel changed' messages when you move between zones."},
DawnModConfigItem_FixChatStickies,      "Fix chat channel stickyness",      false,          true,
"B",
    {"Fixes which chat channels are and are not sticky"},
DawnModConfigItem_FixFastTrack,         "Fix 'fast track'",         false,          true,           "B",
    {"This removes the '(X deposited to your guild back)' from loot messages","if your guild has the 'Fast Track' and 'Cash Flow' guild perks."},
DawnModConfigItem_FixSpelling,          "Fix spelling",             true,           true,           "B",
    {"This will fix some of the bad spelling that you'll come across.","ie.  'plx','plz' and 'plox' will be replaced by 'please'."},
DawnModConfigItem_FixSwearing,          "Fix swearing",             false,          true,           "B",
    {"This will fix some of the swearing you come across."},
DawnModConfigItem_InviteAcceptAnswers,      "Invite accept answers",        0,          0,          "N",
    {"How to handle invites from people you've whispered."},
DawnModConfigItem_InviteAcceptFriends,      "Invite accept friends",        0,          1,          "N",
    {"How to handle invites from friends."},
DawnModConfigItem_InviteAcceptGuild,        "Invite accept guild",          0,          1,          "N",
    {"How to handle invites from guild members."},
DawnModConfigItem_InviteAcceptUnknown,      "Invite accept unknown",        0,          2,          "N",
    {"How to handle invites from unknown people."},
DawnModConfigItem_InviteAcceptWhitelist,    "Invite accept whitelist",      0,          0,          "N",
    {"How to handle invites from whitelisted people."},
DawnModConfigItem_InviteOverridePve,        "Override invite option for PvE",   true,           true,           "B",
    {"Override the auto accept/decline and 'invite word' option if you're in a PvE queue."},
DawnModConfigItem_InviteOverridePvp,        "Override invite option for PvP",   true,           true,           "B",
    {"Override the auto accept/decline and 'invite word' option if you're in a PvP queue."},
DawnModConfigItem_InviteWord,           "Invite word",              "INVITEME",     "INVITEME",     "S",
    {"A word which people people whisper you to receive an invite."},
DawnModConfigItem_KarmaPlonk,           "Ignore people for bad karma",      false,          false,          "B",
    {"With this option, if someone's karma gets low enough, they will be added","to your ignore list so you will never see their messages again."},
DawnModConfigItem_KarmaSuspendGroup,        "Suspend karma for group",      true,           true,           "B",
    {"Karma processing is suspended for people while they are in your party or","raid."},
DawnModConfigItem_KarmaSuspendGuild,        "Suspend karma for guild",      true,           true,           "B",
    {"Karma processing is supended for guild chat."},
DawnModConfigItem_KarmaUse,         "Use karma",                true,           true,           "B",
    {"Should karma be tracked.  Every time someone posts an acceptable","message, their karma increases.  Every time someone posts an","unacceptable message, their karma decreases.  If their karma drops below","a certain threshold, you will no longer see their messages."},
DawnModConfigItem_KillSpamGuild,        "Kill guild spam",          true,           true,           "B",
    {"This will kill guild recruitment spam and the people who spam for more","signatures to start their guild."},
DawnModConfigItem_KillSpamMod,          "Kill mod spam",            true,           true,           "B",
    {"This will kill spam from mods such as Bejeweled and Peggle."},
DawnModConfigItem_KillSpamAnnoy,        "Kill annoying spam",           true,           true,           "B",
    {"Should 'annoying' messages be killed?","ie. Chuck Noris crap or the ever popular 'ANAL [insert ability]'"},
DawnModConfigItem_KillSpamGold,         "Kill gold spam",           true,           true,           "B",
    {"Should gold spam be killed?"},
DawnModConfigItem_KillSpamLink,         "Kill link spam",           true,           true,           "B",
    {"Will remove link spam (eg. [Skullflame Shield] spam)"},
DawnModConfigItem_LockOutMsgTime,       "Lockout per message",          DawnMod_DefLockOutTimeMsg,DawnMod_DefLockOutTimeMsg,"N",
    {"The minimum length of time between repeated messages."," ","Set to 0 to disable this option."},
DawnModConfigItem_LockOutMsgGlobalOnly,     "Lockout messages global only",     true,           false,          "B",
    {"Applies the message lock out only to global channels."},
DawnModConfigItem_LockOutPosterTime,        "Lockout per poster",           DawnMod_DefLockOutTimePoster,5,         "N",
    {"The minimum length of time between repeated messages from a given poster."," ","Set to 0 to disable this option"},
DawnModConfigItem_LockOutPosterGlobalOnly,  "Lockout poster global only",       true,           true,           "B",
    {"Applies the poster lock-out only to global channels."},
DawnModConfigItem_PosterCacheLength,        "Max. number of karmas to store",   DawnMod_DefPosterCacheLength,   250,        "N",
    {"The maximum number of posters which will be recorded."},
DawnModConfigItem_PosterCachePurgeDays,     "Max. number of days to store karmas",  DawnMod_DefPosterCachePurgeDays, 7,     "N",
    {"The number of days before an entry in the poster cache gets purged."},
DawnModConfigItem_PosterCacheSave,      "Save poster data between sessions",    true,           true,           "B",
    {"Should karma values be saved when you log out?"},
DawnModConfigItem_ShowCombatFlags,      "Show combat flags",            true,           true,           "B",
    {"Shows a message when you enter or leave combat."},
DawnModConfigItem_StripGlobalChannelUrls,   "Strip global channel URLs",        false,          true,           "B",
    {"Should URLs be stripped from global chat channels.","(eg.  http://youtube.com)"},
DawnModConfigItem_StripGlobalChannelRaidMarks,  "Strip global channel raid marks",  true,           true,           "B",
    {"Should raid marks (eg {star}, {skull}) be stipped from global chat","channels (eg general, trade, etc)"},
DawnModConfigItem_StripPlayerFlags,     "Strip player flags",           true,           true,           "B",
    {"Should player flags (ie. AFK or DND) be stripped from their chat","messages"},
DawnModConfigItem_TrapWhispersCombatOnly,   "Trap in combat only",          true,           true,           "B",
    {"This setting makes TrapWhispersParty and TrapWhispersRaid only trap","whispers from outside your group if you are in combat.  Also, with this","option enabled, you will be notified of any trapped whispers when you leave","combat."},
DawnModConfigItem_TrapWhispersParty,        "Trap whispers in party",       false,          false,          "B",
    {"Traps whispers from people outside your party while you're in a party.  You","will be notified of any trapped whispers when you leave the party, unless","TrapWhispersCombatOnly is enabled."},
DawnModConfigItem_TrapWhispersRaid,     "Trap whispers in raid",        false,          true,           "B",
    {"Traps whispers from people outside your raid while you're in a raid.  You","will be notified of any Trapped whispers when you leave the raid, unless","TrapWhispersCombatOnly is enabled."},
DawnModConfigItem_TrapWhispersReplyOnlyKnown,   "Trap auto-reply for known only",   true,           true,           "B",
    {"If someone's whisper is trapped, "..DawnMod_IdString.." will only auto-reply if","the sender is known. (ie they're a friend, guildie, party/raid member,","or someone you've whispered)" },
DawnModConfigItem_UpdateNotificationEnabled,    "Tells you if an upgrade is available", true,           true,           "B",
    {"Tells you if there is an upgrade to this mod available."},
DawnModConfigItem_FilterOptions,        "",                 nil,            nil,            "T",
    {" "},
DawnModConfigItem_Whitelist,            "",                 nil,            nil,            "T",
    {" "}
    };


local   DawnMod_NumericValuesPerLine    = 4;

--  value id,               minVal,             maxVal,             defaultVal?
DawnModNumericValueData = {
    DawnModConfigItem_ConfigVersion,    DawnMod_ConfigVersion,      DawnMod_ConfigVersion,      0,
    DawnModConfigItem_InviteAcceptAnswers,  0,              255,                0,
    DawnModConfigItem_InviteAcceptFriends,  0,              255,                0,
    DawnModConfigItem_InviteAcceptGuild,    0,              255,                0,
    DawnModConfigItem_InviteAcceptUnknown,  0,              255,                0,
    DawnModConfigItem_InviteAcceptWhitelist,0,              255,                0,
    DawnModConfigItem_LockOutMsgTime,       DawnMod_MinLockOutTimeMsg,  DawnMod_MaxLockOutTimeMsg,  0,
    DawnModConfigItem_LockOutPosterTime,    DawnMod_MinLockOutPosterTime,   DawnMod_MaxLockOutPosterTime,   0,
    DawnModConfigItem_PosterCacheLength,    DawnMod_MinPosterCacheLength,   DawnMod_MaxPosterCacheLength,   1,
    DawnModConfigItem_PosterCachePurgeDays, DawnMod_MinPosterCachePurgeDays,DawnMod_MaxPosterCachePurgeDays,1
    };



----------------------------
--      Functions         --
----------------------------

local function PrintMsg( stMsg) DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end


function DawnMod:New(obj)
    obj = obj or {};

    setmetatable(obj,self);
        self.__index   = self;

    return obj
    end


function DawnMod:DB() return DawnModDB; end

function DawnMod:PosterCache()  return dmPosterCache; end

function DawnMod:MessageCache() return dmMsgCache; end

function DawnMod:Known() return dmKnown; end

function DawnMod:CurConfig() return DawnModDB.curConfig; end

function DawnMod:TempConfig() return tempConfig; end

function DawnMod:SetCurConfig( cc) DawnModDB.curConfig = cc; end;

function DawnMod:SetTempConfig( tc) tempConfig = tc; end;

function DawnMod:GetPosterCacheLastPurge() return DawnModDB.posterCacheLastPurge; end

function DawnMod:SetPosterCacheLastPurge( nv) assertstr(nv);DawnModDB.posterCacheLastPurge=nv;end

function DawnMod:GetMinimapAngle() return DawnModDB.minimapAngle; end

function DawnMod:SetMinimapAngle( newAngle) assertnum( newAngle);DawnModDB.minimapAngle = newAngle;end

function DawnMod:GetLastUpdateNotification() return DawnModDB.lastUpdateNotification; end

function DawnMod:SetLastUpdateNotification(nv) DawnModDB.lastUpdateNotification=nv;end

function DawnMod:GetPosterData() return DawnModDB.posterData; end

function DawnMod:SetConfigWindowShowTooltips( bNewValue) assertbool(bNewValue); DawnModDB.bCfgWindowShowTooltips = bNewValue; end

function DawnMod:GetConfigWindowShowTooltips() return DawnModDB.bCfgWindowShowTooltips; end

function DawnMod:BlockChat( nv) bChatStop = nv; end

function DawnMod:GetVersion()
    local   ver;
    ver = GetAddOnMetadata(DawnMod_IdString, "Version");
    if (ver==nil) then ver="0"; end
    return ver;
    end


function DawnMod:IsDebug()
    return DawnModDB.bDebugMode;
    end

function DawnMod:SetDebug( bDbg)
    assertbool(bDbg);
    DawnModDB.bDebugMode = bDbg;
    end


function DawnMod:GetHeaderString( bColour)
    local   s, bCol;

    assert( (bColour==nil) or (type(bColour)=="boolean"));
    bCol = bColour;
    if (bCol == nil) then bCol = false; end
    s = "";
    if (bCol) then s=s.."|cFFFFFF00"; end;
    s = s..DawnMod_IdString;
    if (bCol) then s=s.."|r";end
    return s;
    end


function DawnMod:GetVersionString( bColour)
    local   s, bCol;

    assert( (bColour==nil) or (type(bColour)=="boolean"));
    bCol = bColour;
    if (bCol == nil) then bCol = false; end
    s = "";
    if (bCol) then s=s.."|cFFD0D0FF"; end;
    s = s..DawnMod:GetVersion().." (";
    if (DawnMod:IsDebug()) then
        s = s.."debug";
    else    s = s.."release";
        end
    s = s..")";
    if (bCol) then s=s.."|r";end
    return s;
    end


function DawnMod:PrintHeader()
    DEFAULT_CHAT_FRAME:AddMessage(
        DawnMod:GetHeaderString(true)..
        " "..
        DawnMod:GetVersionString(true) );
    end


local function PrintMsg( stMsg)
    assertstr(stMsg);
    DEFAULT_CHAT_FRAME:AddMessage(stMsg);
end


local function PrintDbg( stMsg)
    assertstr(stMsg);
    if (DawnMod:IsDebug()) then
        DEFAULT_CHAT_FRAME:AddMessage("|cff80ffffDbg> "..stMsg.."|r");
    end
end


function DawnMod:PrintDbg( stMsg) PrintDbg(stMsg);end


function DawnMod:PrintNotice( stMsg, chatTypeIn)
    local   chatType;

    assert( (type(chatTypeIn)=="table") or (chatTypeIn==nil));
    if (chatTypeIn==nil) then
        chatType=ChatTypeInfo["RAID_WARNING"];
    else    chatType=chatTypeIn;end
    RaidNotice_AddMessage(RaidBossEmoteFrame, stMsg, chatType);
    end


local function PrintNotice( stMsg, chatTypeIn)DawnMod:PrintNotice(stMsg,chatTypeIn);end


function DawnMod:PrintInfoItem( stTop, stTailIn)
    local   stTail;

    assertstr(stTop);
    assert((type(stTailIn)=="string")or(stRecipient==nil));
    stTail = stTailIn;
    if (stTail==nil) then stTail = "";
    else    stTail = tostring(stTail); end
    DEFAULT_CHAT_FRAME:AddMessage("|cffffff00"..stTop.."|r |cffffffff"..stTail.."|r");
    end


local function StringMatch(st1,st2) assertstr(st1);assertstr(st2);return (st1==st2); end


local function GetCh( stString, nIndex)
    assertstr(stString);
    assertnum(nIndex);
    if ((nIndex<0)or(nIndex>string.len(stString))) then return " "; end
    return string.sub(stString,nIndex,nIndex);
    end


local function ChIsAlpha( ch) return ( ((ch>="a")and(ch<="z")) or ((ch>="A")and(ch<="Z")) ); end


local function ChIsNum( ch) return ((ch>="0")and(ch<="9")); end


local function ChIsAlphanum( ch) return (ChIsAlpha(ch) or ChIsNum(ch)); end


function DawnMod:StrCapitalize( str)
    local stOut;

    assertstr(str);
    if (string.len(str)==0) then return "";end
    stOut = string.upper(string.sub(str,1,1));
    if (string.len(str)>1) then
        stOut= stOut..string.lower( string.sub(str,2));
        end
    return stOut;
    end


function DawnMod:StripPadding( strIn)
-- Removes space characters from the start and end of a string
    local str;

    if (strIn=="") then return ""; end
    str = strIn;
    while (string.find(str," ") == 1) do str = string.sub(str,2); end
    while (string.find(str," ",-1)) do str = string.sub(str,1,string.len(str)-1); end
    return str;
end



function DawnMod:MakeChatColour( stType)
    assertstr(stType);
    return format("|cFF%.2X%.2X%.2X",ChatTypeInfo[stType].r*255,ChatTypeInfo[stType].g*255,ChatTypeInfo[stType].b*255);
    end


function DawnMod:GetNumericValueData( cfgNum)
    local index;

    index=0;
    while (DawnModNumericValueData[index*DawnMod_NumericValuesPerLine+1]~=nil) do
        if (DawnModNumericValueData[index*DawnMod_NumericValuesPerLine+1]==cfgNum) then
            return DawnModNumericValueData[index*DawnMod_NumericValuesPerLine+2],
                DawnModNumericValueData[index*DawnMod_NumericValuesPerLine+3],
                DawnModNumericValueData[index*DawnMod_NumericValuesPerLine+4];
                end
        index=index+1;
        end
    return nil,nil,nil;
    end


function DawnMod:DBAssert() return true; end


function DawnMod:DBUnpack()
    local   bUnpack, bRv;

    bRv = false;

    if (DawnModDB and (DawnModDB.version==DawnMod_DBVersion)) then
        bUnpack = true;
    else    bUnpack = false; end

    if (bUnpack) then

        bRv = true;
    else
        DawnModDB               = {};
        DawnModDB.version           = DawnMod_DBVersion;
        DawnModDB.bDebugMode            = false;
        DawnModDB.curConfig         = nil;
        DawnModDB.posterCacheLastPurge      = nil;
        DawnModDB.minimapAngle          = nil;
        DawnModDB.posterData            = nil;
        DawnModDB.bCfgWindowShowTooltips    = true;
        DawnModDB.lastUpdateNotification    = nil;
        DawnModDB.stCatsExpanded        = "";
        end

    return bRv;
    end


function DawnMod:DBPack()

    DawnModDB.version       = DawnMod_DBVersion;

    if (DawnMod:CfgGetValue(DawnModConfigItem_PosterCacheSave)) then
        DawnModDB.posterData = dmPosterCache:Finish();
    else    DawnModDB.posterData = nil;
        end

    end


function DawnMod:CfgGetNumDatas()
    local   cnt;

    cnt=0;
    while (arrDawnModConfigData[cnt*DawnMod_ConfigEntriesPerLine+1]~=nil) do
        cnt=cnt+1;
        end
    return cnt;
    end


function DawnMod:CfgIsEqual( cfgA, cfgB)
    local   cnt, numDatas, bChecked;

    asserttab(cfgA);
    asserttab(cfgB);
    numDatas = DawnMod:CfgGetNumDatas();
    for cnt=1,numDatas do
        bChecked=false;
        if (cnt==DawnModConfigItem_FilterOptions) then
            if (DawnModFilters:CompareOptionsList(cfgA[cnt],cfgB[cnt])==false) then
                PrintDbg("CfgIsEqual: Filter options do not match");
                return false;
                end
            bChecked=true;
            end
        if (cnt==DawnModConfigItem_Whitelist) then
            if (DawnModWhitelist:CompareWhitelists(cfgA[cnt],cfgB[cnt])==false) then
                PrintDbg("CfgIsEqual: Whitelist options do not match");
                return false;
                end
            bChecked=true;
            end
        if ((not bChecked) and (cfgA[cnt]~=cfgB[cnt])) then
            PrintDbg("CfgIsEqual: index #"..tostring(cnt).."   a = "..tostring(cfgA[cnt]).."  b = "..tostring(cfgB[cnt]));
            return false;
            end
        end
    return true;
    end


function DawnMod:CfgChanged()
    if (DawnMod_ConfigWindowVisible) then
        DawnMod:SetDebug( bPreservedDebugMode);
        end

    dmMsgCache:SetMaxLength( DawnMod:CfgGetValue(DawnModConfigItem_LockOutMsgTime) * 4);    -- make the cache able to handle up to 4 msg per sec.

    if (dmPosterCache:GetMaxStored()~=DawnMod:CfgGetValue(DawnModConfigItem_PosterCacheLength)) then
        dmPosterCache:SetMaxStored( DawnMod:CfgGetValue(DawnModConfigItem_PosterCacheLength));
        end

    if (DawnMod:CfgGetValue(DawnModConfigItem_FixChannelChanged) ~= bChannelChangeFixActive) then
        bChannelChangeFixActive = DawnMod:CfgGetValue(DawnModConfigItem_FixChannelChanged);
        if (bChannelChangeFixActive) then
            ChatFrame_AddMessageEventFilter( "CHAT_MSG_CHANNEL_NOTICE", DawnMod_ChannelHandler);
        else
            ChatFrame_RemoveMessageEventFilter( "CHAT_MSG_CHANNEL_NOTICE", DawnMod_ChannelHandler);
            end
        end

    DawnModMinimapButton:SetVisibility( DawnMod:CfgGetValue(DawnModConfigItem_ShowMinimapButton) );
    end


function DawnMod:GetConfigDataValue( cfgNum, cfgDataType)
    local   cnt;

    assertnum(cfgNum);
    assertnum(cfgDataType);
    cnt=0;
    while (arrDawnModConfigData[cnt*DawnMod_ConfigEntriesPerLine+1]~=nil) do
        if (arrDawnModConfigData[cnt*DawnMod_ConfigEntriesPerLine+1]==cfgNum) then
            return arrDawnModConfigData[cnt*DawnMod_ConfigEntriesPerLine+cfgDataType];
            end
        cnt=cnt+1;
        end
    return nil;
    end


function DawnMod:CfgReset( cfg, bPrintMsg, bDoCfgChanged)
    local   cnt, numEntries, tmp, bChanged, valtype;

    assert((type(cfg)=="table") or (cfg==nil));
    assert((type(bPrintMsg)=="boolean")or(bPrintMsg==nil) );

    if (cfg==nil) then
        cfg = {};
        bChanged=true;
        end
    table.wipe(cfg);

    numEntries = DawnMod:CfgGetNumDatas();
    cnt=1;
    bChanged = false;
    while(cnt<=numEntries) do
        tmp = DawnMod:GetConfigDataValue( cnt, DawnModConfigDataType_DefaultValue);
        valtype = DawnMod:GetConfigDataValue( cnt,DawnModConfigDataType_AssertInfo);
        if (valtype=="T") then tmp = {}; end
        cfg[cnt] = tmp;
        cnt=cnt+1;
        end

    if ((cfg==DawnMod:CurConfig()) and bDoCfgChanged) then
        DawnMod:CfgChanged();
        end

    if ((bPrintMsg~=nil)and bPrintMsg) then
        DawnMod:PrintHeader();
        DawnMod:PrintInfoItem("Config reset");
        end
    return cfg,bChanged;
    end


function DawnMod:CfgAssertValue( index, cfgval)
    local   tmp, minval,maxval,bChecked;

    assertnum(index);
    if ((index<1) or (index>DawnMod:CfgGetNumDatas()) ) then
        PrintDbg("CfgAssertValue: index out of range.");
        return false;end
    if (cfgval==nil) then
        PrintDbg("AssertCfgValue: #"..tostring(index).." is nil.");
        return false;end
    tmp = DawnMod:GetConfigDataValue( index,DawnModConfigDataType_AssertInfo);
    if ((tmp=="B")and(type(cfgval)~="boolean"))then
        PrintDbg("AssertCfgValue: Expected type 'boolean' at index #"..tostring(index));
        return false;end
    if (tmp=="N") then
        if (type(cfgval)~="number") then
            PrintDbg("AssertCfgValue: Expected type 'number' at index #"..tostring(index));
            return false;
            end
        minval,maxval = DawnMod:GetNumericValueData( index);
        if ((cfgval<minval)or(cfgval>maxval)) then
            if (cfgval<minval) then PrintDbg("AssertCfgValue: #"..tostring(index).." ("..tostring(cfgval)..") < ("..tostring(minval)..") min.");end
            if (cfgval>maxval) then PrintDbg("AssertCfgValue: #"..tostring(index).." ("..tostring(cfgval)..") > ("..tostring(maxval)..") max.");end
            return false;
            end
        end
    if (tmp=="S") then
        if (type(cfgval)~="string") then
            PrintDbg("AssertCfgValue: Expected type 'string' at index #"..tostring(index));
            return false;
            end
        bChecked=true;
        end
    if (tmp=="T") then
        if (type(cfgval)~="table") then
            PrintDbg("AssertCfgValue: Expected type 'table' at index #"..tostring(index));
            return false;
            end
        bChecked=false;
        if (index==DawnModConfigItem_FilterOptions) then
            if (not DawnModFilters:AssertOptions(cfgval)) then
                PrintDbg("AssertCfgValue: Assertion for Filter Options failed");
                return false;
                end
            bChecked=true;
            end
        if (index==DawnModConfigItem_Whitelist) then
            if (not DawnModWhitelist:AssertList(cfgval)) then
                PrintDbg("AssertCfgValue: Assertion for Whitelist failed");
                return false;
                end
            bChecked=true;
            end
        if (not bChecked) then
            PrintDbg("AssertCfgValue: Unknown table value at index #"..tostring(index)..".");
            return false;
            end
        end

    return true;
    end


function DawnMod:CfgAssert( cfg)
    local cnt, numEntries;

    if ((cfg==nil) or (type(cfg)~="table")) then
        PrintDbg("CfgAssert: cfg is nil or not a table.");
        return false;end

    numEntries = DawnMod:CfgGetNumDatas();
    for cnt=1,numEntries do
        if (not DawnMod:CfgAssertValue( cnt, cfg[cnt])) then
            PrintDbg("DawnMod.CfgAssert: Failed at index #"..tostring(cnt));
            return false;
            end
        end
    return true;
    end


function DawnMod:CfgSetValue( index, newValue, cfgToSet)
    local   cfg;

    assertnum(index);
    if (cfgToSet == nil) then cfg = DawnMod:CurConfig();
    else    cfg = cfgToSet;
        end

        if (not DawnMod:CfgAssertValue( index, newValue)) then return false; end
        cfg[index] = newValue;
        if (cfg==DawnMod:CurConfig()) then DawnMod:CfgChanged(); end
        return true;
        end


function DawnMod:CfgGetValue( index, cfgToGet)
    local   cfg;

    assertnum(index);
    if ((index<1)or(index>DawnMod:CfgGetNumDatas()) )then
        PrintDbg("DM_CfgGetValue: Attempt to index value #"..tostring(index)..".");
        assert(1<0);
        end

    if (cfgToGet == nil) then cfg = DawnMod:CurConfig();
    else    cfg = cfgToGet; end
        return cfg[index];
        end


function DawnMod:CfgCopy( srcCfg, destCfg)
    local   cnt, bChanged, numDatas, cnt2,valtype;

    asserttab(srcCfg);
    asserttab(destCfg);
    if (not DawnMod:CfgAssert( srcCfg)) then
        PrintDbg("DawnMod.CfgCopy: Asserting src config failed.");
        return;
        end
    DawnMod:CfgReset( destCfg, false, false);

    numDatas = DawnMod:CfgGetNumDatas();
    for cnt=1,numDatas do
        valtype = DawnMod:GetConfigDataValue( cnt, DawnModConfigDataType_AssertInfo);
        if (valtype=="T") then
            cnt2=1;
            while(srcCfg[cnt][cnt2]) do
                destCfg[cnt][cnt2] = srcCfg[cnt][cnt2];
                cnt2=cnt2+1;
                end
        else    destCfg[cnt] = srcCfg[cnt];
            end
        end
    if (destCfg==DawnMod:CurConfig()) then
        DawnMod:CfgChanged();
        end
    end


function DawnMod:GetCategoryExpanded( index)
    assertnum(index);
    if (string.len( DawnModDB.stCatsExpanded)>=index) then
        return (GetCh(DawnModDB.stCatsExpanded,index)=="1");
    else    return true;
        end
    end


function DawnMod:SetCategoryExpanded( index, bExpanded)
    assertnum(index);
    assertbool(bExpanded);
    while (string.len(DawnModDB.stCatsExpanded)<(index-1)) do
        DawnModDB.stCatsExpanded=DawnModDB.stCatsExpanded.."1";
        end
    DawnModDB.stCatsExpanded =
        string.sub(DawnModDB.stCatsExpanded,1,index-1)..
        (bExpanded and "1" or "0")..
        string.sub(DawnModDB.stCatsExpanded,index+1);
    end


function DawnMod:GetLongName( stName)
    assertstr(stName);
    if (string.find(stName,"-")==nil) then
        return stName.."-"..GetRealmName();
        end
    return stName;
    end


function DawnMod:GetShortName( stName)
    local   pos;
    assertstr(stName);
    pos = string.find(stName,"-");
    if (pos~=nil) then return string.sub(stName,1,pos-1);end
    return stName;
    end


function DawnMod:ShortNameIfSameRealm( stName)
    local pos;
    assertstr(stName);

    pos = string.find( stName, "-");
    if (pos==nil) then return stName; end
    if (StringMatch( string.upper(GetRealmName()), string.sub( string.upper(stName),pos+1))) then
        return string.sub( stName, 1, pos-1);
        end
    return stName;
    end



function DawnMod:PlayerIsInRaidZone()
    local st,rz;
    st,rz=GetInstanceInfo();
    return (string.upper(rz)=="RAID");
    end


function DawnMod:PlayerIsInPartyZone()
    local st,rz;
    st,rz=GetInstanceInfo();
    return (string.upper(rz)=="PARTY");
    end


function DawnMod:PlayerIsInRaid()
    return (GetNumRaidMembers()>0);
    end


function DawnMod:PlayerIsInParty()
    return (GetNumPartyMembers()>0);
    end


function DawnMod:NumInGroup()
	local n;

	n = GetNumRaidMembers();
	if (n > 0) then
		return n;
		end
	n = GetNumGroupMembers();
	if (n > 0) then
		return n;
		end
    return 0;
    end


-- This is a debugging function
function DawnMod:ShowReal( stMsg)
    local   s;
    s,_=string.gsub( stMsg,"|","$");
    return s;
    end


function DawnMod:GetNumTrappedWhispers()
    return numCombatWhispers;
    end


function DawnMod:TooltipTableAddLine( stLine)
    local   cnt;

    cnt = 1;

    while (tooltipTable[cnt] ~= nil) do
        cnt = cnt+1;
        end
    tooltipTable[ cnt] = stLine;
    end


function DawnMod:TooltipZoneInfo()
    local   stName,stType,difNo,stDif, st;

    stName, stType, difNo, stDif = GetInstanceInfo();

    st = DawnMod:GetPlayerPosString();
    if ((stType=="party") or (stType=="raid")) then
        st = st.." (";
        if (stType=="party") then
            if (difNo==1) then st=st.."normal";
            else st=st.."heroic";end
            end
        if (stType=="raid") then
            if (difNo==1) then st=st.."10 normal";end
            if (difNo==2) then st=st.."25 normal";end
            if (difNo==3) then st=st.."10 heroic";end
            if (difNo==4) then st=st.."25 heroic";end
            end
        st=st..")";
        end
    return st;
    end


function DawnMod:TooltipLFGCD()
    local x, st;

    x=GetLFGRandomCooldownExpiration();
    if (x~=nil) then
        x = x - GetTime();
        st = format("%2i mins %2i secs",x/60,x%60);
    else    st = "none";
        end
    return "LFG CD: "..st;
    end


function DawnMod:TooltipTrappedWhisperInfo( nLineNo)
    local st;

    st = nil;
    if (DawnMod:GetNumTrappedWhispers()== 0) then return nil; end
    if (nLineNo==1) then
        st = "Trapped whispers: "..tostring(DawnMod:GetNumTrappedWhispers());
        end
    if (nLineNo==2) then
        st = "|cFFA0A0FFRight click icon or '/DM TW'|r";
        end
    if (nLineNo==3) then
        st = "|cFFA0A0FF  retrieve trapped whisper(s).|r";
        end
    return st;
    end


function DawnMod:GetTooltipLine( lineNo)
    local   st, cnt;
    if (lineNo==1) then
        tooltipTable = {};

        DawnMod:TooltipTableAddLine(" ");

        cnt=1;
        repeat
            st = DawnMod:TooltipTrappedWhisperInfo(cnt);
            if (st~=nil) then DawnMod:TooltipTableAddLine( st); end
            cnt=cnt+1;
        until (st==nil);

        st = DawnMod:TooltipZoneInfo();
        if (st~=nil) then DawnMod:TooltipTableAddLine( st); end

        st = DawnMod:TooltipLFGCD();
        if (st~=nil) then DawnMod:TooltipTableAddLine( st); end
        end;
    if (tooltipTable[lineNo] == nil) then
        tooltipTable = nil;
        return nil;
        end
    return tooltipTable[ lineNo];
    end


local function IsIndependantWord( stToken, stString, pos)
    local   lenWrd, lenStr;

    lenWrd = string.len(stToken);
    lenStr = string.len(stString);
    return (  ((pos==1) or (not ChIsAlphanum(GetCh(stString,pos-1)))) and
          ((pos+lenWrd > lenStr) or (not ChIsAlphanum(GetCh(stString,pos+lenWrd)))) );
    end


function DawnMod:GetFailReasonData( nFailReason)
    local   cnt;

    assertnum(nFailReason);
    cnt=1;
    while (arrFailReasonData[cnt]~=nil) do
        if (arrFailReasonData[cnt]==nFailReason) then
            return arrFailReasonData[cnt+1],arrFailReasonData[cnt+2],arrFailReasonData[cnt+3];
            end
        cnt=cnt+4;
        end
    return "NoError",0;
    end


local function UTFBytes(str,pos, firstMask, firstMatch, numNextBytes)
    local   ofs;

    if (pos+numNextBytes>string.len(str)) then return false,1; end

    ofs = 1;
    if (bit.band(string.byte(str,pos,pos),firstMask)~=firstMatch) then
        return false, 1;
        end

    while (ofs <= numNextBytes) do
        if (bit.band(string.byte(str,pos+ofs,pos+ofs),0xC0)~=0x80) then
            return false, 1;
            end
        ofs=ofs+1;
        end
    return true, numNextBytes+1;
    end


local function IsUTF8Char( stMsg)
    local bIsUTF, bNumChars;

    bIsUTF,bNumChars = UTFBytes( stMsg, 1, 0xE0,0xC0, 1);
    if (not bIsUTF) then
        bIsUTF,bNumChars = UTFBytes( stMsg, 1, 0xF0,0xE0,2);
        end
    if (not bIsUTF) then
        bIsUTF,bNumChars = UTFBytes( stMsg, 1, 0xF8,0xF0,3);
        end
    if (not bIsUTF) then
        bIsUTF,bNumChars = UTFBytes( stMsg, 1, 0xFC, 0xF8,4);
        end
    if (not bIsUTF) then
        bIsUTF,bNumChars = UTFBytes( stMsg, 1, 0xFE, 0xFC,5);
        end
    return bIsUTF, bNumChars;
    end


local   new_GtData = {
    linkPart    = 0;
    bInColText  = false;
    pos     = 0;
    };


local function TokenPart_CheckSysText( tokenType)
    if (new_GtData.bInColText) then return GtType_SystemText;
    else    return tokenType; end
    end


local function TokenPart( str)
    local   ch, cnt, stUpr, pos, bIsUTF;
    assertstr(str);

    ch = GetCh(str,1);
    stUpr = string.upper(str);


--  PrintMsg("linkpart: "..tostring(new_GtData.linkPart).." ");
-- ### Section 1 ##
-- Tests for GtType_ControlCodes

-- GtType_ControlCodes - Pre Link Text
    if ((new_GtData.linkPart==1) and (ch=="[")) then
        return "", GtType_ControlCode_LinkPart2;
        end

-- GtType_ControlCodes - End of ColorText
    if ((ch=="|") and (GetCh(stUpr,2)=="R")) then
        return string.sub( str,1,2), GtType_ControlCode_InColTextFalse;
        end

-- GtType_LinkText
    if (new_GtData.linkPart==2) then
        pos = string.find(stUpr, "%]");
        if (pos == nil) then pos = string.len(stUpr);end
        return string.sub( str, 1, pos), GtType_ControlCode_LinkPart3;
        end

-- GtType_ControlCodes - Link End
    if (new_GtData.linkPart==3) then
        return "", GtType_ControlCode_LinkPart4;
        end


-- GtType_ControlCodes - Link
    if ((ch=="|") and (GetCh(stUpr,2)=="H")) then
        if (new_GtData.linkPart==4) then
            return string.sub(str,1,2), GtType_ControlCode_LinkPart0;
        else
            pos = string.find( string.sub(stUpr,2),"|H");
            if (pos==nil) then pos = string.len(stUpr);
            else    pos = pos+2;
                end
            return string.sub(str,1,pos), GtType_ControlCode_LinkPart1;
            end
        end

-- GtType_RaidMark
    if (ch == "{") then
        cnt=1;
        while (arrRaidMarks[cnt]~=nil) do
            if (string.sub(stUpr,1,string.len(arrRaidMarks[cnt]))==arrRaidMarks[cnt]) then
                return arrRaidMarks[cnt], GtType_RaidMark;
                end
            cnt=cnt+1;
            end
        end

-- GtType_ControlCodes - Color
    if (string.find(string.sub(stUpr,1,10),"%|C%x%x%x%x%x%x%x%x")) then
        return string.sub(str,1,10), GtType_ControlCode_InColTextTrue;
        end


-- ### Section 2 ##
-- These tests must be after the GtType_ControlCodes tests.

-- GtType_Alpha
    if (ChIsAlpha(ch)) then return ch, TokenPart_CheckSysText(GtType_Alpha); end
-- GtType_Num
    if (ChIsNum(ch)) then return ch, TokenPart_CheckSysText(GtType_Num); end
-- GtType_Whitespace
    if (ch==" ") then return ch, TokenPart_CheckSysText(GtType_Whitespace); end

-- GtType_UTF8
    bIsUTF,cnt = IsUTF8Char( str);
    if (bIsUTF) then
        return string.sub(str,1,cnt), TokenPart_CheckSysText(GtType_UTF);
        end
-- GtType_Punctuation - Everything else has been eliminated.
    return ch, TokenPart_CheckSysText(GtType_Punctuation);
    end


function DawnMod:GetToken( strIn, argNo)
    local   stPart, provId, provId2, stToken, stTokenPart, tmpId,cc, startPos, sanity;

    assertstr( strIn);
    assertnum( argNo);

    sanity = 0;
    bDone = false;
    stToken = "";
    if (argNo==0) then
        new_GtData.pos = 1;
        new_GtData.linkPart = 0;
        new_GtData.bInColText = false;
        end
    startPos = new_GtData.pos;

    while (not bDone) do

        sanity = sanity + 1;
        if (sanity >= 1000) then
            if (DawnMod:IsDebug()) then DawnMod:PrintHeader(); end
            PrintDbg("'GetToken': Sanity failure");
            PrintDbg(">"..strIn.."<");
            Printdbg("REAL >"..DawnMod:ShowReal(strIn).."<");
            return nil,nil,nil;
            end

        if (new_GtData.pos>string.len(strIn)) then
            if (new_GtData.pos==startPos) then
                --return "", GtType_EOS, posIn;
                return nil,nil,nil;
            else
                return string.sub(strIn, startPos, new_GtData.pos-1), provId, new_GtData.pos;
                end
            end
        stTokenPart, tmpId = TokenPart( string.sub( strIn, new_GtData.pos));
--      PrintMsg(">"..string.sub(strIn, new_GtData.pos).."<   ["..tostring(tmpId).."]");
        cc = 0;
        if (tmpId>=100) then
            cc = tmpId;
            tmpId=GtType_ControlCodes;
            if (cc==GtType_ControlCode_LinkPart3) then tmpId=GtType_LinkText;end
            end
        if (new_GtData.pos == startPos) then provId = tmpId; end
        if (tmpId~=provId) then return stToken, provId, new_GtData.pos; end
        if (cc>=100) then
            if (cc==GtType_ControlCode_LinkPart0) then new_GtData.linkPart = 0; end
            if (cc==GtType_ControlCode_LinkPart1) then new_GtData.linkPart = 1; end
            if (cc==GtType_ControlCode_LinkPart2) then new_GtData.linkPart = 2; end
            if (cc==GtType_ControlCode_LinkPart3) then new_GtData.linkPart = 3; end
            if (cc==GtType_ControlCode_LinkPart4) then new_GtData.linkPart = 4; end
            if (cc==GtType_ControlCode_InColTextTrue) then new_GtData.bInColText = true;end
            if (cc==GtType_ControlCode_InColTextFalse) then new_GtData.bInColText = false;end
            end
        stToken = stToken..stTokenPart;
        new_GtData.pos = new_GtData.pos + string.len(stTokenPart);
        end
    end


local function CountUTF8Chars( stMsg)
    local pos,numUTF, numBytes;
    pos=1;
    numUTF = 0;
    while (pos<=string.len(stMsg)) do
        _,numBytes=IsUTF8Char(stMsg);
        pos=pos+numBytes;
        numUTF=numUTF+1;
        end
    return numUTF;
    end


function DawnMod:GTTokenLength( stToken, tokenType)
    local   iLength = 0;

    assert(type(stToken)=="string");
    assert(type(tokenType)=="number");
    if (stToken == nil) then return 0; end

    if ((tokenType == GtType_Alpha) or
        (tokenType == GtType_Num) or
        (tokenType == GtType_Punctuation) or
        (tokenType == GtType_Whitespace) or
        (tokenType == GtType_LinkText) )then
        iLength = string.len( stToken);
        end
    if (tokenType == GtType_UTF) then
        iLength = CountUTF8Chars( stToken);
        end
    if (tokenType == GtType_RaidMark) then iLength = 1; end
    return iLength;
    end


function DawnMod:GetWordCount( stString)  -- Counts alpha & num tokens
    local   cnt, wordCount, bMore, tokenType,stToken;

    assert(type(stString)=="string");
    cnt             = 0;
    wordCount       = 0;
    bMore           = true;
    while (bMore) do
        stToken,tokenType,cnt = DawnMod:GetToken( stString,cnt);
        if (stToken==nil) then
            bMore = false;
        else    if ((tokenType==GtType_Alpha) or
                (tokenType==GtType_Num) ) then
                wordCount=wordCount+1;
                end;
            end
        end
    return wordCount;
    end


function DawnMod:StripByTokenType( stMsg, nTokenType)
    local   bMore,stToken,tokenType, nIndex, stOut, bStripped;

    assertstr(stMsg);
    assertnum(nTokenType);
    nIndex      = 0;
    bMore       = true;
    bStripped   = false;
    stOut       = "";
    while (bMore) do
        stToken,tokenType,nIndex = DawnMod:GetToken( stMsg, nIndex);
        if (stToken~=nil) then
            if (tokenType==nTokenType) then
                bStripped = true;
            else    stOut=stOut..stToken;
                end
        else    bMore=false;
            end
        end
    return stOut, bStripped;
    end


function DawnMod:StripLinks( stMsg)
    assert(type(stMsg)=="string");
    return DawnMod:StripByTokenType( stMsg, GtType_LinkText);
    end


function DawnMod:StripRaidMarks( stMsg)
    assert(type(stMsg)=="string");
    return DawnMod:StripByTokenType( stMsg, GtType_RaidMark);
    end


function DawnMod:CheckSpamWords( stMsg, wrds, checkType)
    local   total, cnt, cnt2, stMsgUpr, bMore, stToken,tokenType,
        bSpacesChecked;

    assert(type(stMsg)=="string");
    assert(type(wrds)=="table");
    assert(type(checkType)=="number");
    cnt             = 0;
    total           = 0;
    bMore           = true;
    stMsgUpr    = string.upper(stMsg);
    bSpacedChecked  = false;

    while (bMore) do
        stToken,tokenType,cnt = DawnMod:GetToken(stMsgUpr,cnt);
        if (bMsgIsSystemText) then return 0;end
        if (stToken~=nil) then
            if ( ((checkType==GtType_Alpha) and (tokenType==GtType_Alpha)) or
                 ((checkType==GtType_LinkText) and (tokenType==GtType_LinkText)) ) then
                cnt2=0;
                if ((checkType==GtType_LinkText)and(tokenType==GtType_LinkText)) then
                    stToken=string.sub(stToken,2,string.len(stToken)-1); -- strip the '[' and ']' from link text.
                    end
                while (wrds[cnt2+1]~=nil) do
                    if (string.find(wrds[cnt2+1]," ")~=nil) then
                        if ((not bSpacesChecked) and (string.find(stMsgUpr,wrds[cnt2+1])~=nil)) then
                            total = total + wrds[cnt2+2];
                            bSpacesChecked = true;
                            end
                    else    if (StringMatch(stToken,wrds[cnt2+1])) then
                            total = total + wrds[cnt2+2];
                            end
                        end
                    cnt2=cnt2+2;
                    end
                end
            if ((checkType==GtType_Punctuation) and (tokenType==GtType_Punctuation)) then
                cnt2=0;
                while (wrds[cnt2+1]~=nil) do
                    if (string.find(stToken,wrds[cnt2+1])) then
                        total = total+ wrds[cnt2+2];
                        end
                    cnt2=cnt2+1;
                    end
                end
            end
        if (stToken== nil) then bMore = false; end
        end
    return total;
    end


function DawnMod:CheckForSpam( stMsg, failScore)
    assert(type(stMsg)=="string");
    assert(type(failScore)=="number");
    if (DawnMod:CfgGetValue(DawnModConfigItem_KillSpamMod) and (DawnMod:CheckSpamWords( stMsg, DawnMod_Words.swMods,GtType_Alpha)>=failScore)) then
        return FailReason_Mod; end
    if (DawnMod:CfgGetValue(DawnModConfigItem_KillSpamGold) and (DawnMod:CheckSpamWords( stMsg, DawnMod_Words.swGold,GtType_Alpha)>=failScore)) then
        return FailReason_Gold; end
    if (DawnMod:CfgGetValue(DawnModConfigItem_KillSpamGuild) and ((DawnMod:CheckSpamWords( stMsg, DawnMod_Words.swGuild,GtType_Alpha)+DawnMod:CheckSpamWords(stMsg,DawnMod_Words.swGuildPunctuation,GtType_Punctuation))>=failScore)) then
        return FailReason_Guild; end
    if (DawnMod:CfgGetValue(DawnModConfigItem_KillSpamAnnoy) and (DawnMod:CheckSpamWords( stMsg, DawnMod_Words.swAnnoy,GtType_Alpha)>=failScore)) then
        return FailReason_Annoy; end
    if (DawnMod:CfgGetValue(DawnModConfigItem_KillSpamLink) and (DawnMod:CheckSpamWords( stMsg, DawnMod_Words.swLink,GtType_LinkText)>=failScore)) then
        return FailReason_Link; end
    return 0;
    end


function DawnMod:FixSpelling( stMsg)
    local   stOut, bMore, cnt, stToken,tokenType,stTokenUpr, cnt2, bFixed;
    local   stMsgUpr,pos, pos2, stPre,stPost, arrFixSpelling;

    assert(type(stMsg)=="string");
    bMore   = true;
    cnt = 0;
    stOut   = "";
    bFixed  = false;
    arrFixSpelling = DawnMod_Words.arrFixSpelling;

-- this handles the word replacements with no numbers in the source word.
-- ie.  The easy stuff.
    while (bMore) do
        stToken,tokenType,cnt = DawnMod:GetToken(stMsg, cnt);
        if (bMsgIsSystemText) then return stMsg, false; end
        if (stToken == nil) then
            bMore = false;
        else    if (tokenType==GtType_Alpha) then
                stTokenUpr = string.upper(stToken);
                cnt2=0;
                while (arrFixSpelling[cnt2+1]~=nil) do
                    if (StringMatch(stTokenUpr,arrFixSpelling[cnt2+1])) then
                        bFixed = true;
                        if (StringMatch(stToken,stTokenUpr)) then
                            stToken=arrFixSpelling[cnt2+2];
                        else    stToken=string.lower(arrFixSpelling[cnt2+2]);
                            end
                        end
                    cnt2=cnt2+2;
                    end
                end
            stOut = stOut..stToken;
            end
        end  --if (bMore) do

-- This is the kludge which handles the replacements with numbers in the source
-- word (eg. "any1").  Doesn't work under certain circumstances (like directly
-- after a link) but it's about the best I can manage without a mountain of
-- code.
    cnt = 1;
    while (arrFixSpelling[cnt]~=nil) do
        stMsgUpr = string.upper(stOut);
        if (string.find(arrFixSpelling[cnt],"%d")~=nil)then
            pos = string.find(stMsgUpr,arrFixSpelling[cnt]);
            if ((pos~=nil) and (((pos==1)or (string.find(GetCh(stMsgUpr,pos-1),"[%a%d]")==nil) ) and
                ((pos+string.len(arrFixSpelling[cnt])==string.len(stMsgUpr)) or (string.find(GetCh(stMsgUpr,pos+string.len(arrFixSpelling[cnt])),"[%a%d]")==nil)))) then
                    bFixed=true;
                    stToken=arrFixSpelling[cnt+1];
                    if (string.sub(stOut,pos,pos+string.len(arrFixSpelling[cnt]))==arrFixSpelling[cnt]) then
                        stToken = string.lower(stToken);
                        end
                    stPre="";stPost="";
                    if (pos>1) then stPre=string.sub(stOut,1,pos-1);end
                    if (pos+string.len(arrFixSpelling[cnt])<string.len(stOut)) then
                        stPost=string.sub(stOut,pos+string.len(arrFixSpelling[cnt]));
                        end
                    stOut=stPre..stToken..stPost;
                    end
            end
        cnt = cnt+2;
        end

    return stOut, bFixed;
    end -- function


function DawnMod:IsAllowedCapsWord( stToken)
    local   index;

    assert(type(stToken)=="string");
    index=1;
    bFound = false;

    while (DawnMod_Words.arrAllowedCaps[index]~=nil) do
        if (StringMatch(stToken,DawnMod_Words.arrAllowedCaps[index])) then return true; end
        index=index+2;
        end
    return false;
    end


function DawnMod:CheckMixedWord( stToken, stUprToken)
    local   cnt;

    assert(type(stToken)=="string");
    assert(type(stUprToken)=="string");
    cnt=1;
    while (DawnMod_Words.arrMixed[cnt]~=nil) do
        if (StringMatch(stUprToken,string.upper(DawnMod_Words.arrMixed[cnt]))) then
            return DawnMod_Words.arrMixed[cnt];
            end
        cnt=cnt+2;
        end
    return stToken;
    end


function DawnMod:CheckForCapsSpam( stMsg)
    local   str, wordCount, capsCount, bMore, stToken, tokenType,wordIndex;

    assert(type(stMsg)=="string");
    str             = DawnMod:StripPadding(DawnMod:StripLinks( stMsg));
    wordCount       = 0;
    capsCount       = 0;
    wordIndex   = 0;
    bMore           = true;

    while (bMore) do
        stToken,tokenType,wordIndex = DawnMod:GetToken( stMsg, wordIndex);
        if (bMsgIsSystemText) then return 0;end
        if (stToken~=nil) then
            if ((tokenType~=GtType_Whitespace) and
                (tokenType~=GtType_Punctuation) ) then
                wordCount = wordCount+1;
                end
            if ((tokenType==GtType_Alpha) and
                (StringMatch(stToken,string.upper(stToken))) and
                (not DawnMod:IsAllowedCapsWord(stToken)) ) then
                capsCount = capsCount+1;
                end;
        else
            bMore=false;
            end
        end

--      PrintDbg(format("caps: %i of %i >%s<",capsCount,wordCount,string.sub(stMsg,1,100)));
    if (capsCount > floor(wordCount/3)+1) then return FailReason_Caps; end
    return 0;
    end


function DawnMod:CheckForAsciiSpam( stMsg, iProbable)
    local   bMore, cnt,stToken, tokenType, wordLen, iAscii, iNonAscii;

    assert(type(stMsg)=="string");
    assert(type(iProbable)=="number");

    if (string.len(stMsg)<3) then return 0;end

    cnt=0;
    iAscii = 0;
    iNonAscii = 0;
    bMore = true;
    while (bMore) do
        stToken, tokenType,cnt = DawnMod:GetToken( stMsg, cnt);
        if (bMsgIsSystemText) then return 0;end
        if (stToken==nil) then bMore=false;
        else    wordLen = DawnMod:GTTokenLength( stToken, tokenType);
            if ((tokenType==GtType_Punctuation) or
                (tokenType==GtType_Whitespace) or
                (tokenType==GtType_RaidMark) ) then
                iAscii = iAscii + wordLen;
            else    iNonAscii = iNonAscii + wordLen;
                end
            end
        end
    cnt = iProbable+1;
    if (iAscii>(iNonAscii*2)/cnt) then return FailReason_Ascii; end
    return 0;
    end



function DawnMod:CheckForForeignChars( stMsg)
    local   str, bMore, stToken, tokenType,wordIndex,
        charCount,badCharCount,wordLen;

    assert(type(stMsg)=="string");
    str             = DawnMod:StripPadding(DawnMod:StripLinks( stMsg));
    charCount       = 0;
    badCharCount    = 0;
    wordIndex   = 0;
    bMore           = true;

    while (bMore) do
        stToken,tokenType,wordIndex = DawnMod:GetToken( str, wordIndex);
        if (bMsgIsSystemText) then return 0;end
        if (stToken~=nil) then
            wordLen = DawnMod:GTTokenLength( stToken, tokenType);
            if ((tokenType==GtType_RaidMar) or
                (tokenType==GtType_Num)or
                (tokenType==GtType_Alpha)or
                (tokenType==GtType_Whitespace)or
                (tokenType==GtType_Punctuation) ) then
                charCount = charCount + wordLen;
                end
            if (tokenType==GtType_UTF) then
                badCharCount=badCharCount+wordLen;
                end
        else
            bMore=false;
            end
        end

--PrintDbg(format("foreign lang check> bad: %d  total: %d",badCharCount,charCount+badCharCount));
    charCount = charCount + badCharCount;
    wordLen = floor(charCount/20);
    if (wordLen==0) then wordLen=1; end
    if ((badCharCount> wordLen) and (charCount>12) ) then
        return FailReason_ForeignLanguage; end
    return 0;
    end


function DawnMod:CheckForGlobalChannelGoodWords( stMsg)
    local   bMore, stToken,tokenType, goodWordCount,bDone,arrpos,stUprToken;

    assertstr(stMsg);
    cnt=0;
    goodWordCount=0;
    bMore = true;

    while (bMore) do
        stToken,tokenType,cnt=DawnMod:GetToken(stMsg,cnt);
        if (bMsgIsSystemText) then return 0;end
        if (stToken==nil) then bMore=false;
        else    if(tokenType==GtType_Alpha) then
                arrpos=1;
                bDone=false;
                stUprToken=string.upper(stToken);
                while (not bDone) do
                    if (DawnMod_Words.arrGlobalChannelGoodWords[arrpos]==nil) then
                        bDone=true;
                    else    if (StringMatch(stUprToken,DawnMod_Words.arrGlobalChannelGoodWords[arrpos])) then
                            bDone=true;
                            goodWordCount=goodWordCount+1;
                            end
                        end
                    arrpos=arrpos+1;
                    end
                end
            end
        end
    return goodWordCount;
    end


-- Special case code to capitalize the "L","F" and "M" as found in
-- "LF2M" or "LF23M"

function DawnMod:FixLFNumM( stIn)
    local str,stCaps,rpos,lpos,strlen;

    assert(type(stIn)=="string");
    str = stIn;
    stCaps = string.upper(stIn);
    rpos = 4;
    strlen = string.len(str);
    while (rpos<=strlen) do
        lpos = 0;
        rpos = string.find(stCaps,"M",rpos);
        if (rpos==nil) then return str; end
        if ((GetCh(stCaps,rpos-3)=="L") and (GetCh(stCaps,rpos-2)=="F") and ChIsNum(GetCh(stCaps,rpos-1))) then
            lpos = rpos-3;
            end
        if ((GetCh(stCaps,rpos-4)=="L") and (GetCh(stCaps,rpos-3)=="F") and ChIsNum(GetCh(stCaps,rpos-2)) and ChIsNum(GetCh(stCaps,rpos-1))) then
            lpos = rpos-4;
            end
        if ((lpos>0) and ((lpos==1) or (GetCh(str,lpos-1)==" ")) and ((rpos==strlen) or (GetCh(str,rpos+1)==" ")) ) then
            str = string.sub(str,1,lpos-1)..string.upper(string.sub(str,lpos,rpos))..string.sub(str,rpos+1);
            rpos=rpos+4;
        else    rpos = rpos + 1;
            end
        end
    return str;
    end


function DawnMod:FixBadCaps( stMsg)
    local stIn, stOut, pos, bMore, stToken, stUprToken, bIsAllowed, tokenType;

    assert(type(stMsg)=="string");
    stIn        = DawnMod:StripPadding(stMsg);
    stOut       = "";
    pos     = 0;
    bMore       = true;

    while (bMore) do
        stToken,tokenType,pos = DawnMod:GetToken( stIn, pos);
        if (bMsgIsSystemText) then return stMsg;end
        if (stToken ~= nil) then
            if (tokenType == GtType_Alpha) then
                stUprToken = string.upper(stToken);
                bIsAllowed = DawnMod:IsAllowedCapsWord( stUprToken);

                if (bIsAllowed) then stToken = stUprToken;
                else    stToken = DawnMod:CheckMixedWord(stToken,stUprToken);
                    if (StringMatch(stToken,stUprToken)) then
                        stToken = string.lower(stToken);
                        end
                    end
                end  -- if (tokenType==GtType_Alpha)

            stOut = stOut..stToken;
        else    bMore = false;
            end -- if (stToken~=nil)
        end -- while (bMore) do

    stOut=DawnMod:FixLFNumM(stOut);
    return stOut;
    end



local function FilterByTime( curTime, stMsg)
    local   index;

    assert(type(curTime)=="number");
    assert(type(stMsg)=="string");
    index = dmMsgCache:Find( stMsg);
    if (index<0) then return false; end
    return (curTime-dmMsgCache:GetEntryTime(index) <=DawnMod:CfgGetValue(DawnModConfigItem_LockOutMsgTime));
    end


local function MessageTypeName( msgType)
    assert(type(msgType)=="number");
    return arrMsgTypeData[ (msgType-1)*MsgTypeDataPerLine+1 ];
    end


local function MessageTypeFailScore( msgType)
    assert(type(msgType)=="number");
    return arrMsgTypeData[ (msgType-1)*MsgTypeDataPerLine+2 ];
    end

local function MessageTypeKarmaCheck( msgType)
    assert(type(msgType)=="number");
    return arrMsgTypeData[ (msgType-1)*MsgTypeDataPerLine+3 ];
    end

local function MessageTypeAllowKarmaIgnore( msgType)
    assert(type(msgType)=="number");
    return arrMsgTypeData[ (msgType-1)*MsgTypeDataPerLine+4 ];
    end

local function MessageTypeKarmaGain( msgType)
    assert(type(msgType)=="number");
    return arrMsgTypeData[ ( msgType-1)*MsgTypeDataPerLine+5 ];
    end


local function TimeString( nTime)
    local   days,hours,mins,secs, str;

    assert(type(nTime)=="number");
    secs = math.floor(nTime%60);
    mins = math.floor(nTime/60);
    hours = math.floor(mins/60);
    mins = mins % 60;
    days = math.floor(hours/24);
    hours = hours %24;
    str = "";
    if (days>0) then
        str = str..tostring(days).." day";
        if (days>1) then str = str.."s";end
        end
    if (hours>0) then
        if (string.len(str)>0) then str = str..", ";end
        str = str..tostring(hours).." hour";
        if (hours>1) then str = str.."s";end
        end
    if (mins>0) then
        if (string.len(str)>0) then str = str..", ";end
        str = str..tostring(mins).." min";
        if (mins>1) then str = str.."s";end
        end
    if (secs>0) then
        if (string.len(str)>0) then str = str.." and ";end
        str = str..tostring(secs).." sec";
        if (secs>1) then str = str.."s";end
        end
    return str;
    end


function DawnMod:ChatMsgCountString( nPassCount, nCount)
    local str;

    assert(type(nPassCount)=="number");
    assert(type(nCount)=="number");
    str = tostring(nCount).." message";
    if (nCount~=1) then str=str.."s"; end
    if (nCount>0) then
        str = str.." (";
        if (nPassCount>0) then
            str=str..tostring(nPassCount).." pass";
            end
        if ((nPassCount>0) and (nCount-nPassCount>0)) then
            str=str..":";
            end
        if (nCount-nPassCount>0) then
            str = str..format("%d fail %d%%",(nCount-nPassCount),(nCount-nPassCount)*100/nCount);
            end
        str = str..")";
        end
    return str;
    end


function DawnMod:ChatMsgCountIgnoresString( numIgnores)
    local   str;

    assert(type(numIgnores)=="number");
    str="";
    if (numIgnores>0) then
        str = " and "..tostring(numIgnores).." ignore";
        if (numIgnores > 1) then
            str=str.."s";
            end
        end
    return str;
    end


function DawnMod:ResetMsgPeriodData()
    msgCountData.msgPeriod = 0;
    msgCountData.msgPassPeriod = 0;
    msgCountData.timePeriod = GetTime();
    end


function DawnMod:ChatMsgCounter( curTime, bFailMsg, bIncCounters, bDisplay)

    assert(type(curTime)=="number");
    assert(type(bFailMsg)=="boolean");
    assert(type(bIncCounters)=="boolean");
    assert(type(bDisplay)=="boolean");
    if (bIncCounters) then
        msgCountData.msgTotal = msgCountData.msgTotal + 1;
        msgCountData.msgPeriod = msgCountData.msgPeriod + 1;
        if (not bFailMsg) then
            msgCountData.msgPassTotal = msgCountData.msgPassTotal + 1;
            msgCountData.msgPassPeriod = msgCountData.msgPassPeriod + 1;
            end
        end

    if (msgCountData.timeTotal == 0) then
        msgCountData.timeTotal = curTime;
        msgCountData.timePeriod = curTime;
        return
        end

    if (curTime - msgCountData.timePeriod > DawnMod_MsgTickPeriod) then
        if (bDisplay) then
            DawnMod:PrintHeader();
            PrintMsg("Period: "..
                DawnMod:ChatMsgCountString( msgCountData.msgPassPeriod, msgCountData.msgPeriod)..
                DawnMod:ChatMsgCountIgnoresString(msgCountData.ignoresPeriod)..
                " in "..TimeString(curTime-msgCountData.timePeriod));
            PrintMsg("Total: "..DawnMod:ChatMsgCountString( msgCountData.msgPassTotal, msgCountData.msgTotal)..
                DawnMod:ChatMsgCountIgnoresString(msgCountData.ignoresTotal)..
                " in "..TimeString(curTime-msgCountData.timeTotal));
            end
        msgCountData.msgPeriod = 0;
        msgCountData.msgPassPeriod = 0;
        msgCountData.ignoresPeriod = 0;
        msgCountData.timePeriod = curTime;

        if (curTime-msgCountData.timeTotal>DawnMod_MsgTickPeriod*2) then
            dmMsgCache:TuneLength( DawnMod, curTime, DawnMod:CfgGetValue(DawnModConfigItem_LockOutMsgTime),
                msgCountData.msgPeriod, curTime-msgCountData.timePeriod,
                msgCountData.msgTotal, curTime-msgCountData.timeTotal);
            end
        end
    end


local function GetChatMessageType( event, stChannelName, stMsg)
    local mt, uprChannelName;

    assert(type(event)=="string");
    assert(type(stChannelName)=="string");
    mt = MsgType_Unknown;
    if (event=="CHAT_MSG_BN_WHISPER") then mt = MsgType_Battlenet; end
    if (event=="CHAT_MSG_BN_CONVERSATION") then mt = MsgType_BattlenetConversation; end
    if (event=="CHAT_MSG_BN_WHISPER_INFORM") then mt = MsgType_BattlenetInform; end
    if (event=="CHAT_MSG_BATTLEGROUND") then mt = MsgType_Battleground; end
    if (event=="CHAT_MSG_BATTLEGROUND_LEADER") then mt = MsgType_Battleground; end
    if (event=="CHAT_MSG_CHANNEL") then
        uprChannelName = string.upper(tostring(stChannelName));
        if ( StringMatch(uprChannelName,"GENERAL") or
             StringMatch(uprChannelName,"GLOBAL") or
             StringMatch(uprChannelName,"TRADE - CITY") or
             (string.find(uprChannelName,"GENERAL - ")==1) or
             StringMatch(uprChannelName,"LOOKINGFORGROUP") or
             (string.find(uprChannelName,"LOCALDEFENSE - ")==1) or
             StringMatch(uprChannelName,"GUILD RECRUITMENT") ) then
            mt = MsgType_ChannelGlobal;
        else    mt = MsgType_ChannelPrivate;
            end

        if ((string.find(uprChannelName,"LOCALDEFENSE - ")==1) and
            (string.find(stMsg,"|r")~=nil)) then
            mt = MsgType_System;
            end
        end
    if (event=="CHAT_MSG_EMOTE") then mt = MsgType_EmotePlayer; end
    if (event=="CHAT_MSG_GUILD") then mt = MsgType_Guild;end
    if (event=="CHAT_MSG_OFFICER") then mt = MsgType_Guild;end
    if (event=="CHAT_MSG_PARTY") then mt = MsgType_Party;end
    if (event=="CHAT_MSG_PARTY_LEADER") then mt = MsgType_Party;end
    if (event=="CHAT_MSG_RAID") then mt = MsgType_Raid;end
    if (event=="CHAT_MSG_RAID_LEADER") then mt = MsgType_Raid;end
    if (event=="CHAT_MSG_RAID_WARNING") then mt = MsgType_Raid;end
    if (event=="CHAT_MSG_SAY") then mt = MsgType_Say;end
    if (event=="CHAT_MSG_SYSTEM") then mt = MsgType_System;end
    if (event=="CHAT_MSG_TEXT_EMOTE") then mt = MsgType_EmoteSystem;end
    if (event=="CHAT_MSG_WHISPER") then mt = MsgType_Whisper;end
    if (event=="CHAT_MSG_WHISPER_INFORM") then mt = MsgType_WhisperInform;end
    if (event=="CHAT_MSG_YELL") then mt = MsgType_Say;end

    if (((mt==MessageType_Unknown) or (mt==nil))and DawnMod:IsDebug()) then
        PrintDbg("* Unknown Message Type *\nEvent: "..tostring(event).."\nchannel = "..tostring(stChannelName).."\nstMsg: "..tostring(stMsg));
        bChatStop=1;
        end
    return mt;
    end


function DawnMod:IsInParty( stName)
    return (DawnMod:PlayerIsInParty() and
     ( (UnitInParty(DawnMod:GetShortName(stName))~=nil) or
       (UnitInParty(DawnMod:GetLongName(stName))~=nil)) );
    end


function DawnMod:IsInRaid( stName)
    return (DawnMod:PlayerIsInRaid() and
     ( (UnitInRaid(DawnMod:GetShortName(stName))~=nil) or
       (UnitInRaid(DawnMod:GetLongName(stName))~=nil)) );
    end


function DawnMod:AnnounceTrappedWhispers()
    local   stS;

    if (DawnMod:GetNumTrappedWhispers()~=1) then stS=""; else stS="s";end
    DawnMod:PrintHeader();
    DawnMod:PrintInfoItem("You have "..tostring(DawnMod:GetNumTrappedWhispers()).." whisper"..stS.." waiting.");
    DawnMod:PrintInfoItem("use: /DM TW to print one,");
    DawnMod:PrintInfoItem("or:  /DM TWS to print all.");

    stLastCombatWhisperer="";
    end


function DawnMod:CheckCombatWhispers( nMsgType, stSender)
    local   nReplyType, bAutoReply;

    assert(type(stSender)=="string");
    if (((DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersRaid) and DawnMod:PlayerIsInRaid() and (not DawnMod:IsInRaid(stSender)) ) or
         (DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersParty) and DawnMod:PlayerIsInParty() and (not DawnMod:IsInParty(stSender))) ) and
        ((DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersCombatOnly) and UnitAffectingCombat("PLAYER")) or
         (not DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersCombatOnly))) ) then

--      if (not StringMatch(stLastCombatWhisperer,stSender)) then
        bAutoReply = true;
        if (DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersReplyOnlyKnown)) then
            bAutoReply = DawnMod:CheckWhisperSource( stSender, true,true,true,true,true);
            end
        PrintDbg("bAutoReply = "..tostring(bAutoReply));
        if (bAutoReply) then
            bIgnoreOutgoing = true;
            if (nMsgType==MsgType_Whisper) then nReplyType = DawnMod_Reply_Normal; end
            if (nMsgType==MsgType_Battlenet) then nReplyType = DawnMod_Reply_Battlenet; end
            if (nMsgType==nil) then nReplyType = DawnMod_Reply_Normal;end

            dmReply:SendReply( nReplyType, stSender,
                "["..DawnMod_IdString.."]> "..UnitName("PLAYER").." is in "..GetRealZoneText().." and is busy.");
            stLastCombatWhisperer=stSender;
            end

        return true;
        end
    return false;
    end


function DawnMod:CheckTrappedCombatWhispers( event)
    DawnModMinimapButton:UpdateIcon();
    if (DawnMod:GetNumTrappedWhispers() == 0) then return end

    -- if BlockCombatOnly and ((BlockParty and PlayerIsInParty) or (BlockRaid and PlayerIsInRaid)))
    if ( ((event=="PLAYER_REGEN_ENABLED")and DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersCombatOnly)) and
         ((DawnMod:PlayerIsInParty()and DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersParty) ) or
          (DawnMod:PlayerIsInRaid() and DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersRaid)))) then
        DawnMod:AnnounceTrappedWhispers();
        end

    -- if we've just left party and we're blocking whispers in party (and not just in combat)
    if ( (event=="GROUP_ROSTER_CHANGED") and
         (not DawnMod:PlayerIsInParty()) and
         DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersParty) and
         (not DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersCombatOnly))) then
        DawnMod:AnnounceTrappedWhispers();
        return
        end

    -- if we've just left a raid and we're blocking whispers in raid (and not just in combat)
    if ( (event=="GROUP_ROSTER_CHANGED") and
         (not DawnMod:PlayerIsInRaid()) and
         DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersRaid) and
         (not DawnMod:CfgGetValue(DawnModConfigItem_TrapWhispersCombatOnly))) then
        DawnMod:AnnounceTrappedWhispers();
        return
        end
    end


function DawnMod:TrapWhisper( nMessageType, stSender, senderChatNo, stMsg)
    local   stCol, stSenderString;

    assertnum(nMessageType);
    assertstr(stSender);
    assertnum(senderChatNo);
    assertstr(stMsg);

    stCol = format("|cFF%.2X%.2X%.2X", 200, 250, 200);
    stSenderString=DawnMod:ShowReal(stSender).." : "..tostring(nMessageType);

    if (nMessageType==MsgType_Whisper) then
        stCol = DawnMod:MakeChatColour("WHISPER");
        stSenderString = "|Hplayer:"..stSender..":"..senderChatNo.."|h"..stSender.."|h";
        end
    if (nMessageType==MsgType_Battlenet) then
        stCol = DawnMod:MakeChatColour("BN_WHISPER");
        stSenderString = "|Hplayer:"..stSender..":"..senderChatNo.."|h"..stSender.."|h";
--      stSenderString = stSender;
        end
    table.insert(combatWhispers, stCol..date("%H:%M").." : ["..stSenderString.."] : "..stMsg.."|r");
    numCombatWhispers = numCombatWhispers+1;
    DawnModMinimapButton:UpdateIcon();
    end


function DawnMod:PrintCombatWhispers( bAll)
    local   bNonePrinted, stWord, stNum;

    bNonePrinted = true;
    DawnMod:PrintHeader();
    if (numCombatWhispers==0) then
        DawnMod:PrintInfoItem("There are no whispers to retrieve.");
        return
        end
    while (combatWhispers[1] and (bAll or bNonePrinted)) do
        PrintMsg( table.remove(combatWhispers,1));
        numCombatWhispers = numCombatWhispers-1;
        bNonePrinted=false;
        end
    if (not bAll) then
        if (numCombatWhispers==1) then stWord = "is"; else stWord = "are";end
        if (numCombatWhispers==0) then stNum = "none"; else stNum = tostring(numCombatWhispers);end
        DawnMod:PrintInfoItem("There "..stWord.." "..stNum.." left.");
        end
    DawnModMinimapButton:UpdateIcon();
    end


function DawnMod:CheckEmptyMessage( str)
    local   cnt, stToken, tokenType, bMore;

    assert(type(str)=="string");
    bMore = true;
    cnt = 0;
    while (bMore) do
        stToken,tokenType,cnt = DawnMod:GetToken( str, cnt);
        if (tokenType==nil) then
            bMore=false;
        else    if (tokenType~=GtType_Whitespace) then return 0; end
            end
        end
    return FailReason_EmptyString;
    end


function DawnMod:SwearFilterFixWord( stWord)
    local   src, srcWordIndex, stSrcWord, stSuffix, stPrefix,
        foundIndex, lenWord, lenSrc, bReplace, bReplacedWord,
        stWordOut, numDestWords, tmp,bWildPref,bWildSuf, bKeepGoing;

    src     = 1;
    stWordOut   = string.upper(stWord);
    lenWord     = string.len(stWordOut);
    bReplacedWord   = false;
    bKeepGoing  = true;
    while ((DawnMod_Words.arrSevenWords[src]~=nil) and bKeepGoing) do
        srcWordIndex = 1;
        while (DawnMod_Words.arrSevenWords[src][srcWordIndex] ~=nil) do
            stSrcWord = DawnMod_Words.arrSevenWords[src][srcWordIndex];
            bReplace=false;
            stPrefix="";
            stSuffix="";
            bWildPref = false;
            bWildSuf = false;
            if (string.find(stSrcWord,"*")==1) then
                bWildPref = true;
                stSrcWord = string.sub(stSrcWord,2);
                end
            if ((not bWildPref) and string.find(stSrcWord,"*")) then
                bWildSuf = true;
                stSrcWord = string.sub(stSrcWord,1, string.len(stSrcWord)-1);
                end
            lenSrc = string.len(stSrcWord);
            foundIndex=string.find(stWordOut,stSrcWord);
            if ((foundIndex==1) and (lenWord == lenSrc)) then
                bReplace=true;
                end
            if (foundIndex~=nil) then
                if (bWildPref and (foundIndex+lenSrc-1 == lenWord)) then
                    stPrefix = string.sub( stWordOut, 1, foundIndex-1);
                    bReplace = true;
                    end
                if (bWildSuf and (foundIndex==1) and (lenSrc<lenWord)) then
                    stSuffix = string.sub( stWordOut, foundIndex + lenSrc);
                    bReplace = true;
                    end
                end
            if (bReplace) then
                numDestWords = 0;
                while (DawnMod_Words.arrSevenWords[src+1][numDestWords+1]~=nil) do
                    numDestWords = numDestWords+1;
                    end
                stWordOut = stPrefix..DawnMod_Words.arrSevenWords[src+1][math.random(numDestWords)]..stSuffix;
                lenWord = string.len( stWordOut);
                bReplacedWord = true;
                if ((not bWildPrefix) and (not bWildSuf)) then
                    bKeepGoing = false;
                    end
                end
            srcWordIndex = srcWordIndex+1;
            end
        src = src+2;
        end
    if (bReplacedWord) then
        if (stWord~=string.upper(stWord)) then
            stWordOut = string.lower(stWordOut);
            end
    else    stWordOut = stWord;
        end
    return stWordOut, bReplacedWord;
    end


function DawnMod:SwearFilter( stMsg)
    local   cnt,stToken,tokenType,bMore, stOut, bFixed, numFixed;

    assert(type(stMsg)=="string");
    bMore   = true;
    stOut   = "";
    cnt = 0;
    numFixed = 0;
    while (bMore) do
        stToken,tokenType,cnt=DawnMod:GetToken( stMsg, cnt);
        if (bMsgIsSystemText) then return stMsg, 0;end
        if (stToken~=nil) then
            if (tokenType==GtType_Alpha) then
                stToken,bWordFixed=DawnMod:SwearFilterFixWord(stToken);
                if (bWordFixed) then numFixed=numFixed+1; end
                end
            stOut=stOut..stToken;
        else    bMore = false;
            end
        end
    return stOut, numFixed;
    end


local function StripSingleUrl( stStr, posUrl)
    local   pos, stPre, stPost

    assert(type(stStr)=="string");
    assert(type(posUrl)=="number");
    pos = posUrl;
    while ((pos>0) and (GetCh(stStr,pos)~=" ")) do pos=pos-1;end
    if (pos==0) then
        stPre="";
    else    stPre = string.sub( stStr, 1, pos);
        end

    pos = string.find( string.sub(stStr, posUrl), " ");
    if (pos==nil) then
        stPost = "";
    else    stPost = string.sub( stStr, posUrl+pos-1);
        end

    return stPre..stPost;
    end


local function StripUrls( stMsg)
    local stOut, posFound, stUprOut, arrPos, bStripped;

    assert(type(stMsg)=="string");
    stOut   = stMsg;
    posFound = 1;
    bStripped = false;
    while (posFound~=nil) do
        stUprOut = string.upper( stOut);
        arrPos = 1;
        posFound = nil;
        while ((posFound==nil)and(DawnMod_Words.arrUrlParts[arrPos]~=nil)) do
            posFound = string.find(stUprOut,DawnMod_Words.arrUrlParts[arrPos]);
            arrPos = arrPos + 1;
            end
        if (posFound) then
            stOut = StripSingleUrl( stOut, posFound);
            bStripped = true;
            end
        end
    return stOut, bStripped;
    end


local function EndRecountTracking()
    recountTrackData.bTracking=false;
    return 0;
    end


function DawnMod:CheckForRecountOutput( stSenderName, stMsg, curTime)
    local   pos;

    assert(type(stSenderName)=="string");
    assert(type(stMsg)=="string");
    assert(type(curTime)=="number");
    if ( (string.find(string.upper(stMsg),"RECOUNT - ")==1) or
         (string.find(string.upper(stMsg),"SKADA:")==1)
         )then
        recountTrackData.bTracking = true;
        recountTrackData.stRecountSender = stSenderName;
        recountTrackData.time   = curTime;
        return FailReason_Recount;
        end

    if (recountTrackData.bTracking and (stSenderName==recountTrackData.stRecountSender)) then
        pos = string.find(stMsg,"%d");
        if (pos==nil) then return EndRecountTracking();end
        stMsg = string.sub( stMsg, pos);
        pos = string.find(stMsg,"%s");
        if (pos==nil) then return EndRecountTracking();end
--      PrintDbg("stMsg = >"..stMsg.."<  pos = "..tostring(pos));
--      PrintDbg(">".. string.sub(stMsg,1,pos-1)  .."<");
        if (not (string.find(string.sub(stMsg,1,pos-1),"%d+%.$")==1)) then
            return EndRecountTracking();
            end
        return FailReason_Recount;
        end

    return EndRecountTracking();
    end


local function SetFailMsgColours( stMsg)
    local   cnt, bMore, stOut,stToken,tokenType, bPrevWasLink;

    assert(type(stMsg)=="string");
    bMore   = true;
    stOut   = DawnMod_FailMessageColour;
    cnt = 0;
    bPrevWasLink = true;
    while (bMore) do
        stToken,tokenType,cnt=DawnMod:GetToken( stMsg, cnt);
        if (stToken~=nil) then
            if (bPrevWasLink) then
                bPrevWasLink = false;
                stToken = stToken..DawnMod_FailMessageColour;
                end
            if (tokenType==GtType_LinkText) then
                stToken = DawnMod_FailLinkColour..
                      stToken;
                bPrevWasLink=true;
                end
            stOut=stOut..stToken;
        else    bMore = false;
            end
        end
    return stOut;
    end



local function Plonk( stName)
    local   numIgnores, index, stNameUpr;

    assert(type(stName)=="string");
    numIgnores = GetNumIgnores();
    stNameUpr = string.upper(stName);

    index = 1;
    while (index<=numIgnores) do
        if (string.upper(GetIgnoreName(index))==stNameUpr) then
            return
            end
        index=index+1;
        end

    AddIgnore(stName);
    DawnMod:PrintHeader();
    DawnMod:PrintInfoItem("Now ignoring: "..stName);
    msgCountData.ignoresPeriod = msgCountData.ignoresPeriod + 1;
    msgCountData.ignoresTotal = msgCountData.ignoresTotal + 1;
    end


function DawnMod:KarmaFilter( failReason, stSender, msgType, bonusKarma, curTime)
    local   karmaChange, tmp, stFullSender;

    karmaChange = 0;
    stFullSender = DawnMod:GetLongName(stSender);

    if (failReason>0) then
        tmp,karmaChange = DawnMod:GetFailReasonData( failReason);
    else    karmaChange = MessageTypeKarmaGain( msgType)+bonusKarma;    -- only get bonus karma if the message isn't being failed.
        end
--  PrintDbg("Karma Change = "..tostring(karmaChange).."  (bonus: "..tostring(bonusKarma)..")");
    tmp=dmPosterCache:ChangeScoreByName(stFullSender,karmaChange, curTime);
    if ((tmp==Karma_ScoreMin) and DawnMod:CfgGetValue(DawnModConfigItem_KarmaPlonk) and
        MessageTypeAllowKarmaIgnore( msgType)) then
        Plonk(stFullSender);
        end
    if (tmp<Karma_ScoreBlockBelow) then
        return FailReason_Karma;
        end
    return 0;
    end


local function CheckRepeatPoster( stSender, curTime)
    local   index, stLongName, lastSeen;

    stLongName = DawnMod:GetLongName( stSender);
    index = dmPosterCache:FindName( stLongName);
    if (index<0) then dmPosterCache:AddEntry( stLongName, Karma_ScoreDef, curTime); return 0; end
    lastSeen = dmPosterCache:GetLastSeenByIndex( index);
    if (lastSeen>=0) then
        if (curTime-lastSeen <DawnMod:CfgGetValue(DawnModConfigItem_LockOutPosterTime)) then
            return FailReason_RepeatPoster;
            end
        end
    dmPosterCache:SetLastSeenByIndex( index, curTime);
    return 0;
    end


function DawnMod:CheckWhisperSource( stName, bAllowAnswer, bAllowFriends, bAllowGuild, bAllowParty, bAllowRaid)
    local   bFriend, bGuild, bParty, bRaid, bAnswer;

    bAnswer, bFriend, bGuild, bParty, bRaid =
        dmKnown:IsKnown( stName);

    return ( (bAllowAnswer and bAnswer) or
         (bAllowFriends and bFriend) or
         (bAllowGuild and bGuild) or
         (bAllowParty and bParty) or
         (bAllowRaid and bRaid) );
    end




local function SetFailReason( newFr)
    local   newFailLevel;
    assert(type(newFr)=="number");
    assert(type(failReason)=="number");
    newFailLevel = 0;
    if (newFr~=0) then
        _,_,newFailLevel = DawnMod:GetFailReasonData( newFr);
        end
    if ((failReason==0)and(newFr~=0)and(newFailLevel>failLevel)) then
        failReason = newFr;
        failLevel = newFailLevel;
        end
    end


local function CheckNoFail() return (failLevel<FailLevel_Fatal) or bDebugOrTest; end


function DawnMod:MessageHandler( event,farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...)
    local   str, stOut, curTime, stChannel, stRem, stSenderFlags, stSender,
        bCountMessage,stRemTest,tempVar, bStripped, msgType;

    if ((not DawnMod:CfgGetValue(DawnModConfigItem_Enabled)) or
        ((type(farg6)=="string") and StringMatch(farg6,"GM"))) then -- message is from a GM
        return false, farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
        end

    if (bChatStop) then
        return true, farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
        end

    if ((farg11 == prevMsgData.msgNo) and (prevMsgData.msgNo~=0)) then
--      PrintMsg("msgno = "..tostring(farg11));
        return (prevMsgData.failLevel==FailLevel_Fatal), prevMsgData.stOut, farg2, farg3, farg4, farg5, prevMsgData.stSenderFlags, farg7, farg8, farg9, farg10, farg11, farg12,...
        end

    if (type(farg9)=="string") then stChannel = string.upper(farg9); else stChannel = ""; end
    stOut           = DawnMod:StripPadding(farg1);
    msgType     = GetChatMessageType( event, stChannel, farg1);
    stSender    = string.upper(DawnMod:GetLongName(tostring(farg2)));
    curTime         = GetTime();
    stSenderFlags   = tostring(farg6);
    failReason  = 0;
    failLevel   = 0;
    bCountMessage   = true;
    bStripped   = false;
    bDebugOrTest    = (DawnMod:IsDebug() or DawnMod:CfgGetValue(DawnModConfigItem_TestMode));

    -- Skip messages from Carbonite mod (shouldn't be getting them in the
    -- channels which we're filtering but sometimes do anyhows)
    if (string.find(stChannel,"CRB")==4) then
        return false, farg1, farg2, farg3, farg4, farg5, farg6, farg7, farg8, farg9, farg10, farg11, farg12,...
        end

    -- Unless we're debugging.. dont flag the user's stuff.. or whitelisted people
    if ( ((not DawnMod:IsDebug()) and StringMatch(farg2,UnitName("PLAYER"))) or
         DawnModWhitelist:IsWhitelisted( stSender) ) then
            if (not StringMatch(farg2,UnitName("PLAYER"))) then
                if (bDebugOrTest) then
                    stOut="(WL) "..stOut;
                    end
                end
        return false, stOut, farg2, farg3, farg4, farg5, stSenderFlags, farg7, farg8, farg9, farg10, farg11, farg12,...
        end

-- Strip player flags
    if (DawnMod:CfgGetValue(DawnModConfigItem_StripPlayerFlags)) then
        stSenderFlags = "";
        end

-- Block whispers
    if (CheckNoFail() and (DawnMod:CfgGetValue(DawnModConfigItem_BlockWhispers) and (msgType==MsgType_Whisper) and (not StringMatch(UnitName("PLAYER"),farg2)))) then
        if (not DawnMod:CheckWhisperSource( stSender,
                DawnMod:CfgGetValue(DawnModConfigItem_BlockWhispersExceptAnswers),
                DawnMod:CfgGetValue(DawnModConfigItem_BlockWhispersExceptFriends),
                DawnMod:CfgGetValue(DawnModConfigItem_BlockWhispersExceptGuild),
                DawnMod:CfgGetValue(DawnModConfigItem_BlockWhispersExceptParty),
                DawnMod:CfgGetValue(DawnModConfigItem_BlockWhispersExceptRaid)
                )) then
            SetFailReason( FailReason_UnknownSender);
            end
        end

    stRemTest = "";
    if (CheckNoFail() and
         ((DawnMod:CfgGetValue(DawnModConfigItem_LockOutMsgTime)>0) or DawnMod:CfgGetValue(DawnModConfigItem_BlockGlobalChannelMultiline))) then

-- Block global channel multiline
        if (DawnMod:CfgGetValue(DawnModConfigItem_BlockGlobalChannelMultiline) and (msgType==MsgType_ChannelGlobal)) then
            _,tempVar = dmMsgCache:FindMostRecentMsgByName( stSender);
            if ((tempVar~=nil)and( (curTime-tempVar) < DawnMod_MultilineMinTime)) then
                SetFailReason( FailReason_Multiline);
                end
            end

-- Rem buf
        stRem = stSender..": "..farg1;
        if (msgType==MsgType_EmoteSystem) then
            stRem = stSender..":>".."EMOTE";
            end
        if (msgType==MsgType_System) then
            if (string.find(string.upper(farg1),"IS UNDER ATTACK")~=nil) then
                stRem = stSender..":#SYSTEM";
            else    stRem = "";
                end
            end
        if (DawnMod:IsDebug()) then
            tempVar = dmMsgCache:Find( stRem);
            if (tempVar>=0) then
                local tmp=dmMsgCache:GetEntryTime(tempVar);
                if (type(tmp)~="number") then
                    PrintDbg("tmp is not a number!! (type(tmp) = "..type(tmp)..")");
                    end
                if (tmp>curTime) then
                    PrintDbg("tmp > curTime!!  (tmp = "..tostring(tmp)..")");
                    end

                tempVar=math.ceil(curTime-dmMsgCache:GetEntryTime(tempVar));
                if (tempVar>1000) then tempVar="*";else tempVar = tostring(tempVar);end
                stRemTest = "(m"..tempVar;
            else    stRemTest = "(m-";
                end
            if (msgType==MsgType_System) then
                stRemTest = stRemTest..")";
            else    stRemTest = stRemTest..".";
                end
            end
        if (stRem~="") then
            if ( (msgType==MsgType_ChannelGlobal)or(event=="CHAT_MSG_YELL") or (not DawnMod:CfgGetValue(DawnModConfigItem_LockOutMsgGlobalOnly))) then
                if (FilterByTime(curTime,stRem)) then
                    if (DawnMod:CfgGetValue(DawnModConfigItem_LockOutMsgTime)>0) then
                        SetFailReason( FailReason_RepeatMsg);
                        end
                else    dmMsgCache:Update( curTime, stRem);
                    end
                end
            end

        end -- if (UseRemBuf or BlockGCMultiline)

-- repeat poster
    if (msgType~=MsgType_System) then
        if (bDebugOrTest) then
            tempVar = dmPosterCache:GetLastSeenByName(stSender);
            if (tempVar<0) then
                stRemTest=stRemTest.."p-)";
            else    tempVar = math.ceil(curTime-tempVar);
                if (tempVar>1000) then tempVar="*";else tempVar = tostring(tempVar);end
                stRemTest=stRemTest.."p"..tempVar..")";
                end
            end

        if ( CheckNoFail() and (DawnMod:CfgGetValue(DawnModConfigItem_LockOutPosterTime)>0) and
            ((msgType==MsgType_ChannelGlobal) or(msgType==MsgType_Say) or (not DawnMod:CfgGetValue(DawnModConfigItem_LockOutPosterGlobalOnly))) ) then
            SetFailReason( CheckRepeatPoster( stSender, curTime));
        else    dmPosterCache:SetLastSeenByName( stSender,curTime);
            end
        end

-- Recount
    if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_BlockRecount)) then
        if (not((DawnMod:CfgGetValue(DawnModConfigItem_BlockRecountExceptParty) and (UnitInParty(farg2)~=nil)) or
            (DawnMod:CfgGetValue(DawnModConfigItem_BlockRecountExceptRaid)  and (UnitInRaid(farg2)~=nil)))) then
            SetFailReason( DawnMod:CheckForRecountOutput( stSender, farg1, curTime));
            end
        end

-- Raid zone general
    if (CheckNoFail() and (DawnMod:CfgGetValue(DawnModConfigItem_BlockRaidZoneGeneral) and
        (msgType==MsgType_ChannelGlobal) and
        (string.find(string.upper(tostring(farg9)),"GENERAL - ")==1) and
        DawnMod:PlayerIsInRaid() and DawnMod:PlayerIsInRaidZone())) then
        SetFailReason( FailReason_RaidZoneGeneral);
        end

-- Party zone general
    if (CheckNoFail() and (DawnMod:CfgGetValue(DawnModConfigItem_BlockPartyZoneGeneral) and
        (msgType==MsgType_ChannelGlobal) and
        (string.find(string.upper(tostring(farg9)),"GENERAL - ")==1) and
        DawnMod:PlayerIsInParty() and DawnMod:PlayerIsInPartyZone())) then
        SetFailReason( FailReason_PartyZoneGeneral);
        end

    if ((msgType == MsgType_ChannelGlobal)or(event=="CHAT_MSG_YELL")) then

-- Foreign Language
    if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_BlockGlobalChannelForeign) and (msgType~=MsgType_System)) then
        SetFailReason( DawnMod:CheckForForeignChars( stOut));
        end

-- Block global channel ASCII
        if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_BlockGlobalChannelAscii) and (msgType~=MsgType_System)) then
            tempVar = 0;
            if (stSender==prevMsgData.stSender) then
                tempVar = 1;
                if (prevMsgData.failReason == FailReason_Ascii) then
                    tempVar = 2;
                    end
                end
            SetFailReason( DawnMod:CheckForAsciiSpam( stOut, tempVar));
            end

-- Strip global channel raid marks
        if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_StripGlobalChannelRaidMarks) and (msgType~=MsgType_System)) then
            stOut, tempVar = DawnMod:StripRaidMarks( stOut);
            if (tempVar) then bStripped = true; end
            end

-- Strip global channel URLs
        if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_StripGlobalChannelUrls) and (msgType~=MsgType_System)) then
            stOut, tmpVar = StripUrls( stOut);
            if (tmpVar) then bStripped = true; end
            end

        end -- (msgType == MsgType_ChannelGlobal)

-- Fix spelling
    if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_FixSpelling) and (msgType~=MsgType_System) and (msgType~=MsgType_EmoteSystem)) then
        stOut,tempVar = DawnMod:FixSpelling(stOut);
        stOut = string.sub(stOut,1,255);
        end

-- Check for spam
    if (CheckNoFail() and (msgType~=MsgType_System)) then
        SetFailReason( DawnMod:CheckForSpam( stOut,MessageTypeFailScore(msgType)));
        end

-- Fix bad caps
    if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_FixCaps) and (msgType~=MsgType_System) and (msgType~=MsgType_EmoteSystem)) then
        stOut = DawnMod:FixBadCaps( stOut);
    else    if (CheckNoFail() and (not DawnMod:CfgGetValue(DawnModConfigItem_AllowCaps))) then
            SetFailReason( DawnMod:CheckForCapsSpam( stOut));
            end
        end

-- Fix swearing
    if (CheckNoFail() and DawnMod:CfgGetValue(DawnModConfigItem_FixSwearing) and (msgType~=MsgType_System) and (msgType~=MsgType_EmoteSystem)) then
        stOut,tempVar = DawnMod:SwearFilter( stOut);
        if (DawnMod:IsDebug() and (tempVar>0)) then
            SetFailReason( FailReason_Swear);
            end
        end

    if ((msgType==MsgType_EmotePlayer)or(msgType==MsgType_EmoteSystem)) then
        stOut = string.upper(string.sub( stOut,1,1)) ..
            string.sub(stOut,2);
        end

-- Since we're removing stuff from the message, it's
-- possible that we've removed ALL of the message.
    if (CheckNoFail() and bStripped) then
        SetFailReason( DawnMod:CheckEmptyMessage( stOut));
        end

-- Check karma
    if (DawnMod:CfgGetValue(DawnModConfigItem_KarmaUse) and (stSender~="") and
        (not StringMatch(stSender,string.upper(DawnMod:GetLongName(UnitName("PLAYER"))))) and MessageTypeKarmaCheck(msgType) ) then
            tempVar=true;
        if (DawnMod:CfgGetValue(DawnModConfigItem_KarmaSuspendGroup) and (DawnMod:IsInParty(farg2) or DawnMod:IsInRaid(farg2)) ) then
            tempVar=false;end
        if (DawnMod:CfgGetValue(DawnModConfigItem_KarmaSuspendGuild) and (msgType==MsgType_Guild)) then
            tempVar=false;end
        tempVar2=0;
        if (msgType==MsgType_ChannelGlobal) then
            tempVar2=DawnMod:CheckForGlobalChannelGoodWords( stOut);
            if (tempVar2>0) then tempVar2=1; end
            end
        if (tempVar) then
            SetFailReason( DawnMod:KarmaFilter( failReason, stSender, msgType, tempVar2, curTime));
            end
        end

-- combat whispers (must be last check done so we're only saving a message if it's already survived the other filters)
    if (CheckNoFail()and (((msgType==MsgType_Whisper)or(msgType==MsgType_Battlenet)) and DawnMod:CheckCombatWhispers(msgType, farg2))) then
        DawnMod:TrapWhisper( msgType, farg2, farg11, stOut);
        SetFailReason( FailReason_CombatWhisper);
        end

    DawnMod:ChatMsgCounter( curTime, (failReason>0), bCountMessage, DawnMod.bTickMessage);

    if (DawnMod:IsDebug()) then
        assertnum(failReason);
        assertstr(farg1);
        assertstr(stChannel);
        assertnum(msgType);
        assertnum(curTime);
        assertstr(stOut);
        assertstr(stSender);
        assertstr(stSenderFlags);
        end

    prevMsgData.failReason  = failReason;
    prevMsgData.failLevel   = failLevel;
    prevMsgData.stMsg   = farg1;
    prevMsgData.stChannel   = stChannel;
    prevMsgData.msgType = msgType;
    prevMsgData.curTime = curTime;
    prevMsgData.msgNo   = farg11;
    prevMsgData.stOut   = stOut;
    prevMsgData.stSender    = stSender;
    prevMsgData.stSenderFlags = stSenderFlags;

    if (bDebugOrTest) then
        str="";
--      if (DawnMod:IsDebug()and(failLevel==FailLevel_Fatal)) then PrintDbg(farg2..": "..string.sub(farg1,1,100)); end
--      if (DawnMod:IsDebug()) then str = str.."["..MessageTypeName(msgType).."] ";end      -- message type
--      if (DawnMod:IsDebug()) then str = str.."( |Hplayer:"..farg2..":"..tostring(farg11).."|h"..farg2.."|h )";end
--      if (DawnMod:IsDebug()) then str = str.."<"..tostring(farg11).."> ";end          -- messageNo
        if (DawnMod:IsDebug()) then
            if (DawnMod:CfgGetValue(DawnModConfigItem_KarmaUse)) then
                str=str.."["..tostring(dmPosterCache:GetScore(DawnMod:GetLongName(stSender))-Karma_ScoreDef).."] ";
            else    str=str.."[] ";end
            if ((DawnMod:CfgGetValue(DawnModConfigItem_LockOutMsgTime)>0) or (DawnMod:CfgGetValue(DawnModConfigItem_LockOutPosterTime)>0))then
                str = str..stRemTest.." ";
            else    str = str.."- ";end
            end
        if (failReason>0) then
            tempVar=DawnMod:GetFailReasonData(failReason);
            str = str.."("..tempVar..")";
            end
        if (str~="") then str=str..": ";end
        if (failLevel>=FailLevel_Fatal) then
            stOut=SetFailMsgColours( stOut);
            end

        stOut = str..string.sub(stOut,1,255-string.len(str));
        prevMsgData.failReason  = 0;
        prevMsgData.failLevel   = 0;
        prevMsgData.stOut   = stOut;
        return false,stOut,farg2, farg3, farg4, farg5, stSenderFlags, farg7, farg8, farg9, farg10, farg11, farg12,...
        end
    return (failLevel==FailLevel_Fatal),stOut,farg2, farg3, farg4, farg5, stSenderFlags, farg7, farg8, farg9, farg10, farg11, farg12,...
end


function DawnMod:MessageHandlerInform( event,farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...)
    local   informMsgType;

    if (not DawnMod:CfgGetValue(DawnModConfigItem_Enabled)) then
        return false, farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
        end

    if (bChatStop) then
        return true, farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
        end

    if (farg11 == prevMsgData.informMsgNo) then
        return (prevMsgData.informFailReason>0), farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
        end

    informMsgType   = GetChatMessageType( event, farg9, farg1);
    stSender    = string.upper(DawnMod:GetLongName(tostring(farg2)));
    failReason  = 0;

    if (((informMsgType==MsgType_WhisperInform)or(informMsgType==MsgType_BattlenetInform)) and bIgnoreOutgoing) then
        if (farg11~=prevMsgData.informMsgNo) then PrintDbg("blocked whisper inform: "..farg1);end
        bIgnoreOutgoing = false;
        failReason=FailReason_DawnModOutgoing;
        end

    if ((failReason==0) and ((informMsgType==MsgType_WhisperInform)or(informMsgType==MsgType_BattlenetInform))) then
        dmKnown:AddMisc(DawnMod:GetLongName(farg5));
        end

    prevMsgData.informMsgNo = farg11;
    prevMsgData.informFailReason = failReason;

    return (failReason>0), farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
    end


function DawnMod:MessageHandlerWhisper( event,farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...)
    local   stSender, stMsgUpr, stWrd, cfgNo, bInvite, stType, fNotify,tmp;

    if (not DawnMod:CfgGetValue(DawnModConfigItem_Enabled)) then
        return false, farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
        end

    stMsgUpr = string.upper(farg1);
    stWrd = string.upper(DawnMod:CfgGetValue(DawnModConfigItem_InviteWord));
    if ((string.len(stWrd)>0) and (string.find(stMsgUpr,stWrd)~=nil)) then
        stSender = string.upper(DawnMod:GetLongName(tostring(farg2)));
        cfgNo,stType= DawnMod:GetInviteStatus( stSender);

        tmp= DawnMod:CfgGetValue(cfgNo);
        bInvite = (bit.band( tmp,DawnModInviteFlag_InviteWord) > 0);
        fNotify = (bit.band( tmp,DawnModInviteFlag_DontNotify) == 0);
        if (bInvite) then
            if ( (DawnMod:GetPlayerInPveQueue() and DawnMod:CfgGetValue(DawnModConfigItem_InviteOverridePve)) or
                 (DawnMod:GetPlayerInPvpQueue() and DawnMod:CfgGetValue(DawnModConfigItem_InviteOverridePvp))) then
                DawnMod:PrintHeader();
                DawnMod:PrintInfoItem("","Not auto inviting ("..stType..") '"..farg2.."' because you are in a queue.");
            else    if (fNotify) then
                    DawnMod:PrintHeader();
                    DawnMod:PrintInfoItem("Inviting ("..stType..") '"..farg2.."'.");
                    end
                InviteUnit( farg2);
                return true,farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...
                end
            end
        end

    return DawnMod:MessageHandler(event,farg1,farg2,farg3,farg4,farg5,farg6,farg7,farg8,farg9,farg10,farg11,farg12,...);
    end



function DawnMod:FixFastTrack( stMsgIn)
    local   pos, endPos, stMsg, stDeposited;

    stMsg = stMsgIn;
    stDeposited =  "deposited to guild bank%)";
    pos = string.find( stMsg, stDeposited);
    if (pos~=nil) then
        endPos = pos + string.len(stDeposited);
        pos = 0;
        while (string.find( string.sub( stMsg, pos+1,endPos), "%(") ) do
            pos = string.find( string.sub( stMsg, pos+1,endPos), "%(");
            end
        stMsg = string.sub(stMsg,1,pos-1)..string.sub(stMsg,endPos+1);
        stMsg = DawnMod:StripPadding(stMsg);
        end
    return stMsg
    end


function DawnMod:CheckFilterGroupZone( stGroup, stZone)
    local   pos1,pos2, st;

    pos1=string.find(stGroup,"<");
    pos2=string.find(stGroup,">");
    if ((not pos1) or (not pos2) or ((pos1+1)>(pos2-1))) then
        PrintDbg("stGroup = "..stGroup);
        PrintDbg("stZone = "..stZone);
        return false;
        end
    return (string.upper(string.sub(stGroup,pos1+1,pos2-1))==string.upper(stZone));
    end


function DawnMod:CategoryMessageFilter( stCat, stMessageIn, stZone)
    local   stMsg, stGroup, groupNum, bZoneOk;

    assertstr(stCat);
    assertstr(stMessageIn);
    stMsg = string.upper(stMessageIn);
    stGroup =" ";
    groupNum=1;
    while (stGroup~="") do
        stGroup=DawnModFilters:GetGroupInCategoryByIndex(stCat, groupNum);
--        PrintDbg("["..stCat.."]["..stGroup.."] "..( (stGroup~="") and tostring(DawnModFilters:GetOption(stGroup)) or "nope") );
        if ((stGroup~="") and DawnModFilters:GetOption( stGroup)) then
            bZoneOk=true;
            if ((stZone~=nil)and string.find(stGroup,"<")) then
                bZoneOk=DawnMod:CheckFilterGroupZone( stGroup, stZone);
                end
            if (bZoneOk and DawnModFilters:CheckMessageAgainstCategoryGroup(stMsg,stCat,stGroup)) then
                return true;
                end
            end
        groupNum=groupNum+1;
        end
    return false;
    end


function DawnMod:FilterByOption( stMsg, stOption)
    local   nIndex=1, stFilt, stMsgUpr;
    assertstr(stOption);
    assertstr(stMsg);

--  PrintDbg("stOption: >"..stOption.."<  stMsg >"..stMsg.."<");
    if (DawnModFilters:GetOption(stOption)) then
        stMsgUpr = string.upper(stMsg);
        stFilt=" ";
        while (stFilt~="") do
            stFilt = DawnModFilters:FindFilterByGroup( stOption, nIndex);
            if (stFilt~="") then
                if (string.find(stMsgUpr,string.upper(stFilt))) then
                    return true;
                    end
            else    if (nIndex==1) then return true; end
                end
            nIndex=nIndex+1;
            end
        end
    return false;
    end


function DawnMod:MessageHandlerGameMessages( e,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,...)
    local   stOut, bBlock, pzt, filtType, posYou,stPerson, posYouCheck,pos,
        tmp, zoneText, stExtra;

    stOut = a1;
    bBlock = false;
    stExtra = "";

    if (not DawnMod:CfgGetValue(DawnModConfigItem_Enabled)) then
        return false,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,...
        end

    if ((farg11 == prevMsgData.msgNo) and (prevMsgData.msgNo~=0)) then
        return (prevMsgData.failLevel>0), prevMsgData.stOut, a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,...
        end

--  PrintDbg("[npc] "..a2..": "..a1);
    if  ((e=="CHAT_MSG_MONSTER_YELL") or
         (e=="CHAT_MSG_MONSTER_SAY") or
         (e=="CHAT_MSG_MONSTER_WHISPER") or
         (e=="CHAT_MSG_MONSTER_EMOTE") ) then
        if (DawnMod:CategoryMessageFilter("Npc Messages", a1) or
            DawnMod:CategoryMessageFilter("Npc Names", a2,GetRealZoneText()) ) then
            bBlock=true;
            end
        if (DawnMod:IsDebug() and (not bBlock)) then
            local   st,cnt;
            st=tostring(GetRealZoneText()).." : "..tostring(a2).." : >"..tostring(a1).."<";
            cnt=1;
            while((st~="") and DawnMod.arrNpcChatter[cnt]) do
                if (string.upper(DawnMod.arrNpcChatter[cnt])==string.upper(st)) then
                    st="";
                    end
                cnt=cnt+1;
                end

            if (st~="") then table.insert( DawnMod.arrNpcChatter, st);end
            end
        end

    if (not bBlock) then
        _,pzt=GetInstanceInfo();
        pzt=string.upper(pzt);

        filtType="";
        if ((pzt=="PVP")or(pzt=="ARENA")) then filtType="PvP"; end
        if ((pzt=="RAID")or(pzt=="PARTY")) then filtType="PvE"; end
        if (pzt=="NONE") then filtType="World"; end
        posYou = string.find(string.upper(a1),"^YOU");
        if (posYou==1) then
            posYouCheck=1;
            stPerson="self";
        else
            posYouCheck=nil;
            stPerson="other";
            end
        end

    if (e=="CHAT_MSG_ACHIEVEMENT") then
        if (DawnModFilters:GetOption("["..filtType.."] Achievment announcements")) then
            bBlock=true;
            end
        end

    if (e=="CHAT_MSG_SYSTEM") then
        if (DawnMod:CategoryMessageFilter( "System Messages",a1)) then
            bBlock = true;
            end
        if (((filtType=="PvP")or(DawnMod.lastPvpMessageTime and (GetTime()-DawnMod.lastPvpMessageTime<30))) and DawnMod:CategoryMessageFilter("[PvP] System Messages",a1)) then
            if ((pzt=="PVP")or(pzt=="ARENA")) then
                DawnMod.lastPvpMessageTime = GetTime();
                end
            bBlock=true;
            end
        if ((filtType=="PvE") and DawnMod:CategoryMessageFilter("[PvE] System Messages",a1)) then
            bBlock=true;
            end
        end

    if (e=="CHAT_MSG_CURRENCY") then
        if (string.find(string.upper(a1),"YOU RECIEVE CUR")) then
            PrintDbg("Currency >"..a1.."<");
            end
        if (DawnMod:CategoryMessageFilter( "Currency", a1)) then
            bBlock=true;
            end
        end

    if (e=="CHAT_MSG_LOOT") then
--      PrintDbg("filt = "..filtType);
        if (DawnMod:FilterByOption(a1,"["..filtType.."] Loot item "..stPerson) and (posYou==posYouCheck)) then
            stExtra="loot item zone";
            bBlock=true;
            end
        tmp=string.upper(a1);
        pos=1;
        while(arrColourQuality[pos]) do
            if (string.find(tmp,"|CFF"..arrColourQuality[pos])) then
                filtType = arrColourQuality[pos+1];
                end
            pos=pos+1;
            end
        if (DawnMod:FilterByOption(a1,"Loot item "..stPerson.." ("..filtType.." quality)") and (posYou==posYouCheck)) then
            stExtra="loot item colour";
            bBlock=true;
            end
        if (DawnMod:FilterByOption(a1,"Loot roll selections")) then
            stExtra="Loot roll selection";
            bBlock=true;
            end
        end
    if (e=="CHAT_MSG_MONEY") then
        if (DawnMod:CfgGetValue(DawnModConfigItem_FixFastTrack) ) then
            stOut = DawnMod:FixFastTrack( stOut);
            end

        if (DawnModFilters:GetOption("["..filtType.."] Loot gold "..stPerson) and (posYou==posYouCheck)) then
            bBlock=true;
            end
        end
    if ((e=="CHAT_MSG_FACTION_CHANGE")or(e=="CHAT_MSG_COMBAT_FACTION_CHANGE")) then
        if (DawnModFilters:GetOption("["..filtType.."] Reputation change")) then
            bBlock=true;
            end
        end

     if ((DawnMod:IsDebug() or DawnMod:CfgGetValue(DawnModConfigItem_TestMode)) and bBlock) then
        bBlock = false;
        stOut = SetFailMsgColours("(message filter"..(stExtra~="" and ":"..stExtra or "")..") "..stOut);
        end

    prevMsgData.failReason  = (bBlock and FailReason_MessageFilter or 0);
    prevMsgData.failLevel   = (bBlock and FailLevel_Fatal or 0);
    prevMsgData.stMsg   = a1;
    prevMsgData.stChannel   = "";
    prevMsgData.msgType = MsgType_System;
    prevMsgData.curTime = GetTime();
    prevMsgData.msgNo   = a11;
    prevMsgData.stOut   = stOut;
    prevMsgData.stSender    = a2;
    prevMsgData.stSenderFlags = "";

    return bBlock, stOut,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,...
    end


function DawnMod:GetPlayerPosString()
    local px,py,st,zt;

        st="";
    px,py = GetPlayerMapPosition("PLAYER");
    if ((px>0)and(py>0)) then st=st..format("(%.0f,%.0f) ",px*100,py*100); end
    zt=GetRealZoneText();
    if (zt==nil) then zt="";end
    return st..zt;
end


function DawnMod:CheckParse( stString)
    local   nIndex, stToken, tokenType, bMore, stOut, stParseCol;

    bMore = true;
    nIndex = 0;
    stParseCol = "C0C0FF";
    PrintMsg("|cFF"..stParseCol.."REAL = >|r"..DawnMod:ShowReal(stString).."|cFF"..stParseCol.."<|r");

    while (bMore) do
        stToken,tokenType,nIndex = DawnMod:GetToken(stString, nIndex);
        if (stToken == nil) then bMore = false;
        else    stOut = "|cFF"..stParseCol..tostring(nIndex)..": "..arrGtTypeName[tokenType].."("..tokenType..")>|r";
            if ((tokenType == GtType_Alpha) or
                (tokenType == GtType_Num) or
                (tokenType == GtType_Punctuation) or
                (tokenType == GtType_LinkText) or
                (tokenType == GtType_SystemText) ) then
                stOut = stOut..stToken;
                end
            if ((tokenType == GtType_Unknown) or
                (tokenType == GtType_Whitespace) or
                (tokenType == GtType_RaidMark) or
                (tokenType == GtType_ControlCodes) ) then
                stOut = stOut..DawnMod:ShowReal(stToken);
                end
            PrintMsg(stOut.."|cFF"..stParseCol.."<|r");
            end
        end
    end


function DawnMod:TimeDateString()
    return date("%Y-%m-%d/%H:%M:%S");
    end


function TimeDateInfo:New( obj)
    obj = obj or {};

    setmetatable(obj,self);
        self.__index   = self;

    return obj
    end


function DawnMod:ParseTimeDateString( tdsIn)
    local   tdi,pos, tds;

    tds = tdsIn;
    if (type(tds)~="string") then
        PrintMsg("ParseTimeDateString: tds is ("..tostring(type(tds))..") = "..tostring(tds));
        end
    assertstr(tds);

    tdi = TimeDateInfo:New();

    pos = string.find(tds,"-");
    if (pos==nil) then return nil; end
    tdi.year = tonumber(string.sub(tds,1, pos-1));
    if (tdi.year==nil) then return nil;end
    tds = string.sub(tds,pos+1);

    pos = string.find(tds,"-");
    if (pos==nil) then return nil;end
    tdi.month = tonumber(string.sub(tds,1, pos-1));
    if ((tdi.month==nil)or(tdi.month<1)or(tdi.month>12)) then return nil;end
    tds = string.sub(tds,pos+1);

    pos = string.find(tds,"/");
    if (pos==nil) then return nil;end
    tdi.day = tonumber(string.sub(tds,1, pos-1));
    if ((tdi.day==nil)or(tdi.day<1)or(tdi.day>31)) then return nil; end
    tds = string.sub(tds,pos+1);

    pos = string.find(tds,":");
    if (pos==nil) then return nil;end
    tdi.hour=tonumber(string.sub(tds,1,pos-1));
    if ((tdi.hour==nil)or(tdi.hour<0)or(tdi.hour>24)) then return nil;end
    tds = string.sub(tds,pos+1);

    pos = string.find(tds,":");
    if (pos==nil) then return nil;end
    tdi.min=tonumber(string.sub(tds,1,pos-1));
    if ((tdi.min==nil)or(tdi.min<0)or(tdi.min>59)) then return nil;end
    tds = string.sub(tds,pos+1);

    tdi.sec=tonumber(tds);
    if ((tdi.sec==nil)or(tdi.sec<0)or(tdi.sec>59)) then return nil;end

    tdi.isdst=false;
    return tdi;
    end



function DawnMod:TimeDateNum( tds)
    local   td;

    td = self:ParseTimeDateString( tds);
    if (td == nil) then return nil; end
    return time(td);
    end


function DawnMod:TimeDateDif( timeDateStringA, timeDateStringB)
    local   ta, tb;

    ta = self:TimeDateNum( timeDateStringA);
    tb = self:TimeDateNum( timeDateStringB);

    if ((ta==nil) or (tb==nil)) then return nil; end

    if (ta>tb) then return ta-tb;
    else    return tb-ta;
        end
    end


function DawnMod:CheckGroupChanged()
    local   tmp;

    tmp = DawnMod:NumInGroup();
    if (numGroupMembers~=tmp) then
        if (numGroupMembers==0) then
            DawnMod:SendUpgradeCheck("RAID");
            end
        numGroupMembers = tmp;
        DawnMod:CheckTrappedCombatWhispers( event);
--      PrintDbg("numGroupMembers = "..tostring(numGroupMembers));
        end
    end


function DawnMod:GetVersionInfo( stVerString)
    local   str, pos, stPost, verPost, verNum;

    str = stVerString;
    stPost = "";
    pos = string.find( stVerString," ");
    verPost = 0;
    if (pos) then
        stPost = string.upper(string.sub( str, pos+1));
        str = string.sub( str, 1, pos-1);
        if (StringMatch(stPost,"ALPHA")) then verPost = 1; end
        if (StringMatch(stPost,"BETA")) then verPost = 2; end
        end
    verNum = tonumber(str);
    if ((type(verNum)~="number") or (verNum<1)) then
        return nil,nil;
        end
    return tonumber(str), verPost;
    end


function DawnMod:IsUpgrade( stCheckVer, stCurVer)
    local   curVer, curPost, newVer, newPost;

    curVer, curPost = DawnMod:GetVersionInfo( stCurVer);
    newVer, newPost = DawnMod:GetVersionInfo( stCheckVer);

    PrintDbg("cur >"..tostring(curVer).."<  curPost >"..tostring(curPost).."<");
    PrintDbg("new >"..tostring(newVer).."<  newPost >"..tostring(newPost).."<");

    if ((curVer==nil)or(curPost==nil)or(newVer==nil)or(newPost==nil)) then
        if ((curVer==nil)or(curPost==nil)) then
            PrintDbg("IsUpgrade found invalid version [curVer] >"..stCurVer.."<");
            end
        if ((newVer==nil)or(newPost==nil)) then
            PrintDbg("IsUpgrade found invalid version [checkVer] >"..stCheckVer.."<");
            end
        return false;
        end

    if ((newVer>curVer) and (newPost==0)) then return true; end
    if ((newVer==curVer) and (curPost~=0) and (newPost>curPost)) then return true; end
    return false;
    end


function DawnMod:NewVersionAvailable()
    local td;

    if (not DawnMod:CfgGetValue(DawnModConfigItem_UpdateNotificationEnabled)) then return end
    PrintDbg("lastUpdateNotification = "..tostring(DawnMod:GetLastUpdateNotification()));
    td = DawnMod:TimeDateString();
    if ((DawnMod:GetLastUpdateNotification()==nil) or
        (DawnMod:TimeDateDif( DawnMod:GetLastUpdateNotification(),td)> 23*60*60)) then -- notify every 23+ hours
        DawnMod:PrintHeader();
        DawnMod:PrintInfoItem("A newer version of "..DawnMod_IdString.." is available.");
        DawnMod:PrintNotice("A newer version of "..DawnMod_IdString.." is available.", ChatTypeInfo["SAY"]);
        DawnMod:SetLastUpdateNotification( td);
        end
    end


function DawnMod:SendAddonMessage( stMessage, stDistribution, stSendeeIn)
    local   stSendee;
    assertstr(stMessage);
    assertstr(stDistribution);
    stSendee = stSendeeIn;
    if (type(stSendee)~="string") then stSendee = ""; end
    if (DawnMod:CfgGetValue(DawnModConfigItem_CommunicationEnabled)) then
        SendAddonMessage( DawnMod_IdString, stMessage, stDistribution, stSendee);
        end
    end


function DawnMod:SendUpgradeCheck( stDistribution)
    assertstr(stDistribution);
    DawnMod:SendAddonMessage( "UpgradeCheck:"..tostring(DawnMod:GetVersion()), stDistribution);
    end


function DawnMod:DoVersionCheck( stChannel)
    local   tmp;

    if (not DawnMod:CfgGetValue(DawnModConfigItem_CommunicationEnabled)) then
        DawnMod:PrintInfoItem(DawnMod_IdString.."'s communications are disabled.");
        return
        end
    tmp="";
    if (stChannel~="") then
        if (StringMatch(stChannel,"GUILD")) then tmp="GUILD";end
        if (StringMatch(stChannel,"PARTY")) then tmp="PARTY";end
        if (StringMatch(stChannel,"RAID")) then tmp="RAID";end
        end
    if (tmp=="") then
        if (IsInGuild()) then tmp = "GUILD";end
        if (DawnMod:PlayerIsInParty()) then tmp="PARTY";end
        if (DawnMod:PlayerIsInRaid()) then tmp="RAID";end
        end
    if (StringMatch(tmp,"GUILD") and (not IsInGuild())) then
        DawnMod:PrintInfoItem("You are not in a guild.");
        return false;
        end
    if (StringMatch(tmp,"RAID") and (not DawnMod:PlayerIsInRaid())) then
        DawnMod:PrintInfoItem("You are not in a raid.");
        return false;
        end
    if (StringMatch(tmp,"PARTY") and (not DawnMod:PlayerIsInParty())) then
        DawnMod:PrintInfoItem("You are not in a party.");
        return false;
        end
    if (tmp~="") then
        DawnMod:PrintInfoItem("You're using: ",DawnMod:GetVersion());
        DawnMod:PrintInfoItem("Version check for "..string.lower(tmp)..".");
        versionCheckSent = GetTime();
        DawnMod:SendAddonMessage("VersionCheck", tmp);
        return true;
        end
    DawnMod:PrintInfoItem("Version check should specify; PARTY, RAID or GUILD.");
    return false;
    end


function DawnMod:MessageHandlerAddon( stPrefix, stMessage, stDistribution, stSender,...)
    local   bHandled, stTemp;

    if (not StringMatch( stPrefix, DawnMod_IdString)) then
        return false, stPrefix, stMessage, stDistribution, stSender, ...;
        end

    -- Don't respond to messages which we sent
    if ( StringMatch(string.upper( DawnMod:GetLongName(stSender)),string.upper(DawnMod:GetLongName(UnitName("PLAYER"))))) then
        return true, stPrefix, stMessage, stDistribution, stSender,...;
        end

    bHandled = false;

    if (string.find(stMessage,"UpgradeCheck:")) then
        bHandled = true;
        stTemp = string.sub(stMessage, string.len("UpgradeCheck:")+1);
        if (DawnMod:IsUpgrade( stTemp, DawnMod:GetVersion())) then -- they have a newer version
            DawnMod:NewVersionAvailable();
            end
        if (DawnMod:IsUpgrade( DawnMod:GetVersion(), stTemp)) then -- we're using a newer version
            DawnMod:SendAddonMessage( "HaveUpgrade:"..tostring(DawnMod:GetVersion()), "WHISPER", stSender);
            end
        end

    if (string.find( stMessage, "HaveUpgrade:")) then
        bHandled = true;
        if (DawnMod:IsUpgrade( string.sub( stMessage, string.len("HaveUpgrade:")+1), DawnMod:GetVersion())) then
            DawnMod:NewVersionAvailable();
            end
        end

    if (string.find( stMessage, "VersionCheck")) then
        bHandled = true;
        DawnMod:SendAddonMessage("UsingVersion:"..tostring(DawnMod:GetVersion()),"WHISPER",stSender);
        end

    if (string.find( stMessage, "UsingVersion:")) then
        bHandled = true;

        if (GetTime()-versionCheckSent < 10) then
            DawnMod:PrintInfoItem(stSender..": ",string.sub(stMessage,string.len("UsingVersion:")+1));
            end
        end

    if (not bHandled) then
        PrintDbg("Unknown addon message.");
        PrintDbg("   stMessage >"..stMessage.."<");
        PrintDbg("   stDist    >"..stDistribution.."<");
        PrintDbg("   stSender  >"..stSender.."<");
        end

    return true, stPrefix, stMessage, stDistribution, stSender, ...
    end


function DawnMod:InviteFlags( flagsIn)
    local   flags,fNotify,fWord;

    assertnum(flagsIn);
    flags = flagsIn;
    fNotify = false;
    if (bit.band(flags,128)>0) then
        fNotify = true;
        flags = bit.band(flags,0x7F);
        end
    fWord = false;
    if (bit.band(flags,64)>0) then
        fWord=true;
        flags = bit.band(flags,0xBF);
        end
    return flags,fNotify,fWord;
    end


function DawnMod:GetPlayerInPvpQueue()
    local   cnt, tmp;

    for cnt=1,MAX_BATTLEFIELD_QUEUES do
        tmp=GetBattlefieldEstimatedWaitTime(cnt);
        if ((tmp~=nil)and(tmp>0)) then
            return true;
            end
        end
    return false;
    end


function DawnMod:GetPlayerInPveQueue()
    local   r=GetLFGQueueStats();
    if (r==nil) then r=false;end
    return r;
    end


function DawnMod:GetInviteStatus( stName)
    local   bA,bF,bG,cfgNo,stType;

    bA,bF,bG = dmKnown:IsKnown(DawnMod:GetLongName(stName));
    cfgNo = nil;
    if ((cfgNo==nil) and bF) then cfgNo = DawnModConfigItem_InviteAcceptFriends;stType="friend"; end
    if ((cfgNo==nil) and bG) then cfgNo = DawnModConfigItem_InviteAcceptGuild;stType="guild member";end
    if ((cfgNo==nil) and bA) then cfgNo = DawnModConfigItem_InviteAcceptAnswers;stType="answer";end
    if ((cfgNo==nil) and DawnModWhitelist:IsWhitelisted( DawnMod:GetLongName(stName))) then
        cfgNo = DawnModConfigItem_InviteAcceptWhitelist;
        stType="whitelisted";
        end
    if (cfgNo==nil) then cfgNo = DawnModConfigItem_InviteAcceptUnknown; stType="unknown";end
    return cfgNo, stType;
    end


function DawnMod:PartyInviteCheck( stSender)
    local   bInPvpQueue, bInPveQueue, cfgNo, val,fNotify,stType;

    assertstr(stSender);

    if (not DawnMod:CfgGetValue(DawnModConfigItem_Enabled)) then return end


    bInPvpQueue = DawnMod:GetPlayerInPvpQueue();
    bInPveQueue = DawnMod:GetPlayerInPveQueue();

    PrintDbg("PvP = "..tostring(bInPvpQueue));
    PrintDbg("PvE = "..tostring(bInPveQueue));

    cfgNo,stType = DawnMod:GetInviteStatus( stSender);

    val,fNotify = DawnMod:InviteFlags( DawnMod:CfgGetValue( cfgNo));
    if ((val == DawnModInviteValue_Accept) and
        ( (bInPveQueue and DawnMod:CfgGetValue(DawnModConfigItem_InviteOverridePve)) or
          (bInPvpQueue and DawnMod:CfgGetValue(DawnModConfigItem_InviteOverridePvp))) )then
        PrintDbg("Changing 'accept' to 'ask' because in queue.");
        val = DawnModInviteValue_Ask;
        end
    if (not fNotify) then
        tmp="G";
        if (val==DawnModInviteValue_Accept) then tmp = "Accepting g";end
        if (val==DawnModInviteValue_Deny) then tmp="Denying g";end
        DawnMod:PrintHeader();
        DawnMod:PrintInfoItem(tmp.."roup invite from "..stType.." '"..stSender.."'.");
        end

    if (val==DawnModInviteValue_Accept) then
        AcceptGroup();
        DawnMod.bHideGroupAcceptPopup=true;
        end
    if (val==DawnModInviteValue_Deny) then
        DeclineGroup();
        DawnMod.bHideGroupAcceptPopup=true;
        end
    end


local function DawnFrameEventHandler( frame, event,...)
    if (event=="PLAYER_REGEN_DISABLED") then    -- +combat
        DawnModMinimapButton:UpdateIcon();
        if (DawnMod:CfgGetValue(DawnModConfigItem_Enabled) and
            DawnMod:CfgGetValue(DawnModConfigItem_ShowCombatFlags)) then
            PrintNotice("+combat", ChatTypeInfo["RAID_WARNING"]);
            end
        end
    if (event=="PLAYER_REGEN_ENABLED") then     --  -combat
        DawnModMinimapButton:UpdateIcon();
        if (DawnMod:CfgGetValue(DawnModConfigItem_Enabled) and
            DawnMod:CfgGetValue(DawnModConfigItem_ShowCombatFlags)) then
            PrintNotice("-combat", ChatTypeInfo["SAY"]);
            end
        DawnMod:CheckTrappedCombatWhispers( event);
        end
    if (event=="PARTY_INVITE_REQUEST") then
        DawnMod:PartyInviteCheck(...);
        if (DawnMod.bHideGroupAcceptPopup) then
            StaticPopup_Hide("PARTY_INVITE");
            DawnMod.bHideGroupAcceptPopup=false;
            end
        end
    if (event=="CHANNEL_INVITE_REQUEST") then
        DawnMod:ChannelInviteCheck(...);
        end;
    if (event=="GROUP_ROSTER_CHANGED") then
        DawnMod:CheckGroupChanged();
        if (DawnMod.bHideGroupAcceptPopup) then
            StaticPopup_Hide("PARTY_INVITE");
            DawnMod.bHideGroupAcceptPopup=false;
            end
        end
    if (event=="PLAYER_LOGIN") then
        DawnMod:Begin();
        if (IsInGuild()) then DawnMod:SendUpgradeCheck("GUILD"); end
        end
    if (event=="PLAYER_LOGOUT") then
        DawnMod:Term();
        end
    if (event=="CHAT_MSG_ADDON") then
        return DawnMod:MessageHandlerAddon( ...);
        end
    end


function prdata( stName, data)
    PrintDbg("stName ("..type(data)..") = >"..tostring(data).."<");
    end


function frinfo( index)
    local   presenceID, givenName, surname, toonName, toonID, client, isOnline, lastOnline, isAFK, isDND, broadcastText, noteText, isFriend, broadcastTime  = BNGetFriendInfo(index);
    prdata("presenceID",presenceId);
    prdata("givenName",givenName);
    prdata("surname",surname);
    prdata("toonName",toonName);
    prdata("toonID",toonID);
    prdata("client",client);
    prdata("isOnline",isOnline);
    end


function DawnMod:DoTest( args)
    local   stSrc, stSrc2, stOut, cnt, bResult, index, value,st,
            cat, grp, stCat,stGrp;

--  PrintDbg(stSrc);
--    stSrc = "Consequences";
--    stSrc = "Cantheal";

--    frinfo(index);
--  DawnMod:PartyInviteCheck( stSrc);
--  DawnMod:CheckCombatWhispers( MsgType_Whisper, stSrc);

--  PrintDbg("stSrc = >"..stSrc.."<");
--  PrintDbg("stOut = >"..stOut.."<");

    PrintDbg("test");

    cat = 1;
    stCat = "1";
    while (stCat ~= "") do
        stCat = DawnModFilters:GetCategoryByIndex(cat);
        if (stCat ~= "") then
            PrintDbg("#"..cat..": "..stCat);

            grp = 1;
            stGrp = "1";
            while (stGrp ~= "") do
                stGrp = DawnModFilters:GetGroupInCategoryByIndex(stCat,grp);
                if (stGrp ~= "") then
                    PrintDbg("  "..grp..": "..stGrp);
                end
                grp = grp+1;
            end

        end
        cat = cat + 1;


    end
end


function DawnMod:OnConfigWindowSave()
    PrintDbg("OnConfigWindowSave");
    DawnMod:CfgCopy( tempConfig, DawnMod:CurConfig());
    end


function DawnMod:OnConfigWindowDefaults()
    PrintDbg("OnConfigWindowDefaults");
    DawnMod:CfgCopy( DawnMod:CurConfig(), tempConfig);
    end


function DawnMod:OnConfigWindowShow()
    if (not DawnMod_ConfigWindowVisible) then
        PrintDbg("> Visible <");
        DawnMod_ConfigWindowVisible = true;
        bPreservedDebugMode = DawnMod:IsDebug();
        DawnMod:OnConfigWindowDefaults();
        end
    end

function DawnMod:OnConfigWindowClose()
    PrintDbg("OnConfigWindowClose");
    if (DawnMod_ConfigWindowVisible) then
        DawnMod_ConfigWindowVisible = false;
        PrintDbg("> Not Visible <");
        DawnMod:SetDebug( bPreservedDebugMode);
        end
    end


function DawnMod:ShowConfigWindow()
    if (DawnMod_ConfigWindowVisible) then return end

    if (DmCfg) then
        InterfaceOptionsFrame_OpenToCategory(DmCfg:GetFrame());
        DmCfg:ConfigChanged();
    else
        DawnMod:PrintHeader();
        DawnMod:PrintInfoItem("Configuration options not available");
        end
    end


function DawnMod_SlashHandlerCamp(Command)
    Logout();
end

function DawnMod_SlashHandlerReloadUI(Command)
    ReloadUI();
end

function DawnMod_SlashHandlerKillAuctions(Command)
    local i,n;
    n = GetNumAuctionItems("owner");
    PrintMsg("Cancelling: "..n.." auction(s).");
    for i=1,n do
        CancelAuction(i);
        end
end

function DawnMod_SlashHandlerLeaveParty(Command)
	PrintMsg("Leaving Party");
	LeaveParty();
end

local function DawnMod_SlashHandlerWM( stMsgIn)
    SendChatMessage( stMsgIn,"WHISPER",nil,UnitName("PLAYER"));
    end


local function DawnMod_SlashHandlerExplain( stMsgIn)
    DawnMod:SlashHandlerExplain( stMsgIn);

    end

function DawnMod:SlashHandlerGetAchievement( stMsgIn, asType)
    DawnMod:PrintAchievementList( stMsgIn, asType);
end

function DawnMod:SlashHandlerGetAttunements()
    DawnMod:PrintAttunes();
end


local function DawnMod_SlashHandlerGetAchievement( stMsgIn)
    DawnMod:SlashHandlerGetAchievement( stMsgIn, AS_Achievement);
    end


local function DawnMod_SlashHandlerGetGuildAchievement( stMsgIn)
    DawnMod:SlashHandlerGetAchievement( stMsgIn, AS_GuildAchievement);
    end


local function DawnMod_SlashHandlerGetStatistic( stMsgIn)
    DawnMod:SlashHandlerGetAchievement( stMsgIn, AS_Statistic);
    end

local function DawnMod_SlashHandlerGetAttunements()
    DawnMod:SlashHandlerGetAttunements();
   end



function DawnMod_SlashHandler(stMsgIn) DawnMod:SlashHandler(stMsgIn);end


function DawnMod:GetMajorMinorVersionString( stVer)
    local pos1, pos2;

    pos1=string.find( stVer, "%.");
    if (pos1) then
        pos2=string.find( string.sub( stVer, pos1+1), "%.");
        if (pos2) then return string.sub(stVer, 1, pos1+pos2-1); end
        end
    return stVer;
    end



function DawnMod:ChannelInviteCheck(a1,a2,a3,a4,a5,...)
    local block = DawnMod:CfgGetValue(DawnModConfigItem_BlockChannelInvite);
    -- a1 = name of channel
    -- a2 = name of toon doing invite
    PrintDbg("CHANNEL_INVITE_REQUEST  a1="..tostring(a1).." a2="..tostring(a2).." a3="..tostring(a3));
    if (block) then
        StaticPopup_Hide("PARTY_INVITE");
        end
    return block, a2, a3, a4, a5;
    end


local   pratfixer=false;

local function DawnMod_MessageHandlerCustom(f,e,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,...)
    local   index, bSeen;
    local   stMsg;
    local   bNew=false;


    if (e=="CHAT_MSG_TEXT_EMOTE") then
        stMsg=a2..":"..a1;

        if (_G.Prat3DB ~=nil) then
            pratfixer=not pratfixer;
            if (pratfixer) then
                return false,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,...
                end
            end

        if (DawnMod.arrCustomMsgNo==nil) then
            DawnMod.arrCustomMsgNo = {};
            DawnMod.stLastCustomMsg = nil;
            end
        if (stMsg~=DawnMod.stLastCustomMsg) then
            bNew=true;
        else
            bSeen=false;
            index=1;
            while( DawnMod.arrCustomMsgNo[index]) do
                if (DawnMod.arrCustomMsgNo[index]==f) then
                    bSeen=true;
                    end
                index=index+1;
                end
            if (bSeen) then bNew=true; end
            end

        if (bNew) then
            index=1;
            while(DawnMod.arrCustomMsgNo[index]) do
                DawnMod.arrCustomMsgNo[index] = nil;
                index=index+1;
                end
            DawnMod.arrCustomMsgNo[1] = f;
            DawnMod.stLastCustomMsg = stMsg;
            DawnMod.customMsgNo = DawnMod.customMsgNo - 1;
        else
            table.insert(DawnMod.arrCustomMsgNo,f);
            end
    --  PrintDbg("Sending #"..tostring(math.abs(DawnMod.customMsgNo)).."  '"..a1.."'"..
    --      " to fr = "..tostring(f).." ("..tostring(f:GetID())..")");
        return DawnMod:MessageHandler( e,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,DawnMod.customMsgNo,a12,...);
        end -- if (e=="chat_msg_text_emote")
    end


function DawnMod:SetChatStickies()
    -- see also GetChatTypeIndex
    ChatTypeInfo["SAY"].sticky = 1;
    ChatTypeInfo["GUILD"].sticky = 1;
    ChatTypeInfo["OFFICER"].sticky = 1;
    ChatTypeInfo["PARTY"].sticky = 1;
    ChatTypeInfo["RAID"].sticky = 1;
    ChatTypeInfo["YELL"].sticky = 0;
    ChatTypeInfo["WHISPER"].sticky = 0;
    ChatTypeInfo["EMOTE"].sticky = 0;
--    ChatTypeInfo["INSTANCE_CHAT"].sticky = 1;
 --   ChatTypeInfo["INSTANCE_CHAT_LEADER"].sticky = 1;
    ChatTypeInfo["CHANNEL1"].sticky = 0;
    ChatTypeInfo["CHANNEL2"].sticky = 0;
    ChatTypeInfo["CHANNEL3"].sticky = 0;
    ChatTypeInfo["CHANNEL4"].sticky = 0;
    ChatTypeInfo["CHANNEL5"].sticky = 0;
    ChatTypeInfo["CHANNEL6"].sticky = 0;
    ChatTypeInfo["CHANNEL7"].sticky = 0;
    ChatTypeInfo["CHANNEL8"].sticky = 0;
    ChatTypeInfo["CHANNEL9"].sticky = 0;
    ChatTypeInfo["CHANNEL10"].sticky = 0;
    end


function DawnMod:Begin()
    local   bPrintHeader = true,bFirstTime, stTimeDate, dif, tmp, tmp2;

    DawnMod.customMsgNo = -1;
    DawnMod.bTickMessage    = false;
    DawnMod.bHideGroupAcceptPopup = false;

    dmPosterCache   = DawnModPosterCache:New();
    dmMsgCache  = DawnModMsgCache:New();
    dmReply     = DawnModReply:New();
    dmKnown     = DawnModKnown:New();

    if (not DawnMod:DBUnpack()) then bFirstTime=true;end

    if (DawnMod:IsDebug()) then
        if (bPrintHeader) then DawnMod:PrintHeader();end bPrintHeader=false;
        end

    if (bFirstTime)then
        PrintNotice("Welcome to "..DawnMod_IdString..".", ChatTypeInfo["SAY"]);
        PrintNotice("Use: /DM to for options.", ChatTypeInfo["SAY"]);
        end

    if (DawnMod_ForceDebugMode) then DawnMod:SetDebugMode( true); end

    dmPosterCache:Begin( Karma_ScoreMin, Karma_ScoreMax, Karma_ScoreDef, DawnMod:GetPosterData());
    dmMsgCache:Begin();
    dmReply:Begin();
    dmKnown:Begin( 25);
    DawnModMinimapButton:Begin( DawnMod:GetMinimapAngle());
    if (DawnModDB.stCatsExpanded==nil) then DawnModDB.stCatsExpanded="";end

    if ( (DawnMod:CurConfig()==nil) or
       (not DawnMod_DoConfigUpgrade(DawnModDB)) or
       (not DawnMod:CfgAssert(DawnMod:CurConfig())) or
       (DawnMod:CfgGetValue(DawnModConfigItem_ConfigVersion) ~= DawnMod_ConfigVersion) )then
        if (bPrintHeader) then DawnMod:PrintHeader();end bPrintHeader=false;
        PrintMsg("Resetting Cfg");
        DawnMod:SetCurConfig( DawnMod:CfgReset(DawnMod:CurConfig(),false,false));
        end

    DawnMod:CfgChanged();

    stTimeDate = DawnMod:TimeDateString();

    if (DawnMod:GetPosterCacheLastPurge()==nil) then
        dif = 0;
        DawnMod:SetPosterCacheLastPurge( DawnMod:TimeDateString());
    else    dif = DawnMod:TimeDateDif( stTimeDate,DawnMod:GetPosterCacheLastPurge());
        end
    if (DawnMod:IsDebug()) then
        if (bPrintHeader) then DawnMod:PrintHeader();end bPrintHeader=false;
        PrintDbg("dif = "..TimeString(dif));
        PrintDbg("  lastPurge="..DawnMod:GetPosterCacheLastPurge().."     cur="..stTimeDate);
        end
    if ((dif~=nil) and (dif>24*60*60)) then
        dif = dmPosterCache:DoPurge( DawnMod:CfgGetValue(DawnModConfigItem_PosterCachePurgeDays));
        PrintDbg("Purged "..tostring(dif).." PosterCache entries.");
        DawnMod:SetPosterCacheLastPurge( stTimeDate);
        end

    DawnMod.arrNpcChatter = {};

    for _,event in pairs({
        "CHAT_MSG_BATTLEGROUND",
        "CHAT_MSG_BATTLEGROUND_LEADER",
        "CHAT_MSG_BN_CONVERSATION",
        "CHAT_MSG_BN_WHISPER",
        "CHAT_MSG_CHANNEL",
        "CHAT_MSG_EMOTE",
        "CHAT_MSG_GUILD",
        "CHAT_MSG_OFFICER",
        "CHAT_MSG_PARTY",
        "CHAT_MSG_PARTY_LEADER",
        "CHAT_MSG_RAID",
        "CHAT_MSG_RAID_LEADER",
        "CHAT_MSG_RAID_WARNING",
        "CHAT_MSG_SAY",
        "CHAT_MSG_SYSTEM",
        "CHAT_MSG_WHISPER",
        "CHAT_MSG_YELL"
        })
        do ChatFrame_AddMessageEventFilter(event, DawnMod.MessageHandler); end

    ChatFrame_AddMessageEventFilter("CHAT_MSG_TEXT_EMOTE", DawnMod_MessageHandlerCustom);

    ChatFrame_AddMessageEventFilter("CHAT_MSG_BN_WHISPER", DawnMod.MessageHandlerWhisper);
    ChatFrame_AddMessageEventFilter("CHAT_MSG_WHISPER", DawnMod.MessageHandlerWhisper);

    ChatFrame_AddMessageEventFilter("CHAT_MSG_BN_WHISPER_INFORM", DawnMod.MessageHandlerInform);
    ChatFrame_AddMessageEventFilter("CHAT_MSG_WHISPER_INFORM",    DawnMod.MessageHandlerInform);

--    ChatFrame_AddMessageEventFilter("CHANNEL_INVITE_REQUEST", DawnMod_ChannelInviteHandler);

    for _,event in pairs({
        "CHAT_MSG_ACHIEVEMENT",
        "CHAT_MSG_COMBAT_FACTION_CHANGE",
        "CHAT_MSG_CURRENCY",
        "CHAT_MSG_FACTION_CHANGE",
        "CHAT_MSG_LOOT",
        "CHAT_MSG_MONEY",
        "CHAT_MSG_MONSTER_YELL",
        "CHAT_MSG_MONSTER_SAY",
        "CHAT_MSG_MONSTER_WHISPER",
        "CHAT_MSG_MONSTER_EMOTE",
        "CHAT_MSG_SYSTEM"
        })
        do ChatFrame_AddMessageEventFilter(event, DawnMod.MessageHandlerGameMessages); end

    tmp = true;
---
    if (tmp) then DawnMod.frame:RegisterEvent("CHAT_MSG_ADDON"); end


    SLASH_DAWNMOD1 = "/dawnmod";
    SLASH_DAWNMOD2 = "/dm";
    SlashCmdList["DAWNMOD"] = DawnMod_SlashHandler;

    SLASH_DAWNMOD_WHISPERME1 = "/whisperme";
    SLASH_DAWNMOD_WHISPERME2 = "/wm";
    SlashCmdList["DAWNMOD_WHISPERME"] = DawnMod_SlashHandlerWM;

    SLASH_DAWNMOD_EXPLAIN1 = "/explain";
    SlashCmdList["DAWNMOD_EXPLAIN"] = DawnMod_SlashHandlerExplain;

    SLASH_DAWNMOD_GETACHIEVEMENT1 = "/getach";
    SLASH_DAWNMOD_GETACHIEVEMENT2 = "/getachievement";
    SlashCmdList["DAWNMOD_GETACHIEVEMENT"] = DawnMod_SlashHandlerGetAchievement;

    SLASH_DAWNMOD_GETGUILDACHIEVEMENT1 = "/getgach";
    SLASH_DAWNMOD_GETGUILDACHIEVEMENT2 = "/getguildachievement";
    SlashCmdList["DAWNMOD_GETGUILDACHIEVEMENT"] = DawnMod_SlashHandlerGetGuildAchievement;

    SLASH_DAWNMOD_GETSTATISTIC1 = "/getstat";
    SLASH_DAWNMOD_GETSTATISTIC2 = "/getstatistic";
    SlashCmdList["DAWNMOD_GETSTATISTIC"] = DawnMod_SlashHandlerGetStatistic;

    SLASH_DAWNMOD_GETATTUNEMENTS1 = "/getatt";
    SLASH_DAWNMOD_GETATTUNEMENTS2 = "/getattune";
    SlashCmdList["DAWNMOD_GETATTUNEMENTS"] = DawnMod_SlashHandlerGetAttunements;

    SLASH_DAWNMOD_LEAVEPARTY1 = "/lp";
    SlashCmdList["DAWNMOD_LEAVEPARTY"] = DawnMod_SlashHandlerLeaveParty;

    SLASH_DAWNMOD_KILLAUCTIONS1 = "/ka";
    SlashCmdList["DAWNMOD_KILLAUCTIONS"] = DawnMod_SlashHandlerKillAuctions;

    SLASH_DAWNMOD_RELOAD1 = "/rl";
    SlashCmdList["DAWNMOD_RELOAD"] = DawnMod_SlashHandlerReloadUI;

    SLASH_DAWNMOD_CAMP1 = "/cmap";
    SlashCmdList["DAWNMOD_CAMP"] = DawnMod_SlashHandlerCamp;

    prevMsgData.prevTime = GetTime();

    if (DawnMod:CfgGetValue(DawnModConfigItem_FixChatStickies)) then
        DawnMod:SetChatStickies();
        end


	SetCVar("cameraDistanceMax",50);

	if (bPrintHeader) then DawnMod:PrintHeader(); end

    DawnMod.frame:RegisterEvent("PLAYER_LOGOUT");
    DawnMod.frame:RegisterEvent("PLAYER_REGEN_DISABLED"); -- +combat
    DawnMod.frame:RegisterEvent("PLAYER_REGEN_ENABLED"); -- -combat
    DawnMod.frame:RegisterEvent("PARTY_INVITE_REQUEST"); -- invited to party
    DawnMod.frame:RegisterEvent("GROUP_ROSTER_CHANGED"); -- may have left party
    DawnMod.frame:RegisterEvent("CHANNEL_INVITE_REQUEST"); -- invited to channel

    DmCfg:Begin();
    end


function DawnMod:Term()
    DawnMod:DBPack();
    end


DawnMod     = DawnMod:New();
DawnMod.frame   = CreateFrame("Frame");
DawnMod.frame:SetScript("OnEvent", DawnFrameEventHandler);
DawnMod.frame:RegisterEvent("PLAYER_LOGIN");
