

----------------------
--  Minimap Button  --
----------------------

local	Icon_Combat	= 1;
local	Icon_NoCombat	= 2;
local	Icon_Mail	= 3;

	DawnModMinimapButton = CreateFrame("Button", "DMMinimapButton", Minimap);
local	minimapicon = nil;

local	currentIcon	= 0;
local	lastReportedMMA = -1;


function DawnModMinimapButton:Begin()
	local	stLine, cnt;
	
	if (DawnMod:GetMinimapAngle() == nil) then
		DawnMod:SetMinimapAngle(220);
		end

	self:SetHeight(31);
	self:SetWidth(31);
	self:SetFrameStrata("MEDIUM");
--	self:SetPoint("CENTER", -65.35, -38.8);
	self:SetMovable(true);
	self:SetUserPlaced(true);
	self:SetNormalTexture(icon);
	self:SetPushedTexture(icon);
	self:SetHighlightTexture("Interface\\Minimap\\UI-Minimap-ZoomButton-Highlight");
	self:RegisterForClicks("anyUp");

	minimapicon = self:CreateTexture(nil, "BACKGROUND");
	DawnModMinimapButton:UpdateIcon();
	minimapicon:SetWidth(20); minimapicon:SetHeight(20);
	minimapicon:SetTexCoord(0.05, 0.95, 0.05, 0.95);
	minimapicon:SetPoint("TOPLEFT", 7, -5);
	self.icon = minimapicon;

	local overlay = self:CreateTexture(nil, "OVERLAY");
	overlay:SetWidth(53); overlay:SetHeight(53);
	overlay:SetTexture("Interface/Minimap/MiniMap-TrackingBorder");
	overlay:SetPoint("TOPLEFT");

	self:SetScript("OnClick", function(self, button)
		if (IsShiftKeyDown() or (button == "RightButton")) then return end
		DawnMod:ShowConfigWindow();
		end);
	self:SetScript("OnDragStart", self.OnDragStart);
	self:SetScript("OnDragStop", self.OnDragStop);
	self:SetScript("OnKeyDown", DawnMod.DoDragCheck);
	self:SetScript("OnMouseUp", function(a,b)self:OnMouseUp(a,b);end);
	self:SetScript("OnMouseDown", self.OnMouseDown);
		
	self:SetScript("OnEnter", function(self)
		self:DoDragCheck();
		GameTooltip:SetOwner(self, "ANCHOR_BOTTOMRIGHT")
		GameTooltip:SetText(DawnMod:GetHeaderString(true),NORMAL_FONT_COLOR.r, NORMAL_FONT_COLOR.g, NORMAL_FONT_COLOR.b, 1);
		GameTooltip:AddLine(DawnMod:GetVersionString(true));
		GameTooltip:AddLine("|cFFA0A0FFLeft click to show options|r");
		GameTooltip:AddLine("|cFFA0A0FFShift + drag to move icon|r");
		
		cnt	= 1;
		stLine	= "";
		while (stLine ~= nil) do
			stLine = DawnMod:GetTooltipLine( cnt);
			cnt = cnt+1;
			if (stLine) then
				GameTooltip:AddLine( stLine);
				end
			end
		
		GameTooltip:Show();
		end)
	self:SetScript("OnLeave", function(self)
		GameTooltip:Hide();
		end)
	self:UpdatePosition();
	end


function DawnModMinimapButton:DoDragCheck()
	if (IsShiftKeyDown() or IsAltKeyDown()) then
		self:RegisterForDrag("LeftButton");
	else	self:RegisterForDrag(nil);
		end
	end


function DawnModMinimapButton:OnDragStart()
	self.dragging = true;
	self:LockHighlight();
	self.icon:SetTexCoord(0, 1, 0, 1);
	self:SetScript("OnUpdate", self.OnUpdate);
	GameTooltip:Hide();
	end


function DawnModMinimapButton:OnDragStop()
	self.dragging = nil;
	self:SetScript("OnUpdate", nil);
	self.icon:SetTexCoord(0.1, 0.9, 0.1, 0.9);
	self:UnlockHighlight();
	end


function DawnModMinimapButton:OnUpdate()
	local mmx,mmy,px,py, scale;
	
	mmx, mmy = Minimap:GetCenter();
	px, py = GetCursorPosition();
	scale = Minimap:GetEffectiveScale();
	px = px/scale;py = py/scale;
	DawnMod:SetMinimapAngle(math.deg(math.atan2(py-mmy,px-mmx))%360 );
	self:UpdatePosition();
	end


function DawnModMinimapButton:OnMouseDown()
	self.icon:SetTexCoord(0,1,0,1);
	end


function DawnModMinimapButton:OnMouseUp(obj,stBtn)
	self.icon:SetTexCoord(0.05,0.95,0.05,0.95);
	if ((DawnMod:GetNumTrappedWhispers()>0)and(stBtn=="RightButton")) then
		DawnMod:PrintCombatWhispers(false);
		end
	end


function DawnModMinimapButton:SetVisibility( bShow)
	if ( (type(bShow)=="boolean") and bShow) then
		self:Show();
	else	self:Hide();
		end
	end


function DawnModMinimapButton:UpdatePosition()
	local x,y,angle,cos,sin,mms,rnd;

	x = floor(DawnMod:GetMinimapAngle());
	if (x~=lastReportedMMA) then
		DawnMod:PrintDbg("Minimap angle: "..tostring(x));
		lastReportedMMA = x;
		end
	angle =math.rad(DawnMod:GetMinimapAngle());
	cos =math.cos(angle);sin =math.sin(angle);
	mms =GetMinimapShape and GetMinimapShape() or "ROUND";

	rnd = false;
	if (mms=="CORNER-BOTTOMLEFT") then rnd= not(cos>0 or sin>0);end
	if (mms=="CORNER-BOTTOMRIGHT") then rnd= not(cos<0 or sin>0); end
	if (mms=="CORNER-TOPRIGHT") then rnd= not(cos<0 or sin<0);end
	if (mms=="CORNER-TOPLEFT") then rnd= not(cos>0 or sin<0);end
	if (mms=="ROUND") then rnd =true;end
	if (mms=="SIDE-LEFT") then rnd= cos<=0;end
	if (mms=="SIDE-RIGHT") then rnd= cos>=0;end
	if (mms=="SIDE-TOP") then rnd= sin<=0;end
	if (mms=="SIDE-BOTTOM") then rnd= sin>=0;end
	if (mms=="SQUARE") then rnd= false;end
	if (mms=="TRICORNER-BOTTOMLEFT") then rnd= not(cos>0 and sin<0);end
	if (mms=="TRICORNER-BOTTOMRIGHT") then rnd= not(cos<0 and sin<0);end
	if (mms=="TRICORNER-TOPLEFT") then rnd= not(cos>0 and sin>0);end
	if (mms=="TRICORNER-TOPRIGHT") then rnd= not(cos<0 and sin>0);end

	if rnd then
		x =cos*80; y =sin*80;
	else	x =math.max(-82, math.min(110*cos, 84));
		y =math.max(-86, math.min(110*sin, 82));
		end
	self:SetPoint("CENTER", x, y);
end


-- http://www.wowwiki.com/WoWWiki:WoW_Icons/Icon_List/Images

function DawnModMinimapButton:UpdateIcon()
	local	newIcon;
	
	if (UnitAffectingCombat("PLAYER")) then
		newIcon = Icon_Combat;
	else
		if (DawnMod:GetNumTrappedWhispers()>0) then
			newIcon = Icon_Mail;
		else	newIcon = Icon_NoCombat;
			end
		end
	if (newIcon == currentIcon) then return; end
	
	if (newIcon==Icon_Combat) then minimapicon:SetTexture("Interface/Icons/Spell_ChargePositive"); end
	if (newIcon==Icon_NoCombat) then minimapicon:SetTexture("Interface/Icons/Spell_ChargeNegative");end
	if (newIcon==Icon_Mail) then minimapicon:SetTexture("Interface/Icons/INV_Letter_10"); end
	currentIcon = newIcon;
	end
