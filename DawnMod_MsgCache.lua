
local function PrintMsg( stMsg)	DEFAULT_CHAT_FRAME:AddMessage(stMsg); end
local function PrintDbg( stMsg) DawnMod:PrintDbg(stMsg); end
local function assertstr(x) assert(type(x)=="string");end
local function assertnum(x) assert(type(x)=="number");end
local function assertbool(x) assert(type(x)=="boolean");end
local function asserttab(x) assert(type(x)=="table");end

	DawnModMsgCache = {};

function DawnModMsgCache:New(obj)
	obj = obj or {};

    	obj.scoreMin = 0;
  	obj.scoreMax = 0;

	setmetatable(obj,self);
    	self.__index   = self;
    	
    	obj.suggestedMaxLength	= 0;	-- what the outside code is saying it should be
    	obj.maxLength		= 0;	-- what it actually is.
    	obj.arr			= {};	-- [1] time#1, [2]msg#1 [3]time#2 [4]msg#2
    	obj.numEntries		= 0;
    	obj.peakMsgPerSec	= 0;
    	
	return obj
	end


function DawnModMsgCache:Begin()
	end


function DawnModMsgCache:FindMostRecentMsgByName( stName)
	local	index, stUprName, mostRecentIndex, mostRecentTime, tmp;
	
	assertstr(stName);
	if (self.numEntries==0) then return nil,nil; end

	stUprName = string.upper(stName);
	mostRecentIndex = nil;
	
	for index=0,self.numEntries-1 do
		if (stUprName==string.sub(self.arr[index*2+2],1, string.find(self.arr[index*2+2],"%:")-1) ) then
			if (mostRecentIndex==nil) then
				mostRecentIndex = index;
				mostRecentTime = self.arr[index*2+1];
			else	if (self.arr[index*2+1]<mostRecentTime) then
					mostRecentIndex = index;
					mostRecentTime = self.arr[index*2+1];
					end
				end
			end
		end
	return mostRecentIndex, mostRecentTime;
	end


function DawnModMsgCache:TuneLength( DawnMod, curTime, timeToKeep, msgPeriod, timePeriod, msgTotal, timeTotal)
	local	incBy, tmpNum, incFlag, wantedCoverage;
	
	asserttab(DawnMod);
	assertnum(curTime);
	assertnum(timeToKeep);
	assertnum(msgPeriod);
	assertnum(timePeriod);
	assertnum(msgTotal);
	assertnum(timeTotal);
	if (UnitAffectingCombat("PLAYER")) then return end;
	
	if (self.numEntries>0) then
	-- check if we need to make the arr longer.
		incFlag = 0;
		wantedCoverage=timeToKeep + (timeToKeep/4);	-- time-to-remember * 125%
		
		-- if (msgPerPeriod > wantedCoverage)
		if ((msgPeriod>0) and (timePeriod>0) and (msgPeriod>timePeriod)) then
			if (msgPeriod*wantedCoverage/timeTotal<self.maxLength) then
				incFlag = incFlag+1;
				PrintDbg("rembuf_TuneLength: Flag #1 tripped.");
				end
			tmpNum=msgPerPeriod/timePeriod;
			if (tmpNum>self.peakMsgPerSec) then
				peakmsgPerSec = tmpNum;
				end
			end
		
		if ((msgTotal>0) and (timeTotal>0) and (msgTotal>timeTotal)) then
			if (msgTotal*wantedCoverage/timeTotal>self.maxLength) then
				incFlag = incFlag+1;
				PrintDbg("rembuf_TuneLength: Flag #2 tripped.");
				end
			end
		
		if (incFlag>0) then
			incBy = math.floor(self.maxLength / 20 * incFlag);	-- upto 15% increase.
			if (incBy<1) then incBy = 1; end			-- min increase of 1
			self.maxLength = self.maxLength + incBy;
			PrintDbg("rembuf:TuneLength increased maxLength by ("..tostring(incFlag*5).."%) "..tostring(incBy).." to "..tostring(self.maxLength)..".");
			end
		end
	end


function DawnModMsgCache:SetMaxLength( newMaxLength, bForceLengthChange)
	local bForce;
	assertnum(newMaxLength);
	
	if ((bForceLengthChange==nil)or(type(bForceLengthChange)~="boolean")) then
		bForce=false;
	else	bForce=bForceLengthChange;
		end
	
	if ((newMaxLength~=self.suggestedMaxLength) or bForceLengthChange) then
		self.suggestedMaxLength = newMaxLength;
		self.maxLength = newMaxLength;
		end
	end


function DawnModMsgCache:GetMaxLength()
	return self.maxLength;
	end

function DawnModMsgCache:Length()
	return self.numEntries;
	end


function DawnModMsgCache:GetEntryString( index)
	assertnum(index);
	if ((index==nil) or (index<0)or(index>=self.numEntries)) then
		return nil;
		end
	return self.arr[index*2+2];
	end


function DawnModMsgCache:FindOldest()
	local	cnt, indexOldest, timeOldest;

	if (self.numEntries==0) then return -1; end
	indexOldest = 0;
	timeOldest = self.arr[1];
	for cnt=0,self.numEntries-1 do
		if (self.arr[cnt*2+1]<timeOldest) then
			indexOldest = cnt;
			timeOldest = self.arr[cnt*2+1];
			end
		end
	return indexOldest;
	end


function DawnModMsgCache:Add( curtime, stMsg)
	local	index;

	assertnum(curtime);
	assertstr(stMsg);
	if (self.numEntries == self.maxLength) then
		index = self:FindOldest();
	else	index = self.numEntries;
		self.numEntries = self.numEntries+1;
		end
	self.arr[index*2+1] = curtime;
	self.arr[index*2+2] = stMsg;
	end


function DawnModMsgCache:Wipe()
	self.numEntries = 0;
	end


function DawnModMsgCache:Find( stMsg)
	local	index;

	assertstr(stMsg);
	index = 0;
	while (index < self.numEntries) do
		if (stMsg==self.arr[index*2+2]) then
			return index;
			end
		index = index + 1;
		end
	return -1;
	end



function DawnModMsgCache:GetEntryTime( nIndex)
	assertnum(nIndex);
	if ((nIndex>=0) and (nIndex < self.numEntries)) then
		return self.arr[nIndex*2+1];
		end
	return nil;
	end


function DawnModMsgCache:SetTime( nIndex, newTime)
	assertnum(nIndex);
	assertnum(newTime);
	if ((nIndex>=0) and (nIndex < self.numEntries)) then
		self.arr[nIndex*2+1] = newTime;
		end
	end


function DawnModMsgCache:Update( newTime, stMsg)
	local	index;

	assertnum(newTime);
	assertstr(stMsg);
	index = self:Find( stMsg);
	if (index >= 0) then 
		self:SetTime( index, newTime);
	else	self:Add( newTime, stMsg);
		end
	end


function DawnModMsgCache:FindNearestEntryByTime( targetTime)
	local	curIndex, lowIndex, curDist, lowDist;

	assertnum(targetTime);
	if (self.numEntries == 0) then return nil; end

	lowDist		= 9999999999;
	curIndex	= 0;

	while ((lowDist>0) and (curIndex<self.numEntries)) do
		if (targetTime>self.arr[curIndex*2+1]) then
			curDist = targetTime-self.arr[curIndex*2+1];
		else	curDist = self.arr[curIndex*2+1]-targetTime;
			end
		if (curDist<lowDist) then
			lowIndex = curIndex;
			lowDist = curDist;
			end
		curIndex=curIndex+1;
		end
	return lowIndex;
	end
